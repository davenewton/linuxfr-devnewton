URL:     https://linuxfr.org/news/entretien-avec-alekmaul-a-propos-de-pvsneslib
Title:   Entretien avec Drush à propos de RGBDS
Authors: devnewton
Date:    2024-02-12T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   1


Drush développe [un SDK](https://github.com/gbdev/rgbds) pour créer des jeux pour les consoles [Game Boy et Game Boy Color](https://fr.wikipedia.org/wiki/Game_Boy) : le [Rednex Game Boy Development System](https://rgbds.gbdev.io/).

Cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.

![Game Boy](https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Game-Boy-Original.jpg/480px-Game-Boy-Original.jpg?uselang=fr)

----

[RGBDS](https://rgbds.gbdev.io/)
[Un article sur l'architecture de la Game Boy](https://www.copetti.org/writings/consoles/game-boy/)
[Le dossier de Grospixels sur cette console](https://www.grospixels.com/site/gameboy.php)

----

## Partie 1: présentation

### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?

### Pourquoi le retrogaming est-il important pour vous ?

## Partie 2: Game Boy

### Comment en êtes-vous venu à vous intéresser à la Game Boy ?

### Qu’est-ce que cette console a de particulier ?

### Est-elle proche de la [NES](https://fr.wikipedia.org/wiki/Nintendo_Entertainment_System)  ?

### Comment fonctionne l'affichage (PPU, écran LCD) ?

### Comment fonctionne la puce sonore (PSG chip) ?

### La machine possède une ROM interne, à quoi sert-elle ?

### La GB possède un dispositif anti piratage, comment fonctionne-t-il ?

### Comment fonctionne le réseau (Game Boy Link) ?

### Certaines versions ont aussi un émetteur/récepteur infrarouge, à quoi sert-il ?

### Les cartouches peuvent-elles embarquer des coprocesseurs ?

### Comment la Game Boy Color ajoute-t-elle la couleur tout en gardant la rétrocompatibilité ? Apporte-t-elle d'autres nouveautés ?

### Les émulateurs sont-ils bons ?

### Quels sont vos jeux commerciaux préférés sur cette console ?

### Quels sont vos jeux "homebrew" préférés sur cette console ?

## Partie 2 : RGBDS

### Pourquoi créer un SDK aujourd’hui pour si vieux système ?

### Est-ce que vous participez vous même à la création de jeux ?

### Quels ont été les difficultés pour créer RGBDS ?

### Vous aimeriez vivre du développement de ce logiciel libre?

### Est-ce que RGBDS gère les accessoires (caméras, imprimantes, sonar...) de la console ?

### Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement de jeux Game Boy ?

### Quels sont les outils pour créer/préparer des graphismes utilisable par RGBDS ?

### Quels sont les outils pour créer/préparer des musiques et des sons utilisable par RGBDS  ?

### Est-il possible de créer ses propres cartouches ?

### Comment s’accommoder des faibles ressources (peu de RAM, CPU 8 bits...) ?

### Est-ce qu'il existe d'autres SDK libres pour ces consoles ?

## Partie 3: pour finir

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

### Quelle question auriez-vous adoré qu’on vous pose ?

### Quelle question auriez-vous détesté qu’on vous pose ?
