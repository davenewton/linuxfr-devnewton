URL:     https://linuxfr.org/news/liberer-les-consoles-delivrer-les-jeux
Title:   Libérer les consoles, délivrer les jeux. Episode 1 : un coup d'oeil dans le rétro.
Authors: Dave Newton 🍺
         Chilin
Date:    2024-02-06T00:00:00+01:00
License: CC By-SA
Tags:    jeu_vidéo et retrogaming
Score:   1


Depuis les soucis de Richard Stallman avec son imprimante, le libre s'est
imposé dans beaucoup de domaines, mais il reste anecdotique dans le jeu
vidéo. On a souvent parlé sur linuxfr des possibles modèles économiques
librés pour ce secteur, mais rarement des problèmes que posent
le modèle privateur et des solutions que le libre permettraient d'y
apporter.

DRM, exclusivités, tivoisation, perte du patrimoine, obsolescence rapide... aujourd'hui la seule réponse pour les joueurs passionnés floués par l'industrie
est le piratage. Et si le libre offrait une porte de sortie de légale ?

Cette dépêche est le début d'une série, ce premier épisode se concentre sur le rétrogaming.



![ Star Wars - 1983 arcade game - Coin slot, source Wikipédia, ©Sean Cook (Pixel8) 2011](https://upload.wikimedia.org/wikipedia/commons/1/1f/Star_Wars_-_1983_arcade_game_-_Coin_slot.jpg)



[Liste de clones libres de jeux privateurs](https://osgameclones.com/)

----


----

# Les restrictions imposées aux joueurs

TODO: lister tout ce qui peut embêter les joueurs :-)

## DRM

La [gestion numérique des restrictions](https://fr.wikipedia.org/wiki/DRM) ou DRM prends diverses formes dans le monde du rétrogaming.

Généralement utilisé pour restreindre directement les droits des joueurs, l'une des plus célèbres dans l'histoire est le [CIC](https://en.wikipedia.org/wiki/CIC_(Nintendo)) de Nintendo qui vise plutôt les développeurs : cette puce a pour de les forcer à obtenir l'approbation de "Big N" pour faire tourner un jeu sur la [NES](https://en.wikipedia.org/wiki/Nintendo_Entertainment_System). L'argument à l'époque était d'empêcher la sortie de jeu de piêtre qualité (à entendre plutôt au sens "buggé" que "mauvais", comme le montre les sorties de Paper Boy, Dragon's Lair ou Zelda II). Pourtant le système n'est pas sans conséquence pour les joueurs : les éditeurs reporte la "taxe" Nintendo sur les joueurs, certains renoncent à sortir leurs jeux, les homebrews sont empêchés ainsi que le droit à la copie de sauvegarde.

Ce dernier droit est souvent celui visé par les DRM sous couvert de lutte contre le piratage. Jusqu'ici les DRM ont toujours fini par être crackés comme celui de la PS1 avec des [modchips](https://blog.kchung.co/making-playstation-modchips/) ou sans comme [sur Saturn](https://gbatemp.net/threads/the-sega-saturn-drm-has-been-cracked-after-twenty-years.434065/).

Certaines consoles n'ont pas de DRM, comme la [PC Engine de NEC](https://fr.wikipedia.org/wiki/PC-Engine), car à l'époque reproduire une [hucard](https://fr.wikipedia.org/wiki/HuCard) ou graver un CDROM demande un matériel si cher que NEC a du estimer que ce serait inacessible à l'immense majorité des joueurs.

## Le zonage

## Obsolescence matérielle, logicielle

## Exclusivité

## Lien avec des services en ligne

# Les problèmes pour le bien commun

## Difficultés de préservation du patrimoine

## Accès inégal selon les pays ?

## Sauvegardes : Konami veut le monopole du coeur

Si aujourd'hui les logiciels d'édition de sauvegarde ou le partage de sauvegarde est monnaie courante, certains éditeurs les ont combattu activement au nom du respect de leurs oeuvres d'art.

Dans une affaire concernant la *dating sim*, l'éditeur [Konami est allé en justice](http://gaming.moe/?p=2938) pour faire interdire la vente de cartes mémoires contenant des sauvegardes pour son jeu déclenchant l'ire des joueurs souhaitant profiter de certains romances du jeu sans y passer les milliers d'heures nécessaires.

...

# Et avec les quatres libertés ?

TODO: montrer à quels problèmes précédemment décrit avoir les libertés d'une licence libre serait une solution. Peut être avec un schéma ?

## Portages

### Amiga CD32 : les fans de Commodore se consolent

L'Amiga CD32 a reçu des portages de jeux Amiga : https://www.rgcd.co.uk/2011/04/unofficial-amiga-cd32-conversions-amiga.html

# Et en attendant ?

TODO: parler du compromis actuel => piratage, abandonware, gros hacks...

## Edition de sauvegarde



## Copie privée

La sauvegarde de jeux sur cartouches demande un périphérique spécial. Les premiers furent les [Game backup device](https://en.wikipedia.org/wiki/Game_backup_device) qui permettaient de "dumper" la ROM d'un jeu sur une disquette et de la restaurer dans une mémoire embarquée.

Aujourd'hui on utilise des cartes SD ou des transferts via USB. Certains dispositifs sont opensource comme le [cartreader](https://github.com/sanni/cartreader) à base d'Arduino.

A quelques protections prêt, la copie de jeux sur CD ou DVD est plus facile. Pour les consoles utilisant un disque spécial (comme le [GD-ROM](https://dcemulation.org/index.php?title=A_History_of_Media_and_Booting_on_the_Sega_Dreamcast) de 1 Gio sur Dreamcast par exemple), il faut souvent recourir à un hack de la console : pour restaurer la sauvegarde, on sera obligé de recourir à un CD-ROM de 650 Mio en compressant si besoin certains données (textures, son) pour que ça rentre ou bien de modifier la console pour installer un [lecteur de carte SD](https://gdemu.wordpress.com/about/).

# Conclusion

TODO: proposer de débattre, des ouvertures sur d'autres sujets?
