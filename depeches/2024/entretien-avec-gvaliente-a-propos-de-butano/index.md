URL:     https://linuxfr.org/news/entretien-avec-alekmaul-a-propos-de-pvsneslib
Title:   Entretien avec GValiente à propos de Butano
Authors: devnewton
Date:    2024-02-12T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   1


GValiente développe [un SDK](https://github.com/GValiente/butano) pour créer des jeux pour la console [Game Boy Advance](https://en.wikipedia.org/wiki/Game_Boy_Advance) : [Butano](https://gvaliente.github.io/butano/).

Cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.

![Game Boy Advance](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Nintendo-Game-Boy-Advance-Purple-FL.jpg/640px-Nintendo-Game-Boy-Advance-Purple-FL.jpg?uselang=fr)

----

[Butano](https://gvaliente.github.io/butano/)
[Un article sur l'architecture de la Game Boy Advance](https://www.copetti.org/writings/consoles/game-boy-advance/)
[Le dossier de Grospixels sur cette console](https://www.grospixels.com/site/gameboy.php)

----

## Partie 1: présentation

### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?

Après des études d'informatique, j'ai travaillé dans plusieurs domaines autour du logiciel comme les pages web ou les applications graphiques Java/Swing.

Aujourd'hui je travaille plutôt en C et C++ dans l'embarqué, ainsi même si mon parcours professionel n'est pas directement lié aux jeux vidéos, mon boulot actuel en est plutôt proche.

Comme loisir, j'ai joué un peu avec RPG Maker 2K avant de commencer à programmer pour la GBA.

### Pourquoi le retrogaming est-il important pour vous ?

D'abord pour la nostalgie : être capable de jouer à nouveau aux jeux de votre enfance est très important pour tout le monde. Malheureusement ne pas pouvoir rejouer à de vieux jeux est quelque chose que nous sommes en train de perdre à cause des restrictions des jeux modernes (mode en ligne obligatoire, DRM...).

Ensuite, grâce aux émulateurs il est très facile de lancer sans problème des jeux faient pour la GBA il y a 20 ans. Si je les avais fait pour Mandrake 8.0 à la place, ce ne serait pas aussi facile de les tester aujourd'hui  sans recompiler du vieux code et autre.

## Partie 2: Game Boy Advance

### Comment en êtes-vous venu à vous intéresser à la Game Boy Advance ?

La GBA SP était un grand progrès par rapport au modèle original grâce à l'écran rétro-éclairé et la batterie intégrée, alors j'en ai acheté une dès qu'elle est sortie.

Les jeux 2D me manquaient après la N64 et la GameCube, alors pouvoir jouer à des classiques de la 2D comme Final Fight sur une console portable était génial.

Mais ce qui m'intéressait vraiment à propos de la GBA était la possibilité de créer des jeux grâce au HAM SDK et aux flashcarts.

### Qu’est-ce que cette console a de particulier ?

C'est la dernièr console 2D. Le système graphique de la GBA fonctionne comme les consoles 16 bits classiques comme la SNES ou la Megadrive, avec des sprites, des arrières plans...

Toutefois elle utilise un processeur ARM 32 bits tourant à 16MHz, alors il n'est plus nécessaire ou aussi important de programmer en assembleur pour avoir de bonnes performances.

En plus, je trouve plus "magique" de voir votre jeu tourner sur un écran d'une vieille console portable que sur un écran de télévision.

### Est-elle proche de la Game Boy ou de la SNES ?

Au niveau graphique, c'est comme une SNES avec plus de couleurs simultanées, plus d'arrière plans et beaucoup de sprites par scanline (proche d'une Neo Geo et en plus on peut leur appliquer une rotation !). Elle a aussi des modes "bitmaps" qui rendent le rendu "logiciel" plus facile. Les jeux 3D comme Doom sont beaucoup plus rapide sur la GBA grâce à ces modes. Malheureusement la résolution de l'écran est un peu trop basse (240x160 contre 256x224 pour la SNES par exemple).

Cependant, au niveau son, c'est pire que la SNES: la GBA a même le canal PSG que la Game Boy originale avec deux nouveaux canaux directs pour jouer des samples PCM. Avoir seulement deux canaux PCM demandent presque toujours de gâcher des tonnes de cycles CPU en mixage audio et même après cela sonne toujours pire que la SNES.

### Comment fonctionne l'affichage (PPU, écran LCD) ?

Comme je l'ai dit, cela fonctionne comme une SNES : vous avez un nombre fixe d'arrière plans et de sprites, vous les configurez en écrivant des registres. La GBA a aussi des interruptions HDMA et H-BLank, donc vous pouvez faire beaucoup d'effets "raster" comme le fameux mode 7 de la SNES.

Néanmoins, quelques limitations pénibles du PPU de la SNES ont été retiré, ce qui rends le PPU de la GBA plus facile à programmer. Par exemple, la GBA permets d'écrire en VRAM pendant le "V-DRAM" (quand le PPU rafraîchit l'écran). Cela permets d'utiliser toutes les tailles de sprites disponibles en même temps alors que la SNES ne permettait que deux tailles simultanément.

### La console peut-elle faire de la 3D ?

La GBA n'a pas d'accélération 3D matérielle, mais son CPU est assez rapide pour faire du rendu logiciel (à un faible taux de rafraîchissement). Il y a quelques techniques pour dessiner des polygones 3D avec des sprites 2D, mais cela vient avec des tonnes de limitations. Dans [Varooom 3D](https://gvaliente.itch.io/varooom-3d), j'ai utilisé des sprites 2D poour dessiner des lignes horizontales, ce qui m'a permis de dessiner quelques polygones non texturés à 60 images par seconde.

### Comment fonctionne le son ?

Je ne sais pas très bien comment fonctionne l'audio de la GPU, car je n'en ais pas eu besoin: il y a de très bonnes bibliothèques disponibles et j'ai préféré les intégrer plutôt que d'implémenter ma propre solution.

### Comment marche la rétrocompatibilité avec les précédentes Gameboy ?

Il y a 3 modèles de GBA disponible: GBA, GBA SP and GBA Micro. Seules les deux premières sont compatibles avec la Game Boy originale.
La rétrocompatibilité est transparent pour le développeur et la plutôt des fonctionnalités de la Game Boy ne sont pas disponibles en mode "natif": un jeu GBA ne peut pas utiliser le CPU de la Game Boy CPU par exemple.

### La machine possède une ROM interne, à quoi sert-elle ?

La GBA démarre depuis le BIOS, une petite ROM qui montre l'écran d'accueil et exécute le jeu après cela. Il a aussi quelques fonctions liés à l'énergie comme arrêter le CPU jusqu'au V-Blank ou mettre la console en veille. Enfin il propose quelques routines comme des fonctions mathématiques, mais je ne les utilise pas pour des questions de performances. Cela aide aussi à éviter les bugs d'émulation du BIOS.

### La GB possède un dispositif anti piratage, comment fonctionne-t-il ?

Je préfère vous renvoyer à l'article de [copetti.org](https://www.copetti.org/writings/consoles/game-boy-advance/#anti-piracy-homebrew) sur le sujet.

### Comment fonctionne le réseau (Game Boy Link) ?

Comme pour l'audio, je ne sais pas trop comment cela fonctionne, car j'ai préféré intégré une [bibliothèque](https://github.com/afska/gba-link-connection).
En général je préfère une bonne bibliothèque plutôt que passer du temps à implémenter une plus mauvaise solution.

### Les cartouches peuvent-elles embarquer des coprocesseurs ?

Bien sûr, mais le CPU est tellement puissant par rapport à ceux des consoles 16 bits, qu'il n'y en a pas souvent besoin. Le meilleur exemple d'une cartouche officiel avec un coprocesseur dont je me rappelle est la Play-Yan : elle semble embarquer un VideoCore 1 pour jouer des muisques mp3 et des vidéos mp4 depuis une carte SD.

### Les émulateurs sont-ils bons ?

Extraordinnaire. Les émulateurs GBA sont si bons que vous n'avez presque jamais besoin de tester sur du vrai matériel. Si votre jeu fonctionne sur la plupart des émulateurs modernes, alors votre jeu a toutes les chances de fonctionner sur une console réelle sans souci. D'ailleurs la plupart des membres actifs de [gbadev](https://gbadev.net/) ne possèdent même pas de GBA.

### Quels sont vos jeux commerciaux préférés sur cette console ?

Beacoup :
- des joyaux de Treasure comme Astro Boy Omega Factor et Gunstar Super Heroes ;
- d'autres jeux d'actions comme Ninja Five-0, Dragon Ball Advanced Adventure et Final Fight ;
- tous les Simphony of the Night like Castlevanias.
- les ports de  Doom ;
- bien sûr les classiques de Nintendo classics comme Wario Ware et Zelda the Minish Cap ;
- des RPGs bien connus comme Mother 3, Mario and Luigi et Final Fantasy I&II ;
- quelques RPGs moins connus comme Riviera et CIMA the Enemy.

### Quels sont vos jeux "homebrew" préférés sur cette console ?

Il y en a beaucoup aussi, mais mon préféré est de loin [GBA Microjam '23](https://gbadev.itch.io/gba-microjam-23): c'est une collection de mini jeux très amusants à la Wario Ware. 
Ce qui le rends très spécial, c'est que chaque minijeu a été fait par un membre différent de [gbadev](https://gbadev.net/), c'est un jeu "communautaire".

D'autres très bons:
- [Goodboy Galaxy](https://goodboygalaxy.com/) ;
- [Demons of Asteborg DX](https://neofidstudios.itch.io/demons-of-asteborg-dx) ;
- [GBADoom](https://github.com/doomhack/GBADoom), un portage opensource de Doom ;
- [SNESAdvance](https://archives.dcemulation.org/gba/www.snesadvance.org/www.snesadvance.org/compatlist.html) ;
- [OpenLara](https://github.com/XProger/OpenLara) ;
- [Super Wings](https://pdroms.de/files/nintendo-gameboyadvance-gba/superwings) ;
- [Symbol Merged](https://copyrat90.itch.io/sym-merged) ;
- [Anguna](https://www.bitethechili.com/anguna/)
- [Vulkanon](https://vulkanon.pqrs.org/) ;
- [Inheritors of the Oubliette](https://mahoushoujomagicalmoestar.itch.io/inheritors-of-the-oubliette) ;
- [Demoscene entries like all Shitfaced Clowns prods](https://www.pouet.net/groups.php?which=4843) ;
- et les jeux que j'ai fait bien sûr.

## Partie 2 : Butano

### Pourquoi créer un SDK aujourd’hui pour si vieux système ?

L'objectif de Butano était de pouvoir travailler avec le PPU de la GPU et le reste du système aussi facilement que possible sans perdre trop de cycles CPU. Et je pense que j'y suis arrivé : avec Butano, vous pouvez créer, afficher et détruire des sprites, des arrière plans, du texte, des effets raster et plus encore avec une seule ligne de C++.

Les prémisses de Butano était une bibliothèque interne à mes jeux. Je n'avais pas de plan pour rendre ça public à part faire quelques exemples et documentations.

Finalement je suis content d'avoir rendu ça public : le plus grand accomplissement de Butano est le grand nombre de [jeux de qualité fait avec](https://github.com/GValiente/butano?tab=readme-ov-file#also-made-with-butano).

### Est-ce que vous participez vous même à la création de jeux ?

Oui, Butano vient avec le code source de deux jeux que j'ai fait : [Butano Fighter](https://gvaliente.itch.io/butano-fighter) et [Varooom 3D](https://gvaliente.itch.io/varooom-3d).

### Quels ont été les difficultés pour créer Butano ?

Pour être honnête, je n'ai pas eu beaucoup de difficultés grâce au grand nombre de bibliothèques, tutoriaux, émulateurs et outils disponibles pour la GBA.

### Vous aimeriez vivre du développement de ce logiciel libre?

Bien sûr, mais à moins de travailler à plein temps sur un jeu homebrew à grand succès, c'est difficile voire impossible de vivre de ça.

### Est-ce que Butano gère les accessoires (e-Reader, WormCam, Play-Yan...) de la console ?

Il gère les accessoires les plus communs : SRAM, rumble et l'horloge temps réel / real time clock (RTC).

Pour les accéssoires plus exotiques, je pense qu'il est préférable d'utiliser d'autres bibliothèques.

### Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement de jeux Game Boy Advance ?

Premièrement, vous devez apprendre les basiques du C/C++ : la plupart des nouveaux connaissent uniquement des langages de haut niveau comme Javascript ou Python, malheuresement ils ont un peu trop lourd pour la pauvre GBA.

Après, vous pouvez suivre ce [beau guide](https://gbadev.net/getting-started.html) au lieu de suivre mes modestes conseils.

### Quels sont les outils pour créer/préparer des graphismes utilisable par Butano ?

J'utilise Gimp [Usenti](https://www.coranac.com/projects/usenti/) (un logiciel proche de MS Paint pour la GBA et notamment une gestion des couleurs 15 bits et des palettes). Toutefois, la plupart des outils permettant de produire des images indexés peuvent faire l'affaire.

Pour les cartes, j'ai aimé utiliser [Tiled](https://www.mapeditor.org/)

### Quels sont les outils pour créer/préparer des musiques et des sons utilisable par Butano  ?

[OpenMPT](https://openmpt.org/) est l'outil audio le plus populaire pour la GBA, les musiques étant générallement créées par un tracker. Il a aussi de bons outils pour travailler avec les samples. D'autres utilisent [hUGETracker](https://github.com/SuperDisk/hUGETracker) et [Furnace](https://github.com/tildearrow/furnace).

### Est-il possible de créer ses propres cartouches ?

Je ne suis pas juriste, mais comme Butano est sous licence zlib, tant que vous respectez cette licence et celles des autres dépendances, vous pouvez faire vos propres cartouches et même les vendre.

Je pense que ce que font la plupart des gens aujourd'hui, c'est d'acheter des cartouches pirates Pokémon pas chère et de les flasher pour y mettre leurs propres jeux.

### Pourquoi le choix de C++ pour ce SDK ?


Comme je l'ai dit, un langage de haut niveau avec ramasse miettes est généralement trop pour la GBA.

Entre C et C++, j'ai choisi ce dernier, car il permets de réduire grandement la quantité  de code sans gâcher trop de CPU.

Par exemple:
- les destructeurs de C++ permettent de ne pas avoir à écrire trop de code pour nettoyer les resources, ce qui est une source de bugs importante sur les grands projets GBA ;
- la GBA ne gère pas les nombres flottants en hardware, donc utiliser des nombres en virgule fixe est essentiel. Grâce à la surcharge d'opérateurs, C++ permets d'écrire des classes qui se comportent comme des nombres flottants.
- L'opérateur `constexpr` permets de générer et stocker des tables d'appels ou autres constantes en ROM sans avoir à utiliser d'outils externes.

### Est-ce qu'il existe d'autres SDK libres pour ces consoles ?

Il y a beaucoup de SDK pour GBA, mais malheureusement (à mon avis) la plupart sont de plus bas niveau que Butano.

Voici une liste de ressources (compilers, toolkits, libraries, etc.) pour la GBA: [resources GBA](https://gbadev.net/resources.html).

## Partie 3: pour finir

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

J'utilise généralement Windows à cause du boulot et de certains jeux PC, mais la plupart des programmes que j'utilise sont libres.

L'éditeur de code que j'utilise presque toujours est Qt Creator, il est génial pour C++.

A part les logiciels libres pour le développement GBA, j'uitlise Firefox, Notepad++, VLC, 7-Zip, LibreOffice, TortoiseGit, VirtualBox et bien sûr les émulateurs pour les vieilles consoles et bornes d'arcade.

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

Pour le développement embarqué, j'utilise GCC et les outils GNU. Pour les applications de bureau, j'utilise Qt avec MinGW.

GCC est aussi le compilateur le plus populaire pour le développement GBA, alors je n'aurais pas pu l'éviter même si j'avais voulu.

D'autres logiciels que j'utilise au travail: Thunderbird, Putty and WinSCP.

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

Ubuntu est bien pour le peu d'usage de Linux que je fais.

Mes logiciels libres favoris sont ceux avec lesquels je passe le plus de temps:: Firefox, Qt Creator, GCC, les émulateurs.

### Quelle question auriez-vous adoré qu’on vous pose ?

Mmh... non, rien ne me vient à l'esprit.

### Quelle question auriez-vous détesté qu’on vous pose ?

Pourquoi perdez-vous votre temps avec des consoles vieilles de 20 ans ?

Maintenant que j'y pense... J'aurais adoré qu'on me demande ça.
