
## Part 1: presentation

### Who are you, what is your history and how is related to video games ?

After studying computer science I worked in several software related fields, such as web pages, Java Swing GUI applications, etc.
Currently I work more with C and C++ on embedded devices, so although my professional background isn't at all related to video games, my current job is somewhat closer to them.

As a hobby I played around a bit with RPG Maker 2K before I started programming for the GBA.

### Why is retrogaming important to your ?

On the one hand there's the nostalgia factor: being able to play your childhood games again is something that should be very important to everyone.
Unfortunately, being able to replay old games is something we are losing with today's games due to always-online requirements, DRM, etc.

On the other hand, thanks to emulators it's very easy to run without problems the games I made for the GBA 20 years ago.
If I had made them for Mandrake 8.0 instead, it might not be so easy to test them today without rebuilding old code and so on.

## Partie 2: Game Boy Advance

### How did you become interested by the Game Boy Advance ?

The GBA SP was a big improvement over the original model thanks to the back-lit screen and the built-in battery, so I bought one as soon as it came out.
I missed 2D games after the N64 and the GameCube, so being able to play 2D classics like Final Fight on a handheld was a blast.

But what really interested me about the GBA was being able to make games for it thanks to the HAM SDK and flashcarts.

### How this console is special / unique ?

It's the last 2D console. The graphics system of the GBA works like that of classic 16bit consoles such as SNES and Mega Drive; with sprites, backgrounds and such.
However, it includes a 32bit ARM CPU running at 16MHz, so it isn't necessary or as important to program in assembler to achieve good performance.
Besides, for me it's much more "magical" to see your game moving on the screen of an old handheld than on the TV to which the console is connected.

### Is it close to Game Boy, SNES or other Nintendo consoles ?

Graphics wise, it's like a SNES 2: it has more simultaneous colors, more backgrounds and much more sprites per scanline (it's close to the Neo Geo, and you can rotate them!).
It also has bitmap video modes that make software rendering much easier. 3D games like Doom are much faster on the GBA thanks to them.
Unfortunately, screen resolution is a bit too low (240x160 vs 256x224 on the SNES for example).

However, sound wise it's worse than the SNES: the GBA shares the PSG channels of the original Game Boy with two new Direct Sound channels for playing PCM samples. 
Having only two PCM channels almost always require wasting tons of CPU cycles mixing audio, and after that is still sounds worse than a SNES.

### How does the display/graphic work (PPU, LCD screen) ?

As I said, it works like a SNES: you have a fixed number of 2D backgrounds and sprites and you configure them by writing to hardware registers.
The GBA also has HDMA and H-Blank interrupts, so you can show lots of raster effects like the famous mode 7 3D plane of the SNES.

However, some of the more annoying limitations of the SNES PPU have been removed, making the GBA PPU easier to work with.
For example, the GBA allows writing to its VRAM during V-Draw (when the PPU is refreshing the screen). 
It also allows you to use all available sprite sizes at once, while the SNES only allowed to use two sprite sizes at once.

### Can it display 3D ?

The GBA doesn't have 3D hardware acceleration, but it has a fast enough CPU to make 3D software rendering viable (at a low frame rate, but still).
There are also some techniques for rendering 3D polygons with 2D sprites, but with tons of limitations.
In Varooom 3D (https://gvaliente.itch.io/varooom-3d) I used 2D sprites to render horizontal lines, which allowed me to display a few flat 3D polygons at 60FPS.

### How does the sound/music work ?

I don't know much about how audio works on the GBA because I didn't need to: as there are already very good third-party audio libraries available, I preferred to integrate them than to implement my own solution.

### Is it backward compatible with previous Gameboy ? How ?

There are three GBA models available: GBA, GBA SP and GBA Micro. Only the first two are backwards compatible with the original Game Boy.
Backward compatibility is transparent to the developer, and most Game Boy features are unavailable in native mode: for example, a GBA game can't use the Game Boy CPU.

### There is an internal ROM, what is it's purpose ?

The GBA boots from the BIOS, a small ROM that shows the splash screen and executes the game after that.
It also has some power related functions such as halting the CPU until V-Blank happens or enabling sleep mode.
At last, it has some extra routines like mathematical functions, but I don't use them for performance reasons. It also helps to avoid BIOS emulation bugs.

### There is an antipiracy system, could you describe it ?

Not much, so I prefer to link to a copetti.org article: https://www.copetti.org/writings/consoles/game-boy-advance/#anti-piracy-homebrew

### How does the network work (Game Boy Link) ?
As with audio, I don't know much about how the cable link works because I preferred to integrate a third-party library (https://github.com/afska/gba-link-connection).
In general, if there's a good enough third-party library, I prefer to integrate it than to waste time implementing a worse solution.

### Can cartbridge embed coprocessors ?

Of course, but since the GBA CPU is so powerful compared to the 16bit consoles, there is not much need to embed them.
The best example of an official cart with a coprocessor I remember is the Play-Yan: it seems to embed a VideoCore 1 to play MP3 audio and MP4 video from a SD Card.

### How good are GBA emulators ?

Amazing. Modern GBA emulators are so good that you almost don't need to test your games on real hardware. 
If your game works on most modern emulators, it will most likely work on a real GBA without any problems.
In fact, many of the active members of the gbadev.net community don't even own a GBA.

### What are your prefered commercial games on this console ?

A lot:
* Treasure jewels like Astro Boy Omega Factor and Gunstar Super Heroes.
* Other action games such as Ninja Five-0, Dragon Ball Advanced Adventure and Final Fight.
* All the Simphony of the Night like Castlevanias.
* The Doom ports.
* Of course, Nintendo classics like Wario Ware and Zelda the Minish Cap.
* Well known RPGs such as Mother 3, Mario and Luigi and Final Fantasy I&II.
* Less known RPGs like Riviera and CIMA the Enemy.

### What are your prefered homebrew games on this console ?

There's a lot too, but my favorite by far is the GBA Microjam '23 (https://gbadev.itch.io/gba-microjam-23): it's a very fun Wario Ware like collection of minigames. 
What makes it special is that each minigame was made by a different member of the gbadev.net community, so it is a "community" game.

More good stuff:
* Goodboy Galaxy: https://goodboygalaxy.com/
* Demons of Asteborg DX: https://neofidstudios.itch.io/demons-of-asteborg-dx
* GBADoom, open source port of prBoom to the GBA: https://github.com/doomhack/GBADoom
* SNESAdvance, a SNES emulator for the GBA: https://archives.dcemulation.org/gba/www.snesadvance.org/www.snesadvance.org/compatlist.html
* OpenLara: https://github.com/XProger/OpenLara
* Super Wings: https://pdroms.de/files/nintendo-gameboyadvance-gba/superwings
* Symbol Merged: https://copyrat90.itch.io/sym-merged
* Anguna: https://www.bitethechili.com/anguna/
* Vulkanon: https://vulkanon.pqrs.org/
* Inheritors of the Oubliette: https://mahoushoujomagicalmoestar.itch.io/inheritors-of-the-oubliette
* Demoscene entries like all Shitfaced Clowns prods: https://www.pouet.net/groups.php?which=4843
* And the games I have made, of course.

## Part 2 : Butano

### Why create an SDK for an old system ?

The goal with Butano was to be able to work with the GBA PPU and the rest of the system in the easiest possible way without losing too much CPU cycles, and I think I succeeded: with Butano you can create, display and destroy sprites, backgrounds, text, raster effects and more with only one line of C++ code.

I guess at the beginning Butano started as an internal library to make my games. I didn't plan on making it public, let alone creating so much documentation and examples for it.
I'm very glad to have made it public: for me the greatest achievement with Butano is the large number of quality games that have been made with it (https://github.com/GValiente/butano?tab=readme-ov-file#also-made-with-butano).

### Do you create game yourself ?

Yes, Butano comes with the source code of the two games I've made: Butano Fighter (https://gvaliente.itch.io/butano-fighter) and Varooom 3D (https://gvaliente.itch.io/varooom-3d).

### What were the difficulties to create Butano  ?

To be honest, I haven't had many difficulties thanks to the large number of GBA related libraries, tutorials, emulators and other tools that are available.

### Would you like to live from this free/libre software development ?

Of course, but unless you work full time on a very successful homebrew game, it's hard or impossible to live from it.

### Does Butano support accessories (e-Reader, WormCam, Play-Yan...) ?

It supports the most common external hardware: SRAM, rumble and the real time clock (RTC).
For more exotic hardware, I think it's better to use other third-party libraries.

### What would you recommand to someone who want to make a GBA game ?

First, you should learn the basics of C and/or C++: most new users only know high level programming languages like Javascript or Python, and unfortunately they're a bit too much for the poor GBA.
After that, you could follow this nice guide instead of my poor advice: https://gbadev.net/getting-started.html

### What tools are usable to create/prepare graphics for Butano ?

I use GIMP and Usenti, a MS Paint like editor designed for GBA development, with 15bit colors and good color palette tools: https://www.coranac.com/projects/usenti/
However, most tools that can generate indexed images should work.
For working with maps, in the past I've enjoyed using Tiled: https://www.mapeditor.org/

### What tools are usable to create/prepare musics and sounds for Butano ?

OpenMPT (https://openmpt.org/) is the most popular audio tool used for GBA development, since music usually comes from a music tracker. It also has quite nice sample related tools.
Others use hUGETracker (https://github.com/SuperDisk/hUGETracker) and Furnace (https://github.com/tildearrow/furnace).

### Is it possible to create his own cartbridge ?

I'm not a lawyer and such, but since Butano is licensed under the zlib license, if you comply with it and with the licenses of its third-party dependencies (which are also very permissive, like MIT), you should be able to make your own carts with your game and even sell them without legal problems.

What I think most people do these days to have their own carts is to buy cheap Pokémon bootlegs and reflash the cart with their game.

### Why C++ for this SDK ?

As I said, high level programming languages with garbage collectors are usually too much for the GBA.

Between C and C++ I chose the latter because it allows you to greatly reduce the amount of required code without wasting too much extra CPU. 

For example:
* C++ destructors allow to forget about writing extra code for resources cleanup, a common source of bugs in large GBA projects.
* The GBA doesn't have hardware support for floating point numbers, so fixed point usage is vital. Thanks to operator overloading, C++ allows to write classes that behave like floating point variables.
* Modern C++ `constexpr` allows to generate and store lookup tables and other constant data in ROM without using external tools, reusing the same code used for the rest of the engine.

### Is there other free/libre software SDK for GBA ?

There are many more SDKs for the GBA, but unfortunately (in my opinion) most of them are lower level than Butano.
Here's a curated list of development resources (compilers, toolkits, libraries, etc.) for the GBA: https://gbadev.net/resources.html

## Part 3: the end

### What free/libre software do you use on your spare time, on which OS ?

I usually use Windows because of work and some PC games, but most of the programs I use are free.
The code editor I almost always use is Qt Creator, it's great for C++.
Apart from the mentioned open source tools for GBA development, some other free software I use is Firefox, Notepad++, VLC, 7-Zip, LibreOffice, TortoiseGit, VirtualBox and of course most emulators for old consoles and arcades.

### What free/libre software do you use at work, on which OS ?

For embedded development I use GCC and the rest of the GNU suite, and for desktop apps I use Qt with MinGW.
By the way, GCC is also the most popular compiler for GBA development, so I couldn't avoid it even if I wanted to.
Some other free software I use at work is Thunderbird, Putty and WinSCP.

### What is your favorite GNU/Linux distribution ? What are your favorite free/libre softwares ?

Ubuntu is nice for what little I do on Linux. 
I guess my favorite free/libre software applications would be the ones I spend the most time with: Firefox, Qt Creator, GCC and console and arcade emulators.

### What question would you have loved to be asked ?

Hm... I don't have anything that comes to mind.

### What question would you have hated to be asked ?

Why are you wasting so much time with 20 year old consoles?
Thinking about it... I would have loved to have been asked that.