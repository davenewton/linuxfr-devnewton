URL:     https://linuxfr.org/news/newton-adventure-evolue-sur-pc-et-debute-sur-android
Title:   Newton Adventure évolue sur PC et débute sur Android
Authors: devnewton
         Florent Zara et Xavier Claude
Date:    2012-05-26T20:04:35+02:00
License: CC By-SA
Tags:    newton_adventure
Score:   19


Abordé régulièrement dans nos colonnes, Newton Adventure est un jeu de plate-forme 2D où le joueur peut parcourir les niveaux
 
- soit de façon classique en faisant courir et sauter le héros, 
- soit changer la direction de la gravité pour le faire tomber lui ou ses ennemis et obstacles.


Sous licence BSD pour le code et CC-BY-SA pour les données, il est disponible pour Linux, MacOS X et Windows ou tout OS avec Java et OpenGL d'installé.   Cette dépêche vous fait part des évolutions du projet.


NdM : merci à devnewton pour son journal.

----

[Téléchargement de la dernière version de Newton Adventure](https://play.devnewton.fr)
[DLFP : Newton Adventure 1.3](http://linuxfr.org/news/newton-adventure-1-3)
[Journal à l'origine de la dépêche](http://linuxfr.org/users/devnewton/journaux/newton-adventure-evolue-sur-pc-et-debute-sur-android)

----


![Screenshot](http://tof.canardpc.com/view/d1703f37-8a46-415e-a403-c7e7052db271.jpg)


Nouvelles versions
==================
Lors de la [dernière dépêche](http://linuxfr.org/users/devnewton/news), il s'agissait de la version 1.3. Voici les nouveautés qu'ont apportés les dernières versions :


1.4
---
* une sauvegarde de la progression et un menu pour voir et choisir les niveaux ;
* de nouveaux niveaux "0.5" de difficulté intermédiaire pour ne plus rebuter les joueurs ;
* quelques bugs en moins et optimisations en plus.

Certaines nouveautés sont invisibles, mais destinées à une prochaine refonte graphique :
 
* l'intégration de nanim ;
* la possibilité d'animer n'importe quel élément du décor ;
* la gestion de plusieurs couches de décors.

1.5
---
Cette nouvelle version est destinée à corriger beaucoup de bugs trouvés ces derniers temps :
 
* affichage des pommes et des explosions limité à un élément ;
* blocage de la clé par les haches tournantes dans certains niveaux ;
* lags du clavier et de la souris ;
* impossible d'aller dans les niveaux bonus.

Portage sur Android
===================
Grâce au très généreux don d'un HTC Magic par Allucard, le portage sur Android a pu commencer comme la capture d'écran ci-dessous l'atteste :
![Screenshot android](http://tof.canardpc.com/view/6deb8bc3-36ff-43d7-b687-70eeaf4e634c.jpg)


Pour l'instant, les binaires pour cette plate-forme ne sont pas disponibles car la version actuelle est trop lente pour être jouable (1 ou 2 images par seconde max). J'ai d'ailleurs beaucoup de mal à trouver comment optimiser, je n'ai pas l'impression de surcharger, mais les mobiles sont des petites choses fragiles et il ne faut sans doute pas trop leur en demander !

Futur
=====


Le projet va continuer d'évoluer :
 
* j'espère pouvoir finir le portage Android si la puissance du HTC me le permet ;
* une refonte graphique est en cours ;
* j'étudie de nouvelles idées : mode deux joueurs, générateur de niveaux, nouveaux pièges, etc.

Contribuer
==========


Quand j'ai commencé le développement de Newton Adventure, je ne pensais pas recevoir de contribution, mais petit à petit je reçois une aide précieuse de plusieurs personnes. Le fait de beaucoup parler de mon travail et de développer des outils réutilisables comme nanim, libtiled-android et scoreserver aide certainement ! Si d'autres personnes souhaitent donner un coup de main, j'ai commencé un gros travail de documentation sur le wiki du jeu.


La première et la plus simple des contribution est bien sûr de jouer !

Liens
=====


[Téléchargement de la dernière version](https://play.devnewton.fr)
