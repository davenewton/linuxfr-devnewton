URL:     https://linuxfr.org/news/newton-adventure-1-3
Title:   Newton Adventure 1.3
Authors: devnewton
         Benoît Sibaud, Xavier Claude, patrick_g et baud123
Date:    2012-03-19T12:27:00+01:00
License: CC By-SA
Tags:    jeux_linux, jeu, jeu_vidéo, opengl, java et newton_adventure
Score:   24


La version 1.3 du jeu de plateforme libre Newton Adventure où la gravité est modifiable apporte des nouveautés qui le rendent plus accessible :

 * Les manettes de jeu USB sont gérées et configurables dans le menu "Options".
 * Dans les niveaux les plus grands, il est possible de collecter deux objets, une carte et une boussole, qui permette de débloquer la vue d'une mini-carte très utile pour se repérer.
 * Une nouvelle quête composée de 5 niveaux, hades, est disponible et propose de nouveaux éléments de jeu tels que les téléporteurs ou les serrures.


NdM : le développement est plutôt actif : les développeurs cherchaient des [bêta-testeurs en décembre dernier](http://linuxfr.org/forums/g%C3%A9n%C3%A9raltest/posts/cherche-beta-testeurs-pour-un-jeu-libre), la version 1.1 [est sortie](http://linuxfr.org/news/sortie-de-newton-adventure-11) quelques jours après et la version 1.2 [parue mi-février](http://linuxfr.org/moderation/news/newton-adventure-1-2). Le code est sous licence BSD et les données sous CC-By-SA.



----

[Site](https://play.devnewton.fr)

----

![Screenshot hades](hades.png)

