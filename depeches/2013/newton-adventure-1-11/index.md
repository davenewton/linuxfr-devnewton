URL:     https://linuxfr.org/news/newton-adventure-1-11
Title:   Newton Adventure 1.11
Authors: devnewton
         Xavier Teyssier, NeoX, Benoît Sibaud et ZeroHeure
Date:    2013-08-26T19:52:26+02:00
License: CC By-SA
Tags:    newton_adventure, jeux_linux, java et debian
Score:   53


Une nouvelle version de Newton Adventure, un jeu de plateforme 2D libre pour Linux, Windows ou Mac OS X.

Le but du jeu consiste à parcourir des niveaux en courant, sautant et faisant tourner la gravité. Pour passer au niveau suivant, il faut trouver une clef et l'amener à la porte de sortie en évitant les nombreux pièges et ennemis.

Plus d'infos en deuxième partie de dépêche.

----

[Site officiel](https://play.devnewton.fr)
[Vidéo du gameplay](http://www.dailymotion.com/video/xxxaka_newton-adventure-1-9-trailer_videogames)
[Bibliothèque LWJGL](http://www.lwjgl.org/)

----

# Nouveautés


Les nouveautés de cette version sont:


- une refonte du menu d'options avec un nouveau thème&nbsp;;
- l'utilisation d'une base de données de manettes de jeu pour les configurer automatiquement&nbsp;;
- le partage des scores via scoreserver est désormais inactif par défaut. Je ne pourrais plus calculer de statistiques pertinentes, mais c'est un choix plus logique pour le respect de la vie privée&nbsp;;
- le passage à la version 2.9.0 de la bibliothèque [lwjgl](https://www.lwjgl.org/), équivalent Java de SDL ou SFML utilisé par la plupart des jeux écrits dans ce langage comme le célèbre Minecraft&nbsp;;
- des optimisations qui permettent de rendre le jeu à peu près jouable sur un netbook atom avec GPU intel.


# Debian


J'ai fait un effort particulier sur l'empaquetage pour debian : j'ai créé une branche spéciale afin que le paquet .deb ne dépende plus désormais que de bibliothèques et logiciels présents dans la distribution stable.

# Cherche contributeurs sérieux


Outre les contributions les plus simples (traductions, rapports de bugs...), je cherche des développeurs Android/iOS/consoles pour réaliser des portages sur d'autres plateformes.


# Quelques captures d'écrans


![menu](menu.jpg)

![capture](039_base_game_zbonus_level3.png)
