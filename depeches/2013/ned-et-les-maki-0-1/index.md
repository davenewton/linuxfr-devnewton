URL:     https://linuxfr.org/news/ned-et-les-maki-0-1
Title:   Ned et les maki 0.1
Authors: devnewton
         al.jes, Pierre marijon, jymistriel, Lili-Lith, Benoît Sibaud, Nils Ratusznik, NeoX, ZeroHeure et Xavier Claude
Date:    2013-12-02T16:04:58+01:00
License: CC By-SA
Tags:    jeux_linux, sokoban, libre et jeu_vidéo
Score:   47


_Ned et les maki_ est un jeu vidéo libre présenté par les [Geeky Goblin Productions](http://geekygoblin.org/) (GGP), [devnewton](https://www.devnewton.fr) et Natir.

Inspiré par le _sokoban_, il s'agit d'un jeu de réflexion où le joueur doit pousser différents makis pour qu'ils rejoignent les assiettes correspondantes. Chaque maki possède son propre comportement&#8239;: les makis avocats se déplacent d'une case, les makis saumons glissent, les bleutés peuvent détruire des obstacles…

La première version jouable est disponible pour GNU/Linux, Windows et Mac&#8239;OS&#8239;X.



![menu](menu.jpg)


----

[Téléchargement](https://play.devnewton.fr/prototypes.html)
[Présentation du jeu chez les GGP](http://geekygoblin.org/ned-et-les-maki/)
[Forge](http://gitlab.pierre.marijon.fr/public/)
[Github](https://github.com/natir/ned-et-les-maki)
[Site officiel des Geeky Goblin Productions](http://geekygoblin.org/)

----

NdM : le pluriel de maki en français est normalement makis mais nous avons conservé le titre de l'auteur.


# Des nouvelles de Ned

## Un développement rapide

Après une [présentation du concept](https://linuxfr.org/users/aljes/journaux/gobelins-cherchent-geeks-pour-koala-masque) fin septembre et le recrutement de développeurs sur [LinuxFr.org](https://linuxfr.org/users/devnewton/journaux/maki-a-la-vapeur), le projet a démarré sur les chapeaux de roues.


Basé à la fois sur des technologies éprouvées (Java, OpenGL, Lwjgl, Tiled), des cadriciels à la mode (Artemis, Guice) et beaucoup d'emprunt à [Newton Adventure](https://play.devnewton.fr), le code a été rapidement mis au point tandis que graphiste et level designer travaillaient rapidement en parallèle.


L'objectif était de présenter une première démo jouable aux JM2L et d'obtenir des premiers retours&#8239;:


- les graphismes ont été bien accueillis, même si le choix de la vue isométrique perturbe dans un premier temps ;
- les commandes demandent à être étudiées plus en détails pour être plus intuitives ;
- les joueurs se trouvent rapidement bloqués&#8239;: c'est généralement la dure loi des _sokoban_, mais beaucoup de joueurs aimeraient pouvoir revenir sur le dernier mouvement et avoir ainsi un jeu plus accessible.

## La démo jouable


Pour ceux qui n'ont pas pas eu la chance d'assister aux JM2L, la démo est téléchargeable [chez les GGP](http://geekygoblin.org/ned-et-les-maki/) ou [chez devnewton](https://play.devnewton.fr/prototypes.html). Plus les retours d'utilisateurs sont nombreux, mieux c'est&#8239;!

Cette démo propose&#8239;:


- une introduction avec des illustrations&#8239;;
- 5 niveaux jouables&#8239;;
- les fonctions classiques&#8239;: réglage de la résolution, plein écran ou fenêtrée, configuration du clavier et des manettes de jeu…

![demo](demo.jpg)



À venir
=======


Les ajouts minimums prévus pour la version 0.2 sont&#8239;:



* une amélioration de la qualité du code (parce qu'en général on n'essaie pas de la dégrader)&#8239;;
* des raffinements pour l'écran-titre&#8239;;
* davantage d'animations&#8239;;
* des escaliers intéractifs pour passer au niveau supérieur&#8239;;
* des premiers niveaux plus didactiques.

Et les codeurs&#8239;?
======================


Suite au [premier journal présentant le jeu](https://linuxfr.org/users/aljes/journaux/gobelins-cherchent-geeks-pour-koala-masque), certains se sont inquiétés quant au devenir des développeurs qui rejoindraient l'aventure. Ils avaient tort.

Ceux qui on vu devnewton aux JM2L ont pu le constater&#8239;: les développeurs capturés sont bien nourris, ont le poil brillant, l’œil vif, tiennent une forme olympique et font leurs nuits (sauf les veilles de livraison, bien entendu).


D'ailleurs, s'ils font bien leur travail, ils ont droit à de jolies nimages, comme [celle-ci](http://tof.canardpc.com/view/0dcac4a9-2fdb-4c52-9927-d4000ff16160.jpg "Ned & Newton") ou [celle-là](http://tof.canardpc.com/view/e3bb20b7-0fee-46ab-b6a6-7e2c38183905.jpg "Fanservice!").
