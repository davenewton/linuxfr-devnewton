URL:     https://linuxfr.org/news/sortie-du-bureau-leger-xfce-4-14
Title:   Sortie du bureau léger Xfce 4.14
Authors: devnewton
         Benoît Sibaud, Davy Defaud et ZeroHeure
Date:    2019-08-13T09:22:56+02:00
License: CC By-SA
Tags:    xfce et troll
Score:   49


Xfce est un bureau léger pour UNIX. Son principal avantage est d’être « normal » : un menu pour lancer des applications, une barre de tâches, un gestionnaire de fichiers, un panneau de configuration… Il ne cherche pas à révolutionner le bureau, à en mettre plein la vue ou à supprimer des fonctionnalités à chaque version.

Après plus de quatre années de développement, la version 4.14 remplace la version 4.12.

----

[L’annonce sur le site officiel](https://xfce.org/about/news/?post=1565568000)

----

Captures d’écran
================
![Capture Xfce 4.14](4.14-1.png)


![Capture Xfce 4.14](4.14-2.png)

GTK
===
Le passage de GTK 2 à GTK 3 et de D-Bus GLib à GDBus étaient les objectifs de cette nouvelle version qui ont demandé beaucoup d’efforts.


Nouveautés
==========
Le gestionnaire de fenêtres gère maintenant la synchronisation verticale, les écrans à haute résolution ([HiDPI](https://wiki.archlinux.org/index.php/HiDPI)) et a un nouveau thème par défaut.
    
Le gestionnaire de fichiers, Thunar, propose maintenant des énormes et gigantesques vignettes et une navigation au clavier plus complète. Les gens qui stockent de la musique ou des photos de poneys furieux seront contents de pouvoir changer la vignette d’un dossier en mettant simplement un fichier `folder.jpg` à la racine pour faire apparaître la pochette d’un album photo ou une image de poney arc-en-ciel.
    
Le service de notification a maintenant un mode « Ne pas déranger » pour ne plus voir les invitations à discuter avec des Polonaises ou les insultes du chef qui râle, car la production est cassée.
    
Enfin, le projet s’est doté de son propre économiseur d’écran.
