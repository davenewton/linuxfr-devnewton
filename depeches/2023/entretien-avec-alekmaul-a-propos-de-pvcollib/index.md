URL:     https://linuxfr.org/news/entretien-avec-alekmaul-a-propos-de-pvcollib
Title:   Entretien avec Alekmaul à propos de PVCollib
Authors: devnewton
Date:    2023-05-04T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   37


Alekmaul développe [un SDK](https://github.com/alekmaul/pvcollib) pour créer des jeux pour la console [ColecoVision](https://fr.wikipedia.org/wiki/ColecoVision), cet entretien présente les raisons qui l’ont amené à s’intéresser à cette console.

![ColecoVision](https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/ColecoVision-wController-L.jpg/640px-ColecoVision-wController-L.jpg)

----

[PVCollib](https://github.com/alekmaul/pvcollib)

----

## Comment en êtes-vous venu à vous intéresser à la ColecoVision?

La Colecovision est la console de ma jeunesse, je n’avais hélas pas les moyens de me la payer mais un copain en avait une. On a joué pas mal de fois à Donkey Kong et Zaxxon dessus.

Quand j’ai vu que des choses existaient sur Colecovision avec les travaux d’Amy Purple (Daniel Bienvenu à l’époque), j’ai décidé de regarder comment je pourrais m’en inspirer pour développer des jeux et ainsi faire revivre cette étincelle que j’avais eu quand j’y joué il y a maintenant très longtemps.

Ainsi est né mon SDK pour Colecovision : [PVCollib](https://github.com/alekmaul/pvcollib).

## Qu’est-ce que cette console a de particulier ?

Elle posséde très peu de mémoire et faut faire avec !

32Kio pour les programmes (même si on fait des versions avec plus de mémoire maintenant avec les notions de bank switching) et surtout moins d’1Kio de RAM ! Et là, c’est sportif mais on y arrive ;)

## Comment fonctionne le processeur graphique (TMS9918) ?

Il fonctionne comme le processeur de la SNES mais avec un seul plan et une palette de couleurs qui ne peut pas être modifiée. On a aussi le droit à des sprites mais en une seule couleur.

C’est le même processeur graphique sur les ordinateurs de type MSX1 et sur le TI99-4A.

## Comment fonctionne la puce sonore (SN76489) ?

On programme des ports d’entrée / sortie pour faire du son.

L’avantage de la Coleco est d’avoir un BIOS (programme interne à la machine) qui aide à gérer le son en envoyant des commandes plus facilement via l’appel de fonctions en assembleur. Ca évite de programmer les entrées sorties à chaque fois.

## Quelle est votre manette préférée ?

La manette standard, bien difficile à prendre en main et à faire fonctionner, surtout pour changer de direction :D !

## Les émulateurs ColecoVision sont-ils bons ?

Oui, ils sont bons mais ne possèdent pas de mode aidant les développeurs. On peut citer BlueMSX qui est le plus connu, CoolCV de mon ami NanoChess aussi.

Le peu d’aide pour les développeurs et le fait qu’aucun émulateur ne supporte le processeur graphique supplémentaire F18A embarqué dans le Phoenix (machine à base de FPGA permettant de faire comme si on avait une vrai Colecovision) ont fait que j’ai développé mon propre émulateur que j’utilise tout le temps : [E.mult T.wo](https://github.com/alekmaul/emultwo). J’ai lassé tomber les autres. Je vérifie quelques fois sur BlueMSX quand E.mult T.wo ne fonctionne pas sur certains points (il est pas mal buggé encore \^\^).

## Quels sont vos jeux commerciaux préférés sur cette console ?

Je dirais Zaxxon et Donkey Kong.

## Quels sont vos jeux "homebrew" préférés sur cette console ?

Sydney Hunter de Nanochess est vraiment bien. J’aime pas les conversions par décompilation des jeux MSX ou SG-1000 par contre. Et il y en a pas mal sur Coleco hélas ...

## Pourquoi créer un SDK aujourd’hui pour si vieux système ?

Car on est jamais mieux servi que par soi même et y’avais pas de SDK à proprement parlé !

Il n’y a que des morceaux de librairies, sans vraiment de choses permettant de dissocier ce qu’il fallait pour coder les graphismes, le son, les manettes. Sans dissociation de la compilation, du linkage, il n'y avait rien aussi quand j’ai débuté pour gérer le bank switching, j’ai fais mes propres outils (disponibles dans pvcollib).

Ca a été la raison de réaliser PVCollib, qui fonctionne comme PVSneslib. ça m’aide si je dois voir comment porter un jeu entre ses 2 plates-formes.

## Est-ce que vous participez vous même à la création de jeux ?

Oui, j’ai fait pas mal de jeux sur cette console donc un Bomberman : RobeeBlaster ;) J’ai aussi réalisé une adaptation du jeu d’arcade Bagman qui fonctionne vraiment bien.

## Quels ont été les difficultés pour créer PVCollib?

Y’en a pas eu de trop, après avoir étudié la console, le kit est arrivé assez vite. J’ai mis un peu plus de temps à créer les outils et surtout E.mult T.wo qui en est à sa "je sais plus combien" de version :D !

## Est-ce que PVcollib gère les divers périphériques de la ColecoVision ?

Oui, PVCollib gère le volant par exemple. Il gère aussi le F18A, processeur graphique additionnel.

## Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement ColecoVision ?

Comme sur SNES, copier/coller de ma réponse ;) La difficulté supplémentaire est le peu de RAM disponible. Il faut vraiment se faire des noeuds au cerveau pour tenir dans moins d’1K de RAM.

Je suis assez fier de mon Bomberman, RobeeBalster qui permet avec si peu de RAM d’avoir un mode Story et un mode Battle à 2 !

J’avais même prévu le mode à 4 mais c’était pas terrible avec uniquement 2 manettes sur Colecovision, cette dernière n’ayant pas de multitap ou autre accessoire de ce genre (enfin, à ma connaissance). On utilisait des câbles en Y pour doubler chaque manette :D !

## Quels sont les outils pour créer/préparer des graphismes utilisable par PVcollib ?

Comme sur SNES, GraphicGale pour la conception des graphiques et un outil pour transformer les graphiques au format Coleco.

J’ai aussi un outil interne qui me permet d’optimiser les graphiques et les compresser comme je veux, les répéter d’un écran à l’autre. Il n’est pas "assez souple" pour être partagé à d’autres développeurs, j’aurais trop de questions tellement il est peu intuitif !

## Quels sont les outils pour créer/préparer des musiques et des sons utilisable par PVcollib ?

Là, y’a rien de dispo dans le KIT hélas, il faut les faire à la main et les transformer par quelque chose de lisible par la Colecovision :( On est toujours en attente d’un tracker pour cette console hélas.

Mon outil "peu intuitif" me permet quand même de transformer sans trop de débat du format MIDI vers le formation lisible par la Coleco.

## Est-il possible de créer ses propres cartouches ?

Oui, tout à fait. Au Canada [Collectorvision](https://collectorvision.com/) en fabrique vraiment beaucoup et a réalisé la plupart de mes jeux.

En France, [Côté Gamers]((https://cotegamers.com/shop/fr/)) est aussi de la partie, j’ai édité 2 jeux avec eux.

## Comment s’accommoder des faibles ressources ?

Il faut faire avec, optimiser et optimiser. Par rapport à la taille des programme, c’est surtout les graphiques qui prennent de la place.

On a donc des compressions assez puissantes pour réduire tout cela et le décompresser dans la mémoire vidéo de la Coleco.

On utilise par exemple du Pletter (algorithme générique) ou encore du DANx (DAN1 à DAN3, réalisés par Amy Purple).

Pour la RAM, on gère au plus juste les 8 bits d’un octets ou alors on utilise aussi en complément de la mémoire vidéo pour pallier au peu de RAM en stockant dans des zones non affichées à l’écran des données.
