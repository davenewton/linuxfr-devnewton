URL:     https://linuxfr.org/news/entretien-avec-luigiblood
Title:   Entretien avec LuigiBlood
Authors: devnewton 🍺
         gUI, Arkem et Ysabeau
Date:    2023-04-11T09:53:55+02:00
License: CC By-SA
Tags:    retrogaming, nintendo64 et super_nintendo
Score:   10


LuigiBlood est acteur de la scène Retrogaming, cet entretien revient sur ses projets et réalisations dans le domaine : traductions, rom hacking, ingénierie inverse, etc. C’est presque une histoire des jeux vidéos qu’il délivre à travers cet entretien.

![LuigiBlood avatar](yakumono_anim.gif)

----

[Le site de LuigiBlood](https://luigiblood.neocities.org/)
[L'architecture de la Nintendo 64](https://www.copetti.org/writings/consoles/nintendo-64/)
[Un dossier sur la N64](https://www.grospixels.com/site/n64.php)
[Un dossier sur la SNES](https://www.grospixels.com/site/snes.php)
[L'architecture de la SNES](https://www.copetti.org/writings/consoles/super-nintendo/)

----

## Partie 1 : présentation


### Qui êtes-vous, quel est votre parcours et en quoi est-il lié aux jeux vidéos ?

Je suis LuigiBlood, parfois connu sous le nom de Yakumono aussi, je ne parle pas trop de ma vie privée sur Internet en général, une sorte de règle que je me suis donné étant plus jeune déjà. Mais j’habitais en banlieue parisienne puis j’ai déménagé dans le sud plus tard, avec un parcours scolaire plutôt compliqué où j’avais du mal à m’intéresser à des matières et d’autres trucs que je supportais moins.

J’avais commencé à jouer aux jeux vidéos depuis je pense mes cinq ans, avec la Super NES qui était déjà considérée comme d’ancienne génération. Ma famille est plutôt fan de jeux vidéos en général et plutôt ouverte. La plupart des consoles que je possède actuellement viennent de ma famille, avec du Nintendo, Sega, Sony, Microsoft, mais la majeure partie du temps c’était plus Nintendo qui dominait nos intérêts de jeux. On n’était pas spécialement riches, mais on était assez aisés pour avoir toujours à peu près ce qu’on voulait au bout d’un moment.

À peu près dans le même temps, je commençais déjà à me fasciner pour l’informatique à regarder mon père faire des manipulations sur DOS et Windows, formatage, installation de drivers, etc, et au fur et à mesure je commençais à me familiariser avec tout ça on avait un vieux Packard Bell avec un Windows 95 puis un HP avec Windows XP, qui étaient plutôt les ordinateurs qu’on se partageait l’utilisation à l’époque avant d’opter pour d’autres ordinateurs dont j’avais un PC à moi avec Windows ME pendant très longtemps.


Avec Internet j’ai ensuite beaucoup appris de l’anglais, de la programmation, comment créer un jeu vidéo aussi qui était un sujet qui m’intéressait beaucoup déjà avec The Games Factory et RPG Maker qui étaient cools pour un enfant comme moi à l’époque, je voyais beaucoup du milieu amateur de la création JV. Puis j’avais vu le milieu du ROM Hacking qui me fascinait beaucoup aussi, c’était vraiment plutôt étrange de voir autant d’engouement d’une petite communauté qui se passionnait pour l’édition des vieux ROMs de jeux de vieilles consoles. Puis y a eu l’apparition de YouTube où j’ai commencé à connaître des choses parfois obscures sur Nintendo comme les fameux jeux Mario et Zelda sur [Philips CD-i](https://www.grospixels.com/site/cdi.php) et de les voir parodiés à outrance qui me faisaient beaucoup rire.

J’avais participé à l’extraction et conversion des données vidéo des cinématiques de ces jeux CD-i, avec les animations et les décors en séparés, histoire de bien améliorer ces parodies avec des _assets_ de haute qualité, puis au fur et à mesure je m’enfonçais de plus en plus dans l’obscur Nintendo jusqu’au point de non-retour.
## Partie 2 : N64 et 64DD


![N64 avec un 64DD](963px-64DD-Attached.png)


### Comment en êtes-vous venu à vous intéresser à la N64 ?


C’était une console avec laquelle j’avais déjà pas mal jouée, à peu près vers la transition vers la génération “128-bit” avec Super Mario 64, Mario Kart 64, Goldeneye et Smash Bros entre autres. Mais après que j’étais intéressé avec la Satellaview sur Super Famicom, la 64DD était déjà dans ma ligne de mire parce que j’avais passé beaucoup de temps sur Mario Paint sur Super NES et que j’étais très curieux de ses successeurs Mario Artist sur 64DD.

### Qu’est-ce que cette console a de particulier ?
C’est très complexe comme console, j’avais plus ou moins pris goût aux systèmes plus simples comme la Super Nintendo, j’ai dû m’habituer au fait qu’on devait tout charger dans la RAM même quand la ROM de la cartouche est en accès direct en mémoire parce que c’était trop lent.


### Que dire du processeur (NEC VR4300) ?


L’architecture MIPS est plutôt simple, mais, pour moi, l’enfer c’est que je confonds toujours le _delay slot_ à chaque branche donc je mets quasi toujours un nop après dans le doute, et le pire c’est le cache parce que j’avais pas affaire à ça avant, au point que ça m’a posé problème plusieurs fois, et la précision des calculs à virgule flottante laisse vraiment à désirer…


### Comment fonctionnent les processeurs graphiques (RSP, RDP) et audio ?


Je ne connais pas très bien le fonctionnement.


### Comment fonctionne l’audio ?


C’est en rapport avec le RSP mais je n’en sais rien.


**NDM : si vous connaissez bien cette console, contactez-nous pour une future interview !**


### Quelle est votre manette préférée ?

![Switch Pro Controller](309px-Nintendo-Switch-Pro-Controller-FL.jpg)


Certainement pas la manette Nintendo 64 ! Si je devais choisir une manette ça serait probablement le Switch Pro Controller, j’aurai pu choisir les Joy-Cons si le _stick drift_ n’existait pas parce que avoir deux parties d’une manette dans chaque main est assez confortable. Mais si je devais choisir une manette rétro ça serait la manette Super NES sans hésiter.

### Les émulateurs N64 sont-ils bons ?


On m’aurait posé cette question y a dix ans j’aurai dit absolument pas. Maintenant il y a surtout eu une prise de conscience que l’émulation N64 a beaucoup de problèmes, que ce soit dans la qualité de l’émulation même ou l’interface utilisateur, je me souviens avoir passé beaucoup de temps à me plaindre de ce dernier parce que les deux émulateurs connus, Project64 et [mupen64plus](https://mupen64plus.org/), ne viennent pas avec la meilleure configuration possible dès le départ, et c’est toujours pas le cas totalement.

Il y a eu beaucoup de développement sur l’émulation graphique de la N64, et comme la norme des émulateurs N64 c’est qu’ils utilisent toujours des plugins séparés, on peut généralement profiter de ça, mais mupen64plus a plus ou moins eu une renaissance avec les frontends et les forks comme Rosalie’s Mupen GUI, simple64 et mupen64plus-next (pour RetroArch). Mais je reste toujours à préférer Project64 bien qu’il soit uniquement Windows pour le moment, mais le développeur est totalement interessé de le rendre multi plateforme, mais il gère tout un projet quasiment tout seul et ça prend du temps. Il est le seul qui fait 64DD et avoir un debugger en même temps.

Y a eu un nouvel émulateur qui s’appelle [Ares](https://ares-emu.net/), par le regretté Near, développeur de bsnes et higan, Ares est un multi émulateur mais Near a voulu s’occuper de la N64 par lui-même pour le défi technique, et après son décès d’autres ont repris la où il l’avait laissé et ça se présente très bien, il est actuellement l’émulateur N64 le plus précis, bien que la compatibilité ne soit pas encore parfaite, mais ça avance.

En ce qui concerne l’émulation N64 officielle, l’émulateur N64 de Rare Replay et de Goldeneye sur Xbox semble faire à peu près son taf mais n’est pas parfait, mais en ce qui concerne Nintendo avec le service Switch Online, c’était le pire émulateur N64 que j’avais vu à son lancement en 2022, et bien qu’il y ait eu pas mal d’améliorations et de bugs corrigés, y a toujours des problèmes en ce qui concerne les contrôles absolument pas adaptés correctement pour les manettes actuelles, des problèmes qu’on sait déjà corriger sur les émulateurs PC.
### Quels sont vos jeux commerciaux préférés sur cette console ?
Je n’étonnerai personne si je mentionnais des jeux Nintendo en général, mais j’aime beaucoup Pilotwings 64, Kirby 64, F-Zero X et Super Smash Bros., certains de ces jeux je les ai connus plus tard, mais je peux aussi mentionner Road Rash 64, apparemment un peu mal aimé mais je le trouve sympa avec les différentes armes disponibles pendant les courses. Et j’aime beaucoup Mario Artist sur 64DD, vraiment une suite de jeux uniques.
### Quels sont vos jeux “homebrew” préférés sur cette console ?


Je faisais partie d’un jury pour la [N64brew Game Jam 2021](https://n64squid.com/homebrew/competitions/n64brew-game-jam-2021/), où j’ai pu essayer pas mal de jeux homebrew N64 réalisés pour l’occasion, je pense que la scène homebrew N64 à de beaux jours devant elle, j’avais beaucoup aimé [Wizard of the Board](https://github.com/danbolt/WizardOfTheBoard), un mélange entre un jeu d’échecs et un FPS plutôt sympa, [Mission Lost Control](https://n64squid.com/mission-lost-control/) est un jeu d’action et de stratégie où il faut prendre le plus de bases avec à la fois un personnage qu’on contrôle directement et des petits drones qui répondent à une stratégie qu’on leur donne, et [Voidblade](https://github.com/anacierdem/n64brew-jam) qui est un jeu assez étrange utilisant deux manettes pour avoir deux sticks pour contrôler deux “triangles” reliés à une “scie” et il faut esquiver ou scier tout ce qui tombe.

Il y en a eu d’autres des game jams N64 chez N64brew qui ont tous des jeux intéressants.

### Vous avez beaucoup travaillé sur le 64DD, pourquoi ?


La curiosité, je voulais absolument essayer Mario Artist sans débourser des centaines d’euros (et encore, maintenant c’est encore pire), j’avais vu pendant des années des promesses de personnes qui disent vouloir émuler la 64DD sans jamais que ça se fasse, puis y a eu une tentative de préservation des disquettes 64DD qui s’était soldé sur un échec en 2014, et j’ai eu un sentiment de ras-le-bol, et j’ai voulu tenter quelque chose par moi-même.

Je voulais faire un dumper 64DD, mais je n’y connaissais rien en développement N64. J’avais fait une tentative en utilisant libdragon pendant quelques mois, le SDK open source N64, mais je n’arrivais pas à faire quelque chose qui marchait. J’ai donc opté pour une option plus simple qui était d’utiliser le SDK officiel avec la bibliothèque 64DD officielle, modifiée pour l’occasion grâce a certaines personnes qui m’ont épaulé sur la tâche. En reprenant certaines démos SDK pour avoir un affichage, j’avais réussi à avoir un dumper qui fonctionne en deux semaines. Et le dernier jour de 2014, on avait tous les jeux 64DD préservés avec un dumper écrit totalement à l’aveugle sans pouvoir tester par moi-même, mais plutôt en testant avec la 64DD des collectionneurs qui sont dans des pays différents.

Puis Zoinkity, un hacker N64 de génie, avant même que l’émulation 64DD existe, a réalisé un portage des jeux 64DD vers le format cartouche N64 pour les flashcarts, donc il y a eu un moment où j’ai pu enfin jouer à Mario Artist, bien que c’était loin d’être parfait sur émulateur. Puis quelqu’un qui s’appelle Happy_ a émulé la 64DD pour la première fois sur MESS (qui aura fusionné avec MAME peu après), puis j’ai utilisé cette base de travail pour émuler la 64DD sur Project64, puis mupen64plus en partie, puis Ares. J’avais réécrit une bonne partie au fur et à mesure des années donc la logique du code n’a plus grand-chose à voir avec ce qu’il y a dans MAME, à force de comprendre le fonctionnement de la 64DD parfois avec quelques tests réalisés sur le vrai 64DD qu’entre-temps on m’en a fait don et je ne remercierai jamais assez pour la confiance qu’ils ont pour moi. J’ai pu réécrire le dumper deux fois finalement, et entre-temps j’ai pu avoir la confiance de réaliser un nouvel interface complètement de zéro.

### Pouvez-vous nous présenter Mario Artist ?


![Mario Artist](Mario_Artist_cover.jpg)

[Mario Artist](https://en.wikipedia.org/wiki/Mario_Artist), c’est une suite de logiciels qui fait suite à Mario Paint sur Super NES, un logiciel de dessin, d’animation et de musique. Mario Artist a eu 4 logiciels, Paint Studio, Talent Studio, Polygon Studio et Communication Kit.

Paint Studio c’est le logiciel de dessin classique, jouable jusqu’à 4 en même temps, avec un mode animation, le tout est vraiment très développé et détaillé, et avec un mode de monde 3D à explorer où il est possible de tout re-texturer et prendre des photos, je pense que j’aurais vraiment aimé étant gosse à l’époque.

Talent Studio c’est l’ancêtre des Miis, un éditeur de personnage simple et complexe à la fois avec parfois des options qui feraient rougir certains jeux actuels avec la création de personnage, puisque quasi tout est re-texturable, et y a un éditeur de film qui est assez limité mais qu’on peut en revanche animer chaque personnage quasi sans limites.

Polygon Studio c’est un éditeur 3D, avec un vrai éditeur de modèles 3D en polygones, et un éditeur simplifié conçu pour mettre plusieurs bouts prémâchés ensemble, et un éditeur pour donner un petit décor à la création 3D. Il est un peu connu pour être le jeu qui a inspiré directement Wario Ware avec un mode micro jeux dont Wario Ware en fait la copie conforme, et un mode _open world_ utilisant un véhicule créé avec l’éditeur simplifié, et on récupère d’autres parties de véhicule parfois plus puissants et permettant d’explorer plus loin, c’est un mode très sous-côté et que je recommanderais de faire un jour, bien que ce jeu demande une émulation graphique N64 beaucoup plus précise pour fonctionner correctement.

Communication Kit c’est plus pour partager ses créations sur le service Randnet qui était le service Internet. Quand on avait la 64DD c’était uniquement avec un abonnement Randnet. Il y a aussi un mode alternatif pour utiliser la cartouche de capture RCA (NTSC) qui était aussi compatible avec Paint Studio et Talent Studio mais celui-ci permettait des effets en plus.

J’avais pris le temps de traduire les trois premiers Mario Artist en anglais pour l’accessibilité, bien que mon Japonais ne soit absolument pas bien, mais vu qu’on est surtout dans la traduction de mots simples dans les menus, c’est pas trop difficile, Polygon Studio est celui qui m’a posé le plus de problèmes avec le mode open world, mais j’ai dû retraduire les Mario Artist jusqu’à trois fois au total, chacun avec une meilleure tentative de la technique qui rendait la traduction possible, étant donné que le plus compliqué c’était surtout la place qui manquait, chaque caractère utilisant deux octets j’ai pu réduire à un octet, ce qui double la place disponible et ça m’a fait gagner beaucoup de temps. Après , il y a eu les changements graphiques à faire mais Polygon Studio était super bien programmé comparé aux deux autres qui sont parfois vraiment codés en dur ce qui m’avait compliqué la tache parfois. Les sources des traductions que je réalise sont généralement en ligne.

### Quels “hacks” avez-vous fait sur Zelda ?


Pendant un temps, [Zoinkity](https://www.romhacking.net/community/803/) avait un peu exploré la compatibilité 64DD de Zelda Ocarina of Time, et j’ai voulu aller plus loin et j’ai pu comprendre comment remplacer le texte, puis les _maps_ du jeu. J’ai pu réaliser un test rapide sur le vrai hardware, ce dont le cache m’a vraiment posé problème que j’ai dû utiliser la zone mémoire non cache pour être sûr que ça marche, j’ai vraiment l’impression qu’il y a un vrai bug dans le code du jeu de base.

Puis, après ça, j’ai cherché si quelqu’un était intéressé pour réaliser un mod compatible 64DD, et le projet a pu être en chantier pendant quelques années. À la base ça aurait dû être qu’une seule _map_, mais c’était trop peu et pas assez impressionnant qu’on a préféré faire quelque chose de plus grand, mais j’avais imposé la seule condition de faire seulement une expérience de deux à trois heures tout au plus.

Ce projet deviendra Zelda 64 Dawn & Dusk, et est techniquement entièrement réalisé par Captain Seedy-Eye en version cartouche via la version debug de Master Quest qui avait été dumpé et fuité y a pas mal d’années. J’étais plutôt en charge de faire fonctionner tout ça sur 64DD, j’avais dit qu’il pouvait faire ce qu’il voulait et que c’était à moi de gérer tous les soucis si il faisait des choses qui étaient absolument pas prévu par la fonctionnalité 64DD de base.

J’avais, à la base, voulu réaliser le code pour la fonctionnalité 64DD de Zelda OoT en C, mais je savais pas trop comment gérer le _linker_ correctement et je me suis résolu à tout faire en assembleur MIPS parce qu’au moins je savais comment les choses seraient assemblées et que j’avais un contrôle total. À ce moment-là j’avais pas totalement compris les normes d’utilisation des registres du CPU par rapport au compilateur, je pense que j’aurais codé complètement différemment maintenant.

J’ai dû hacker le moteur du jeu en temps réel avec le code chargé via la 64DD par le jeu, la N64 n’avait pas de système de permissions, donc aucune limite. Le développeur avait réalisé beaucoup de choses, comme de nouvelles musiques, ce qui n’était pas du tout prévu par les développeurs originaux de Zelda donc j’ai dû écrire du code pour qu’il lise les données des musiques par la RAM au lieu de la cartouche ROM. Parfois, j’ai eu à précharger beaucoup de fichiers dans la RAM et les modifier, créer une table pour le remplacement de fichiers à charger en temps réel, une grosse cache pour les fichiers _maps_ du jeu parce que charger des _maps_ de la disquette en plein gameplay faisait stopper le jeu et arrêtait complètement la musique. Il y a eu beaucoup de détails comme ça avec lesquels j’avais eu affaire, heureusement que j’avais 4 Mo de RAM supplémentaire grâce à l’Expansion Pak qui était de toute façon requise par Nintendo pour toute interaction avec la 64DD.

En soi ce n’était pas forcément compliqué, je trouvais toujours une solution technique à chaque problème, mais comme je testais tout sur émulateur avec un debugger, il a fallu quand même que je trouve un moment pour le test sur le vrai hardware. Mais comme réécrire une disquette 64DD via une 64DD normale n’était pas possible à cause des protections, j’ai donc écrit les données sur la partie réinscriptible d’une disquette 64DD, et modifié le jeu de base pour indiquer de charger de cette partie à la place. Ça aurait du fonctionner, mais le jeu n’arrêtait pas de planter. Donc j’ai implémenté un retour par USB pour voir, et clairement mon code était exécuté, mais ça plantait quand même au final.

Il m’a fallu deux semaines pour comprendre que les données sur 64DD n’étaient pas alignées et que l’implémentation de la copie mémoire (bcopy) du jeu de base était complètement buguée sur ce point et ignorait certains octets quand ils n’étaient pas copiés correctement. Ce problème n’existe pourtant pas sur émulateur alors que c’est le même code ! Et là, j’ai compris que la précision des calculs à virgule flottante sur le CPU (ne me demandez pas pourquoi ils s’en servent pour de la copie mémoire, je n’en ai aucune idée) n’était vraiment pas très au point. Une fois que toutes les données étaient alignées, tout fonctionnait correctement, et j’ai pu tester le jeu sans problèmes. Il fallait ensuite faire en sorte que ça fonctionne sur toutes les versions NTSC de Zelda Ocarina of Time : il en existe trois versions, mais c’était surtout un travail de recherche des adresses de chaque fonction que j’utilisais. Le plus compliqué c’était d’adapter la musique pour chaque version puisque les données son n’étaient pas exactement les mêmes, mais à côté du problème d’alignement c’était facile.

Et puis j’ai finalement porté tout ça pour en faire des patchs pour les ROMs cartouche de chaque version en plus d’avoir une version disquette 64DD, ce qui en fait le seul mod Zelda Ocarina of Time a être compatible avec toutes les versions NTSC finaux du jeu.


Zelda 64 Dawn & Dusk est apparu presque au moment où les _mods_ Zelda 64 avaient une renaissance avec beaucoup de _mods_ de très bonne facture sont apparus ensuite, les gens pensaient que Dawn & Dusk était très beau, et Captain Seedy-Eye avait vraiment fait du très bon travail. En revanche le côté implémentation 64DD était assez ignoré, j’étais un peu dépité, j’avais vraiment passé du temps dessus et c'était la raison principale de pourquoi ce _mod_ existe. J’avais porté en version ROM parce que je me doutais bien qu’émuler la 64DD juste pour ça était pas forcément nécessaire, mais aurait pu être au moins apprécié par le fait que ça existe. Le lancement n’était pas forcément parfait non plus, à la fois parce que le jour d’après on avait déjà fait une version 2.0 avec une _map_ corrigée, mais j’ai créé une confusion en la nommant 2.0 faisant penser que le mod existait déjà bien avant, et que les instructions pour jouer à la version 64DD étaient pas claires et trop précises. Tant pis.

Quelques années plus tard, y a eu le mod Zelda Expansion, basé sur les _maps_ 64DD officielles du gigaleak de 2020, et le développeur m’a contacté pour en réaliser un portage 64DD, ce que j’ai fait finalement assez vite avec la même base de code de Zelda 64 Dawn & Dusk. Mais que j’ai mieux compartimenté, et finalement c’était même beaucoup plus simple parce qu’il s’agit juste de remplacement de _maps_ de donjon, ce à quoi était principalement conçue la fonctionnalité 64DD du jeu de base.

## Partie 3 : SNES et BS-X


![SNES et BS-X](Satellaview_system.png)

### Quels sont vos jeux commerciaux préférés sur cette console ?


Je suis pas spécialement un original parce que la plupart des jeux que j’aime bien sont des jeux développés par Nintendo, comme Super Mario All-Stars, même si c’est surtout un portage des quatre Mario en version 16-bit. Mais si je devais en choisir d’autres, je pourrais dire Sutte Hakkun, un jeu de puzzle un peu action qui était sur Satellaview puis sorti en cartouche plus tard, Marvelous - Another Treasure Island - que j’ai bien aimé aussi, le portage SNES de Detective Club Part II qui a récemment eu un remake sur Switch, Kirby’s Dream Course, Special Tee Shot, Pilotwings, Tetris Attack, et une bonne partie de ma vie d’enfant était passé sur Mario Paint avec la souris Super NES, d’où mon intérêt pour Mario Artist. Mais pour des jeux d’éditeurs tiers, je pense que Prince of Persia sur SNES est la meilleure version du jeu, Super Off-Road, récemment j’ai pu jouer a Demon’s Crest par Capcom qui est vraiment cool par le service Nintendo Switch Online, Operation Logic Bomb, Clock Tower, il y en a d’autres mais ce sont généralement les jeux Nintendo qui sont plutôt en haut dans mon classement.
### Quels sont vos jeux “homebrew” préférés sur cette console ?

Je n’en ai pas beaucoup joués, mais je pense à [Super Boss Gaiden](https://mag.mo5.com/97366/super-boss-gaiden-un-homebrew-pour-la-snes-et-son-cd-rom/), un _beat ’em up platforme_ assez humoristique avec le président de Sony qui bat tout le monde parce que la Playstation originelle qui était à la base une Super NES et une Super NES CD en un, avait été trouvé et blâme tout Sony pour ça. C’est rempli de références parfois obscures et ça peut se jouer jusqu’à trois.

### Quelle est votre manette préférée ?


![Manettes américaines et japonaises](800px-USA-SNES_-_JPN-SuperFamicom.png)


J’aime beaucoup la manette Super Nintendo, après j’ai une préférence sur la version colorée de la Super Famicom et Super NES PAL, mais en même temps j’aime bien la manette Super NES américaine pour les boutons X et Y concaves, malgré le violet que je trouve un peu dégueulasse.

### Vous avez beaucoup travaillé sur le SatellaView, qu’en dire ?


![SNES et Satellaview](Satellaview_with_Super_Famicom.jpg)

C’est quelque chose sur laquelle j’ai travaillé très très tôt, bien avant la 64DD, même pendant mon adolescence, alors que j’apprenais comment coder en assembleur. Je découvrais aussi BS Zelda, et les autres jeux Satellaview à peu près au même moment, mais je me rendais compte qu’il s’agissait d’un vrai service par satellite, avec pas mal d’interactions possibles, et j’avais posé la question si quelqu’un avait fait de la recherche sur comment reproduire le signal d’époque ou du moins refaire fonctionner le service. Personne ne le faisait donc je me suis dit de le faire moi-même, et c’est comme ça que je commençais mon premier projet de rétro-ingénierie, à base d’une documentation très basique sur le hardware Satellaview qui se fixait sous la console avec le port EXT, puis d’un émulateur où je passais mon temps à activer le trace log, un désassembleur automatisé parfois capricieux. J’ai vraiment appris sur le tas, j’ai sans doute perdu beaucoup de temps à faire des trucs inutilement longs, mais ça a porté ses fruits en terme d’apprentissage en général, puis le créateur des émulateurs _nocash_ vient avec un nouvel émulateur SNES et une documentation gargantuesque y compris la Satellaview, avec tout le système compris, et le système de machine virtuelle avec un bytecode de la cartouche BS-X qui sert d’accès au service.

Bien que j’ai appris plein de choses tout seul dans mon coin, il y a eu quand même beaucoup d’aides d’autres personnes surtout plus spécialisées au niveau hardware pour comprendre le fonctionnement de la cartouche BS-X par exemple qui utilise un mapper un peu spécial capable de re-mapper différentes parties entre la carte mémoire et la RAM de 512 Ko sur le bus SNES, ce genre de travail à ne pas négliger quant à l’époque la plupart du temps, c’était plutôt des trucs que des gens ont essayé de deviner à partir du code sans pouvoir le vérifier avec le vrai hardware.

J’avais fait un fork de bsnes et de snes9x pour avoir une meilleure émulation de la Satellaview avec des données externes, au final pour les abandonner au profit de directement contribuer au code officiel de snes9x, et au fork bsnes-plus. Certains ont utilisé cette base de travail sur leur émulateur comme Mesen. J’ai partiellement aidé au développement de l’émulation Satellaview sur le flashcart open source sd2snes, en particulier d’expliquer comment ça marche et de faire les données à “télécharger” pour faire marcher le plus de contenu possible, car certains jeux refusent de fonctionner sans des données supplémentaires à télécharger.


Les données externes en question on peut les générer grâce à SatellaWave, un logiciel que j’avais promis de refaire parce que la première version était vraiment trop compliquée à être utilisée, l’interface était imbitable, même pour moi qui a réussi à ne plus comprendre comment ça marche des années plus tard. Je m’étais résolu à ce moment de ne plus jamais faire de logiciels incompréhensibles par moi-même et de coder mieux, pour faire une version vraiment simple d’utilisation dans le futur. Ce qui a pris vraiment beaucoup trop de temps à être réalisé par procrastination + la vie privée qui changeait au fur et à mesure, mais j’ai pu développer ce logiciel de la façon que je voulais en C#, et les (rares) autres personnes qui s’en servent avaient mieux compris comment on s’en sert parce que j’avais tout misé sur l’expérience utilisateur que je prenais beaucoup plus au sérieux.


Depuis tout ça, j’aide beaucoup à la préservation de la Satellaview, parce que, finalement, ce sont tous les problèmes typiques d’un service dématérialisé. Sauf que, dans les années 90, très peu se sont intéressés à préserver ce contenu, donc, maintenant, le seul moyen de préserver, à notre échelle, c’est d’acheter les cartes mémoire Satellaview et espérer de voir du nouveau à l’intérieur, et qui ne soit pas corrompu en plus de ça, ce qui malheureusement, arrive de temps en temps… Ce n’est pas un service qu’on pourra préserver entièrement, donc la seule chose à faire c’est de prendre ce qu’il y a, même si ça coûte vraiment cher en ce moment. Je regarde les dumps avec un éditeur hexadécimal à la main, parfois aidé d’autres outils, je tente à mon échelle de planifier tout ça mais c’est généralement un projet communautaire où les gens dépensent comme ils peuvent.

### Vous avez traduit Marvelous - Another Treasure Island, quel est-ce jeu ?


![Marvelous - Another Treasure Island](Marvelous_boxart.png)

Marvelous c’est le premier jeu dont Eiji Aonuma, producteur actuel des jeux Zelda, était le directeur, juste avant avoir bossé sur _The Legend of Zelda : Ocarina of Time_. C’est un jeu qui était uniquement sorti au Japon, avec deux [préquelles](https://fr.wiktionary.org/wiki/pr%C3%A9quelle) sur Satellaview (Time Athletic & Camp Arnold), qui suit les aventures de trois enfants dans une colonie dans une île au loin, qui se joue presque comme un Zelda finalement, sauf que c’est beaucoup plus basé sur des énigmes à résoudre et le travail d’équipe entre les 3 protagonistes.

C’était déjà traduit par tashi et DackR y a quelques années, et le patch anglais était… disons que j’avais des soucis avec à la fois à cause d’un bug graphique vers la fin du jeu qui n’était pas trop grave. Aussi le script était vraiment bizarre par moments même si on comprenait très bien le jeu, mais c’était vraiment un jeu très sympa à faire.

J’ai voulu traduire les BS Marvelous pour Satellaview, j’ai fini par développer un système de rendu de texte à largeur variable ce qui était assez nouveau puisque le patch anglais du jeu principal n’en avait pas, puis je me suis dit que je pourrais faire peut-être quelque chose pour ce jeu en particulier, il y avait la source de la traduction anglaise, sauf que les outils sont parfois inutilisables… donc j’ai entrepris de réimplémenter exactement la même base mais pour des outils plus actuels et fonctionnels, puis de corriger le bug graphique que j’avais eu. Et ensuite d’améliorer la technique et la traduction, et, entre-temps, DackR et moi avons pu prendre contact et on s’est arrangés pour que je fasse uniquement la technique et DackR tout ce qui est texte et graphismes à changer.

On a un résultat qui, pour l’instant, n’est pas encore terminé. Mais, tout ce qui est technique est terminé et il ne manque vraiment plus qu’à faire les finitions sur le texte, les graphismes et le bêta test, mais DackR maintenant très occupé par sa vie. Actuellement le projet est un peu en pause, bien que je tente de faire en sorte que ça puisse sortir cette année. Après ça, j’aimerais pouvoir réaliser une version française du jeu. Je connais déjà des gens intéressés pour le faire. Je veux vraiment pouvoir rendre ce jeu plus accessible parce qu’il est vraiment sympa et pas trop difficile non plus.

### Pouvez-vous nous parler de Ongaku Tsukuru (Music Maker)


![Music Maker](ongaku_en1.png)

Ongaku Tsukuru fait partie de la série Tsukuru, dont fait partie la série RPG Maker déjà bien connu sur PC, mais ça date de plus de 30 ans ! Et c’était déjà présent sur les micro-ordinateurs de l’époque, mais la ASCII Corporation a publié des versions Super Famicom, avec RPG Tsukuru Super Dante, puis RPG Tsukuru 2, Sound Novel Tsukuru (un éditeur de jeu textuel avec quelques graphismes basiques) et Ongaku Tsukuru. Ces trois derniers sont compatibles Satellaview avec le système de carte mémoire, et le truc avec Ongaku Tsukuru c’est qu’on peut créer des musiques avec un éditeur en mode solfège très précis, puis on pouvait convertir les musiques dans un format compatible pour RPG Tsukuru 2 et Sound Novel Tsukuru, ce qui est plutôt cool, et c’est quelque chose qu’ils ont fait sur les successeurs sur Playstation 1 au Japon aussi.

J’ai commencé à traduire Ongaku Tsukuru un peu pour le fun et de passer le temps. J’ai dû passer énormément de temps de trouver comment je peux traduire sans prendre plus de place mémoire vidéo que je n’en ai, et que ça soit compréhensible surtout : c’est surtout ça le plus compliqué, surtout pour un éditeur avec pleins de menus différents avec des utilisations précises…

### Vous avez patché des jeux (Flash Back, Special Tee Shot), comment et pourquoi ?


![Special tee Shot](SNES_Special_Tee_Shot.png)

Souvent je patche parce que j’avais envie de le faire en premier lieu, je fais rarement des requêtes par exemple.


Pour le cas de Flashback sur Super NES j’avais fait un patch MSU1 qui permet d’avoir les musiques qualité CD et surtout de pouvoir remettre les musiques qui manquent, c’est un portage qui, d’une part, rame pas mal, et les cinématiques sont jamais à la bonne vitesse, et d’autre part, une bonne partie des musiques ont été amputées du jeu, donc j’ai voulu au moins corriger ce dernier problème. J’avais initialement réalisé ce patch sur le prototype du MSU1 qui s’appelait 21FX, et pour résumer le MSU1 pour celles et ceux qui ne savent pas, c’est une “puce” inventée de toute pièce à l’émulateur pour ajouter un accès à la volée de 4 Go de données additionnelles et 65536 pistes audio en qualité CD. Cette “puce” finira par être implémentée sur FPGA via le flashcart open source sd2snes (actuellement FX Pak Pro) et deviendra l’implémentation officielle du MSU1.

Special Tee Shot c’est un jeu de minigolf loufouque qui existe sur Satellaview en 1996, qui a eu un contexte de développement assez atypique puisqu’il était en développement en 1992 par Nintendo et HAL Laboratory, puis Kirby est sorti et il est devenu Kirby’s Dream Course en 1994, mais qu’ils ont quand même fini l’original pour Satellaview en 1996. On a réussi à dumper une version de 1992 via une cartouche vendue à 1 dollar trouvé par pur hasard, mais les contrôles du jeu sont vraiment beaucoup moins intuitifs pour la visée de la balle comparé à ce qui est sorti. Du coup j’avais réalisé un patch pour rendre les contrôles plus proches de la version Satellaview du jeu, parce que je trouve cette version du jeu plutôt intéressante d’une certaine manière.

## Partie 4 : pour finir


### Que dire sur vos autres projets ?


![Mario no Photopi](Mario_no_Photopi_video_game_box_art.jpeg)


J’aime bien faire des petits projets de temps en temps qui ne nécessitent pas trop de temps à faire, j’ai déjà un peu fait du travail sur l’émulation sur du hardware peu connu de la Nintendo 64 comme le lecteur de cartes SmartMedia (qui fait un peu ancêtre de la carte SD) pour Mario no Photopi, un logiciel de compositing photo pour N64 avec une cartouche très atypique avec deux slots SmartMedia.

![Nintendo e-Reader](541px-Nintendo_E-Reader.jpg)


Mes premiers projets de programmation sont en rapport avec l’e-Reader de la Game Boy Advance, qui est un lecteur de cartes à points, avec la même technologie que le QR Code mais beaucoup plus petits. Il existait des cartes avec des niveaux entiers pour Super Mario Advance 4, niveaux qui sont présents dans la version Wii U et Switch à présent. Ce projet s’appelait “Solar Magic Advance” en réference à Lunar Magic, l’éditeur de niveaux de Super Mario World par excellence. J’avais à la base réalisé un générateur de cartes pouvoir (qui file un power-up dans l’inventaire) sur Liberty Basic parce que je ne savais pas encore coder à l’époque, puis au fur et à mesure je prenais conscience des limitations techniques, je suis passé sur Visual Basic avec une version qui lit les cartes niveaux, puis j’ai appris le C# que j’utilise régulièrement sur beaucoup d’autres projets. Au final Solar Magic Advance n’est resté qu’un projet vaporware, le vrai éditeur de niveaux que j’avais prévu ne sera probablement jamais réalisé, bien trop occupé ailleurs pour le faire.

Au final j’aurai quand même réalisé quelques projets pour la GBA, comme un logiciel de communication pour Super Mario Advance 4 et Mario VS Donkey Kong pour transférer les données e-Reader, et un logiciel qui permet de prétendre à la GBA d’être un périphérique pour Nintendo 64 relié à une Game Boy Printer, jamais sortie mais totalement compatible et fonctionnel dans Mario Artist Paint Studio et Communication Kit. Il est même compatible avec une eventuelle Game Boy Printer Color, qui n’a jamais été annoncé.


### Vous avez de futurs projets en cours ?


Je retente une petite carrière de vidéos YouTube travaillées, mais ça prend un peu de temps, ça sera ultra niche, je ne cherche pas forcément une certaine popularité dans ce milieu, mais je veux pouvoir parler de sujets qui me passionnent sans filtre. La première grosse vidéo sera sur l’accessoire [Turbo File Twin](https://snescentral.com/article.php?id=0908) pour la Super Famicom, mais ça reste assez demandeur donc il n’est pas impossible que je chercherai à faire une vidéo plus courte et facile à réaliser avant.

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?


Je suis majoritairement un utilisateur de Windows, mais, des logiciels dont j’ai eu beaucoup d’utilité c’est je pense [bass](https://github.com/ARM9/bass) qui est un logiciel à la base développé par Near puis forké par un autre qui se nomme ARM9, et ça permet d’assembler du code assembleur très très bas niveau, à la base ça faisait du 65c816 pour SNES et du THUMB pour ARM7TDMI pour GBA (pour la traduction anglaise de [Mother 3](https://www.grospixels.com/site/mother3.php), entre autres), puis ce fork a vraiment été plus loin dans le délire pour permettre encore plus d’architectures, et ça me sert pour tous mes projets rétro comme les ROM hacks et les traductions de jeux.

J’ai déjà utilisé [Blender](https://www.blender.org/), [Inkscape](https://inkscape.org), Audacity, pour des trucs parfois plus créatifs, [OpenSCAD](https://openscad.org/) à un moment pour faire des modèles 3D à imprimer. J’utilisais MadEdit et maintenant son fork [wxMEdit](https://wxmedit.github.io/) en tant qu’éditeur texte basique et surtout hexadécimal parce qu’il reconnaît vraiment beaucoup d’encodages de caractères différentes dont le [EUC-JP](https://en.wikipedia.org/wiki/Extended_Unix_Code) qui est plutôt abandonné dans beaucoup d’éditeurs alors que j’en ai parfois besoin quand je m’attelle à des trucs vieux qui s’en servent.

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?


Tout est un peu entremêlé, donc je n’ai pas vraiment de réponse différente à donner.


### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?


J’ai plutôt tendance à utiliser Kubuntu juste pour l’interface KDE qui se rapproche de Windows, mais c’est vrai que j’ai pas pour habitude d’utiliser Linux en dehors du Windows Subsystem for Linux en ce moment, et via Docker parce que l’environnement de développement des distros Linux en général sont quand même bien supérieures à Windows, mais c’est pas évident de changer des habitudes qui sont restées depuis l’enfance.


### Quelle question auriez-vous adoré qu’on vous pose ?


Honnêtement si on me pose des questions sur la Satellaview et la 64DD ou tous mes autres sujets de prédilections, je reste plutôt heureux de répondre.
### Quelle question auriez-vous détesté qu’on vous pose ?


“Pourquoi prends-tu autant de temps pour faire quelque chose ?” – Parce que même si je fais quand même pas mal de trucs, je reste aussi un immense flemmard et procrastinateur, et j’ai rarement eu une réponse satisfaisante à donner.


![Yakumono](yakumono.png)
