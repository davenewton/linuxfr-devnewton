URL:     https://linuxfr.org/news/entretien-avec-johannes-fetz-a-propos-de-jo-engine
Title:   Entretien avec Johannes Fetz à propos de jo-engine
Authors: devnewton 🍺
         Benoît Sibaud
Date:    2023-05-04T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   37


Johannes Fetz développe [un SDK pour créer des jeux pour la console Saturn](https://www.jo-engine.org/), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.


![Console Saturn, source Wikimedia, © Evan-Amos 2011](320px-Sega-Saturn-Console-Set-Mk1.png)

----

[Jo Engine](https://www.jo-engine.org/)
[Un article présentant le hardware de la Saturn](https://www.copetti.org/writings/consoles/sega-saturn/)
[Un podcast sur l’histoire de SEGA](https://cpu.dascritch.net/post/2020/06/11/Ex0139-Sega%2C-game-over-sur-console)
[Un site consacré à SEGA et ses consoles](https://www.sega-16.com/)
[Le dossier de grospixels sur la Saturn](https://www.grospixels.com/site/saturn.php)

----

## Partie 1: présentation


### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?


Initialement, j'ai commencé ma carrière dans la mécanique de précision jusqu'à l'université. Puis, j'ai fait un certain nombre de jobs en intérim pour payer mes études supérieures et je me suis rendu compte que ce milieu (industrie de la production mécanique) n'était pas pour moi. Je me suis donc rabattu sur ma passion, l'informatique. J'ai commencé la programmation pendant mes études de mécanique après le collège avec des machines à programmation numérique (tour, fraiseuse) et dans mon temps libre, j'ai écrit des petits programmes en BASIC. Plus tard au lycée, j'ai appris la programmation en langage C avec des livres. Fait assez surprenant, j'ai eu Internet pour la première fois très tard quand j'ai commencé l'université. Par conséquent, j'empruntais souvent la connexion Internet du lycée à la bibliothèque ou dans les cybercafés. Après ma première année d'université, j'ai cherché une école de programmation et je me suis inscrit à l'Epitech où j'ai obtenu mon diplôme après 5 ans (promotion 2011). En sortant d'école, je n'ai pas eu besoin de chercher du travail, car j'étais déjà en CDI depuis 2009 chez Eureka Technology (éditeur du TMS, Dispatch) et j'y suis toujours. Bien sûr, mon poste a évolué avec le temps. Je suis aujourd'hui responsable des développements et membre du CODIR de l'entreprise. Par conséquent, mon parcours n'est pas du tout lié aux jeux vidéo.



## Partie 2: La Saturn


### Comment en êtes-vous venu à vous intéresser à la Saturn ?


J'ai acheté la console en mai 1996 de manière fortuite sur une brocante en Seine-et-Marne pour 250 francs avec les jeux ManxTT Superbike et Sega Rally. À l'époque, j'avais une Sega Master System et une Nintendo NES, alors j'ai fait un bond technologique important. J'ai ensuite acheté beaucoup de jeux et surtout j'ai eu la chance qu'on me parle de la cartouche permettant de jouer aux jeux japonais. J'ai eu par la suite plusieurs consoles, dont la PlayStation et la Nintendo 64, mais la Sega Saturn a gardé une place importante pour moi.


### Qu'est-ce que cette console a de particulier ?


Si on la compare avec les consoles de la 5ème génération, la Playstation de Sony par exemple, la principale différence, c'est que Sega Saturn était basée sur une architecture à double processeur et conçu pour être un monstre de puissance pour la 2D, car de nombreux jeux populaires étaient encore en 2D à cette époque. L'intégration de la 3D dans le projet de conception de la Saturn est arrivé tardivement suivant moi peut être la pression de la sortie de sa concurrente, la Playstation. En somme, l'architecture de la Sega Saturn est exotique ce qui a malheureusement posé  beaucoup de problème aux développeurs. C'est dommage, car la Saturn était capable de traiter des graphismes 2D et 3D plus avancés que la PlayStation.



### Que dire de ses processeurs (Hitachi SH-2) ?


Le SH-2 est un processeur RISC (Reduced Instruction Set Computing) 32 bits  développé par Hitachi dans le début des années 90 pour faciliter le traitement d'images et les calculs scientifiques (idéal pour le jeu vidéo). Il possède deux unités de traitement entièrement indépendantes, chacune capable de traiter des instructions simultanément. Ceux de la Sega Saturn ont une fréquence de 28.6364 MHz.


![Le processeur SH-2, source wikimedia, © GMPX](749px-Denso-SH2.jpg)


*Le processeur de la Saturn fait partie de la famille [SuperH](https://en.wikipedia.org/wiki/SuperH).*

### Comment fonctionne les processeurs graphiques (VDP1, VDP2) ?


Le VDP1 était responsable du traitement des graphismes 2D et des effets de transparence. Il était capable d'afficher des sprites et des polygones texturés (500 000 polygones texturés par seconde). On pouvait programmer des listes d'affichage, appelées "commandes", qui contenaient les informations nécessaires pour afficher les objets à l'écran. Cette partie est facilement visible dans le code du Jo Engine. Le VDP1 pouvait ensuite les traiter et les afficher en temps réel. Le VDP2, quant à lui, était responsable de la génération d'images de fond et de la gestion des plans de superposition. Il pouvait afficher plusieurs couches de graphismes en même temps (4 plans simultanés), chacune avec ses propres effets de transparence et de distorsion. Le VDP2 pouvait également effectuer des opérations de zoom et de rotation sur les plans d'affichage, ce qui permettait aux développeurs de créer des effets de caméra intéressants. Les deux processeurs graphiques travaillaient en étroite collaboration pour générer les images à l'écran. Le VDP2 envoyait des instructions au VDP1 pour lui dire où afficher les objets à l'écran, tandis que le VDP1 pouvait envoyer des données de texture et de couleur au VDP2 pour générer des arrière-plans. Bref, c'est assez complexe et cette complexité de programmation et leur coût ont également rendu la conception de jeux pour la Saturn plus difficile que pour d'autres consoles de l'époque.



### Comment fonctionne l'audio ?


La Sega Saturn avait un processeur audio intégré appelé "SCSP" (Sega Custom Sound Processor) basé sur une architecture RISC et disposait de 32 canaux audio. Le SCSP était conçu pour offrir une qualité audio supérieure à celle des autres consoles de l'époque, avec une gamme de fonctionnalités avancées tels que la réverbération, la distorsion et le chorus, qui pouvaient être appliqués aux sons en temps réel pour créer des effets sonores intéressants. En plus du SCSP, la Saturn avait également une puce audio "PCM" intégrée.


### Quels services fournissent le BIOS et l'OS de la Saturn ?


En dehors, des éléments standards présents sur toutes les consoles du même type, on a la possibilité de lire des CD audios et des films au format VCD via une carte d'extension.


### Quelle est votre manette préférée ?


J'aime bien la manette standard de la Sega Saturn, mais les manettes d'aujourd'hui sont meilleures. Personnellement, j'utilise principalement la manette Xbox même sur PC.


![Clone de la manette Saturn](640px-8BitDo_M30.png)


*Le design manette Saturn est populaire dans le milieu du retrogaming. Certains constructeurs comme 8BitDo ou Retrobit l'ont donc cloné.*

### Les émulateurs Saturn sont-ils bons ?


Oui et il y en a plein qui sont fonctionnels. SSF est sans doute le plus abouti (à l'exception de son interface), mais [Yabause](https://yabause.org/), [Mednafen](https://mednafen.github.io/), [YabaSanshiro](https://wiki.recalbox.com/fr/emulators/consoles/saturn/libretro-yabasanshiro), [Kronos](https://fcare.github.io/) fonctionnent très bien. Et il y a même des nouveaux qui arrivent, comme Nova. Il y a même des consoles comme la Polymega qui permettent de jouer nativement aux jeux Sega Saturn.


![La console Polymega](Console-01_f93a0f5d-0ef2-4eb7-b034-fd017b47b35e_600x.png)


*La console Polymega tourne sous Linux, mais malheureusement il s'agit d'une distribution privatrice.*

### Quels sont vos jeux commerciaux préférés sur cette console ?


Il y a vraiment trop de bons jeux et souvent exclusifs à la console, mais je peux en citer plusieurs : Radiant Silvergun, la série des Panzer Dragoon, Guardian Heroes, Burning Rangers, Sega Rally, etc.


![Burning Rangers](https://upload.wikimedia.org/wikipedia/en/7/75/Burning_Rangers_cover.jpg)


*Ecouter le [générique de Burning Rangers](burning_hearts.mp3) chanté par [Takenobu Mitsuyoshi](https://en.wikipedia.org/wiki/Takenobu_Mitsuyoshi) donne plus de gniaque qu'un verre de jus d'orange et 42 cafés.*

### Quels sont vos jeux "homebrew" préférés sur cette console ?


- Unreal Sega Saturn (aka Hellslave)
- Sonic Z-Treme
- Bridget Bishop
- Zygo 2D & 3D
- Cubecat
- Sky Blaster
- The case of the city botucaiba
- et d'autres que j'ai pas cité.


###  Partie 3: jo-engine


![jo-engine éditeur de carte](map_ssf.png)


*Le sdk vient avec un éditeur de carte 2D.*

### Pourquoi créer un SDK pour un si vieux système ?


C'est parti d'un petit défi technique personnel et de la curiosité à la base. La console avait cette réputation d'être l'une des plateformes les plus complexes pour programmer de l'histoire alors forcément ça a capté mon attention, en plus de la nostalgie que j'ai pour cette console. De plus, en matière d'architecture exotique, j'ai déjà joué avec le processeur CELL de la Playstation 3.


Quand j'ai commencé à étudier la plateforme, il n'y avait pas beaucoup de jeux homebrew, à l'exception de quelques démos techniques. D'ailleurs, je suis parti de là pour commencer avec le kit SaturnOrbit et ses démos.


J'ai ensuite décidé de commencer à faire un petit Sonic et j'ai posté une vidéo sur [Youtube](https://www.youtube.com/watch?v=y_iELL-o35U).


J'ai reçu plusieurs retours très positifs et j'ai voulu aller plus loin. J'avais réussi à obtenir de bons résultats techniques et j'ai décidé de les partager avec tout le monde en créant un SDK ouvert, doté d'un niveau d'abstraction élevé qui permet de créer facilement des homebrews pour la console. Malheureusement, j'ai dû mettre le développement de mon propre jeu de côté pour libérer du temps.


### Quelles ont été les difficultés pour le concevoir ?


Beaucoup ! 😊


Mais en premier lieu, c'est clairement le manque de documentation qui m'a contraint à coder beaucoup d'outils pour inverser l'architecture de la machine et comprendre comment utiliser chaque registre, chaque zone mémoire, chaque instruction. Heureusement, j'ai eu de l'aide de la communauté à certains moments. En second lieu, ce qui a été difficile, c'est d'abstraire l'architecture matérielle au maximum afin de laisser le développeur se concentrer sur son jeu.


### Quelles sont les fonctionnalités de jo-engine ?


Aujourd'hui il y en a beaucoup en particulier sur la 2D et plein d'exemples pour commencer.


### Est-ce qu'il gère divers accessoires (3D Pad, multitap, clavier, souris, pistolet, Video CD, modem...) ?


Oui à l'exception du modem et du pistolet. Pas de difficulté en particulier, mais j'ai pas eu de demande de fonctionnalité sur ça.


![3D Pad](524px-Sega-Saturn-3D-Controller.jpg)


*Le 3D Pad est une réponse à la sortie de la Nintendo 64 avec ses sticks analogiques.*

### Est-ce que vous participez à la création de jeux Saturn ?


Je viens souvent en aide aux développeurs de homebrew. J'ai un gros projet de jeu (pas le Sonic), mais je peine à trouver le temps pour bosser dessus.


### Est-ce que des jeux amateurs ou commerciaux sont développés avec jo-engine ?


Oui, il y en a plein. Il y a même un concours de programmation chaque année où plusieurs nouveaux jeux sont révélés : récemment, il y a eu le "SEGA Saturn 28th Anniversary Game Competition" dont le gagnant, Francisco J Del Pino Questa (aka Fran Matsusaka) a créé le jeu [Bridget Bishop](https://sites.google.com/site/egstudiogames/bridget-bishop) en utilisant le Jo Engine.


![Capture d'écran de Bridget Bishop](Bridget_Bishop.png)

### Quels conseils donnez-vous aux gens qui se lancent dans le développement de jeux Saturn ?


Je conseille de commencer par développer un jeu 2D pour se faire la main avant de s'aventurer dans la 3D. Il y a une communauté petite mais très active (majoritairement francophone en plus). N'hésitez pas à parler de votre projet et à demander de l'aide.


### Est-ce que vous aimeriez vivre de ce projet ?


Non. La satisfaction de voir des nouveaux projets sur la Sega Saturn (et pas forcément qu'avec le Jo Engine) me suffit largement.


## Partie 4: pour finir


### Que dire sur vos autres projets ?


J'ai un éditeur de modèle 2D/3D dédié à la Sega Saturn que j'ai commencé, mais je manque de temps en ce moment.


### Vous avez de futurs projets en cours ?


J'ai un jeu que j'aimerai bien réaliser avec Sega et en support physique, mais comme pour l'autre projet, je manque de temps. Pour l'instant je privilégie mon parcours professionnel et ma vie de famille.


### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?


Dans mon top 5 :


- GCC sous Linux principalement
- Paint.Net sous Windows
- Notepad++ sous Windows
- Emacs sous Linux
- VLC sous Windows et Linux


### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?


Dans mon top 3 :


- Notepad++ sous Windows
- Paint.Net sous Windows
- 7-Zip sous Windows


### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?


J'ai utilisé plusieurs distributions au fil des années (Mandrakelinux/Mandriva, Debian, Ubuntu, Arch Linux, etc...) ainsi que beaucoup de distributions BSD. Cependant, aujourd'hui, je suis resté sur Ubuntu. D'ailleurs, c'est la version serveur qui est présente sur le serveur dédié de Jo Engine.


En ce qui concerne les logiciels libres, je pense que mes préférés sont ceux que j'utilise le plus (Notepad++, VLC, Paint.NET, etc...).


### Quelle question auriez-vous adoré qu'on vous pose ?


Je n'ai rien qui me vient en tête.


### Quelle question auriez-vous détesté qu'on vous pose ?


Je n'ai rien qui me vient en tête non plus.
