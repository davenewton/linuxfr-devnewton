URL:     https://linuxfr.org/news/entretien-avec-kannagi-a-propos-de-ngdk
Title:   Entretien avec Kannagi à propos de NGDK
Authors: devnewton
Date:    2023-05-04T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   37


Kannagi développe [un SDK pour créer des jeux pour la console Neo Geo](https://github.com/Kannagi/NGDK), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.


![Console Neo Geo, source Wikimedia, © Evan-Amos 2012](https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Neo-Geo-AES-Console-Set.jpg/640px-Neo-Geo-AES-Console-Set.jpg)

----

[Neo Geo](https://github.com/Kannagi/NGDK)
[Un podcast sur la console](https://mag.mo5.com/68128/les-podcasts-de-mo5-com-32-la-neo%c2%b7geo-a-25-ans/)
[Le dossier de grospixels sur la Neo Geo](https://www.grospixels.com/site/neogeo.php)
[Un site pour les passionés de ce système](https://neogeospirit.com/)
[Les autres systèmes d’arcade de SNK](http://system16.com/museum.php?id=13)

----

## Partie 1: présentation


### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?

Alors je n’aime pas les présentations en général, mais je m’appelle Samy, j’ai 34 ans, je suis né à Marseille et je suis actuellement développeur web — oui très loin du Jeux Vidéo ou du bas niveau (malheureusement). La raison est qu’il est plus compliqué de travailler sur ces branches et on ne peut pas travailler vraiment de partout.
L’avantage du développeur web est qu’on peut y travailler partout et assez facilement (dans le sens ou on peut trouver toujours une boite d’informatique qui en demande, même en province).
J’ai été pendant un temps développeur Jeux vidéo en free-lance pour Android et IOS, mais assez mal payé et avec des deadlines assez serrées, j’ai vite arrêté.

En amateur j’ai programmé sur de nombreuses consoles en assembleur de la Nes à la PS2.

## Partie 2: La Neo Geo


### Comment en êtes-vous venu à vous intéresser à la Neo Geo ?

C’est à cause d’un jeu : King of Fighters 98, j’adorais ce jeu et aussi Windjammer que j’ai connu sur borne d’arcade.
Mais j’ai plus connu la Neo Geo via les compilations sur Playstation 2, la Neo Geo coûtait bien trop chère pour moi gamin.
Mais je me suis au final acheté une MVS.

![King of Fighters 98](https://upload.wikimedia.org/wikipedia/en/c/cf/NEOGEO_The_King_of_Fighters_%2798_%28The_King_of_Fighters_%2798_-_The_Slugfest_-_The_King_of_Fighters_%2798_-_Dream_Match_Never_Ends%29.png)

### Qu’est-ce que cette console a de particulier ?

D’un point de vue technique ?
Difficile à dire, disons que en tant que console de salon, elle est unique parce qu’elle prend des technos qu’on voit que sur arcade généralement.
Je m’explique, la plupart des consoles 8/16 bits font en général ROM->VRAM,
La Neo Geo ne fait pas cela, parce que tout simplement les ROM sont littéralement des VRAM.
La seule console qui le faisait était la Nes !
Pour cela que les portages sur console PS1 ou Saturn était assez complexe et pas aussi fidèle que l’original, quand vous avez plusieurs Mo de ROM,c’est comme si vous avez plusieurs Mo de VRAM et pour des consoles comme la PS1 qui avait 1Mo de VRAM, c’est un peu limite !

Une particularité qu’elle a et c’est qu’elle n’a pas de Background hardware, ce n’est que des sprites, le fond ne sont que des sprites assemblés.
La particularité aussi c’est qu’elle peut assembler des sprites hardware, cela à un nom mais j’ai oublié, l’avantage c’est que ça évite pas mal de calcul.
Sur SNES ou Mega drive, vous pouvez faire des "macro sprite" pour faire par exemple des sprites de 64x64, mais du coup vous devez déplacez les 4 sprites individuellement (de 32x32).
Alors que la Neo Geo le fait automatiquement.
Elle gère aussi l’animation des tiles niveau hardware.

Une chose que j’aimerais dire pour la suite, la Neo Geo a donc au moins 5 ROMs différentes, toute dédié :
-Les rom PX, pour Programme qui contienne le programme et les datas du jeu.
-Les rom CX, ils contiennent les sprites du jeu.
-Les rom VX, qui contient les SFX du jeu (en format ADPCM)
-Les rom MX, qui contient le programme sonore et les partitions des musiques.
-La rom S1, qui contient les data pour le fix (un background pour le HuD).

Si vous dézipper une ROM Neo Geo, vous les verrez, vous savez maintenant à quoi elles correspondent et à quoi elles servent !

![Une cartouche contenant les ROMs d'un jeu](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c0/Neo-Geo-AES-Cartridge-Bottom.jpg/640px-Neo-Geo-AES-Cartridge-Bottom.jpg)

### Que dire de ses processeurs (Motorola 68000 et Zylog Z80) ?

Je ne dirais pas grand chose de spécial, parce que ce sont des processeurs assez connus et pas vraiment exotiques.
L’avantage du M68000 c’est qu’il possède un compilateur GCC (que j’utilise pour mon SDK), du coup ça aide pas mal.
C’est un processeur assez simple et agréable à coder en assembleur.
Pour ceux qui ne le savent pas le M68000 était partout,sur console comme la Mega Drive, sur ordinateur comme l’Amiga ou l’Atari St, voir le X68000 (un ordinateur japonais).
Sur une bonne partie des bornes d’arcades etc...
Par contre cela se voit que c’est un processeur peu utilisé de nos jours, c’est un des rares ou j’arrive à battre GCC en assembleur,alors que sur du x86 GCC me bat à plate couture.
Et bien sur le M68000 est donc le processeur principal de la Neo Geo ! :D

Le code se trouve dans le P1 qui fait 1 Mio max, on peut faire plus, mais là on fait du bankswitch, la Neo Geo est pensé pour niveau hardware.
Le bankswitch est qu’il y’a 1Mio que vous pouvez déplacez, si par exemple vous avec 10 Mio de Programme vous avez 1Mio que vous pouvez "sélectionner".
Mais dans les fait 1Mio est largement suffisant, sauf si vous avez beaucoup de data.

Pour le z80, je n’en suis pas grand fan, mais il sert de processeur audio de la Neo Geo.
J’ai pas grand chose à dire parce que je ne l’aime pas beaucoup, même si un processeur ultra courant, qui se trouve sur Master system ou la game boy par exemple.
Sans parler des micro-ordinateur qui s’en équipe.


### Comment fonctionne son processeur graphique ?

Alors que dire, c’est une console assez simpliste de ce coté là.
Alors comme toute console de l’époque, les sprites sont associés à une palette.
La Neo Geo possède donc 256 palettes, et sur ces palettes vous pouvez mettre 16 couleurs, chaque couleurs sont sur 16 bits soit 65536 couleur disponible.
Un Sprite fait 16x16px à 16x512px (mais le 16x512 est considéré comme ayant 32 tiles de 16pixels).
Donc si vous voulez avoir un sprite de 128x128, sur Neo Geo,cela ne vous coûte que 8 sprites.
Un Background coûte en général 32 sprites (on le fait en 512x512 pour faciliter le scrolling).
Vous avez un total de 380 sprites.

Et comme dit auparavant, on peut fusionner les sprites niveau hardware et les controllers comme si y’en avait qu’un seul !
Vous pouvez dézoomer les sprites, eh oui sur Neo Geo on ne peut pas zoomer,même si les jeux faisait cet "effet", les sprites zoomés n’était que des gros sprites qu’on dézoomer !

Niveau concret, il est assez simple à utiliser le processeur graphique, on ne fait qu'écrire sur la VRAM via des I/O.
Elle reste la console à mon sens la plus facile à programmer des consoles 16 bits.

### Comment fonctionne l’audio ?

L’audio dans la scène homebrew n’est pas forcément trop explorer.
Son processeur audio est très proche du YM2608.
Le chipset de la Neo Geo c’est le YM2610.

Le YM2608 a équipé les PC-88 et PC98, ce sont des processeurs qui était populaire au japon, ils avait 70% du marché.
Du coup ce n’est pas "idiot" de penser que les compositeurs sur Neo Geo, le faisait pour PC-88, vu que la transformation pour le YM2610 est assez "simple".

Le chipset sonore est composé comme ceci :

- 4 canaux de synthèse FM
- 3 canaux de SSG
- 6 canaux ADPCM fixe à 18,5 Khz
- 1 canal ADPCM modulaire

Que veux dire tout cela ?
La synthese FM assez populaire dans les années 80/90 permet de simuler un instrument pour quasiment "rien", il faut "juste" quelques réglages et quelques octets donc pour faire un instrument existant ou pas d’ailleurs.
Le SSG, c’est des signaux triangle/carré, donc ça fait souvent le bruit d’un bip.
Le ADPCM, c’est une compression 4 bits du PCM, le PCM (8 ou 16 bits) c’est ce qu’on trouve généralement dans un format WAV.
Il à un canal modulaire vu qu’il a 6 de fixe, comparé a ce qu’on peut croire 18,5 KHz c’est très suffisant.
Alors de nos jours certain ont besoin de 96000Hz apparemment..., mais je doute qu’on voit une vrai différence.
Et pourquoi cette fréquence fixe ?
Parce que c’est principalement fait pour jouer un son (une explosion, une voix, un coup) et à cette fréquence le son est d’assez bonne qualité.
Même si je pense qu’un 16KHz est suffisant pour du SFX.
Le modulaire, est parfait pour jouer un instrument, pour ceux qui connaisse un peu la musique, il y a des notes et des octaves, en général on joue un sample (par exemple, un piano ou une guitare) qu’on module son son pour jouer au final une partition.

La différence avec la version PC-88/PC-98,c’est que l’ADPCM fixe n’était pas modifiable, l’ordinateur venait avec 6 instruments fixe qu’on ne peut pas modifier.
Alors que sur Neo Geo vous pouvez les modifier sans soucis.

### Quels services fournit le BIOS ? Existe-t-il des versions alternatives ? Libres?

Alors aussi étonnant que cela puisse paraître, je me suis pas plongé dans les méandre du BIOS de la Néo Geo.
Il fournit diverses choses,  je l’utilise principalement pour clear la VRAM et autre chose.
Mais il fournit divers services pour le CD, la gestion des manettes, des jeton etc etc.

J’imagine que vous le savez, ils existent bel et bien une version libre (*NDM : il y a des [restrictions sur l'utilisation](http://unibios.free.fr/download.html)*) du BIOS de la Neo Geo qui est UniBios.
Mais comme je suis un vieux barbu, je pourrais pas vous en dire plus, je suis à l’ancienne à taper tout via des IO !

### Quelle est votre manette préférée ?

Le Stick arcade !
Sinon ça sera pas partagé, mais j’aime beaucoup la manette PS2 !

![Le stick arcade de la Neo Geo](https://upload.wikimedia.org/wikipedia/commons/thumb/7/73/Neo-Geo-AES-Controller-FL.jpg/800px-Neo-Geo-AES-Controller-FL.jpg)

### Quel est la différence entre les différentes versions (MVS, AES, CD, CDZ, mini...) ?

Les différences sont vraiment minimes surtout entre les versions MVS et AES.
Je sais que les PCB peuvent être différentes et que les versions Arcade peuvent être un peu plus "légère" que leur version AES.

La CD et CDZ, me semble que la différence est juste la vitesse du CD, le CDZ charge deux fois plus vite (c’est pas négligeable).
Sinon la grosse différence est qu’un CD n’étant pas une ROM, les version CD n’ont que 7 Mio de RAM pour charger les ROMs, ce qui est pas "énorme" pour une Neo Geo.
Ils sont répartie comme cela :
68000 program memory: 2 Mio
Fix layer memory: 128 Kio
Graphics memory: 4 Mio
Sound sample memory: 1 Mio
Z80 program memory: 64 Kio

Et avec les 150 Ko/secondes du lecteur CD,je vous imagine pourquoi les temps de chargement sont si long !
Les autres différences et que les ROM sont différents aussi, j’ai oublié d’indiquer, cela fait longtemps mais je suis sur que pour les ROM C1/C2, par exemple, les octets sont divisé entre les deux, ce n’est pas le cas de la CD ou les pixels sont linéaire.

![La Neo Geo CDZ Z Z, elle a tous les pouvoirs](https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Neo-Geo-CDZ-wController-FL.png/800px-Neo-Geo-CDZ-wController-FL.png)

### Les émulateurs Neo Geo sont-ils bons ?

Bon dans quel sens ?
Proche du hardware de la machine ?
Non, il y’a deux erreurs que j’avais faite et qui ne marchait pas sur console réel.
Un il y’a une latence à respecter avec le processeur graphique (il faut écrire dessus tout les 12 cycles), alors que sur émulateurs tous, sont foutent royalement.
Ce que je savais pas c’est que la première couleur de la palette sert à régler un peu toutes les couleurs, du coup j’avais un bug étrange qui était que toutes les couleurs tourné au vert.
Mais aucun émulateur ne le faisait et affichait les "vrais" couleurs.
Et il existe sûrement plein de chose comme ça.

Mais si on parle de jeux, oui les émulateurs sont assez bon pour retranscrire les jeux originaux.
Pour du dev clairement pas (même MAME).

### Quels sont vos jeux commerciaux préférés sur cette console ?

Oh beaucoup trop, mais je vais citer Windjammers !
Kof 98, 2000,2002, Last Blade 2, Fatal Fury Real Bout spécial.
Les Metal Slug, Blazing Star et Aero Fighter 2.
Bon de mon point de vu, il n'y a que très peu de mauvais jeu sur cette console.

[Windjammers](https://upload.wikimedia.org/wikipedia/en/8/87/NEOGEO_Windjammers_%28Flying_Power_Disc%29.png)

### Quels sont vos jeux "homebrew" préférés sur cette console ?

Ah ben aucun, j’en ai joué à aucun pour le moment ^^’

###  Partie 3: NGDK

### Pourquoi créer un SDK pour un si vieux système ?

Parce que je trouvais pas de SDK facile à prendre en main pour la neo Geo.
Que c’est une de mes consoles favorites et que j’aime la programmation bas niveau et les Jeux Vidéo :)

### Quelles ont été les difficultés pour le concevoir ?

Disons tous les outils pour faciler le dev, ce n’est pas seulement une lib.
Et l’autre et la partie audio que je n’ai pas terminé.

### Quelles sont les fonctionnalités de NGDK ?

Le SDK permet de gérer facilmement pour vous, le fix, la palette, les sprites, la création de background, la fusion des sprites,
le dézoom, la gestion du joystick.
Pouvoir compiler dessus sur windows et Linux, la gestion des ressources (sprite / palette), la gestion des maps.

### Est-ce qu’il gère divers accessoires (cartes mémoires, manette Mahjong) ?

Ah ah non aucun désolé !

### Est-ce que vous participez à la création de jeux Neo Geo ?

Oui et non, le seul jeu que j’aimerais faire c'est un beat hthem all à la Double dragon.

### Est-ce que des jeux amateurs ou commerciaux sont développés avec NGDK ?

Il y’a des jeux amateurs qui sont développés dessus, je ne pense pas qu’un soit terminé, mais c’est cool de voir des démo tourner avec mon SDK ! :)

### Quels conseils donnez-vous aux gens qui se lancent dans le développement de jeux Neo Geo ?

Je conseille de savoir coder en C, d’être un minimum à l’aise dessus.

### Est-ce que vous aimeriez vivre de ce projet ?

Quel question !
Oui bien sûr, mais j’ai arreté de réver ^_^

## Partie 4: pour finir

### Que dire sur vos autres projets ?

J’ai deux autre projet, un qui est un [CRT](https://github.com/Kannagi/CRT) pour émulateur disponible sur FBNeo et un [moteur 3D pour les consoles Dreamcast/PS2/GameCube](https://github.com/Kannagi/LMP3D).

(Et celà me fait un peu de pub pour mon github).
Alors j’aurais beaucoup de chose à dire sur ces projets mais ça sera très très long.

*NDM : ces projets feront peut être l'objet de nouveaux entretiens, si vous êtes un spécialiste des CPU ou de la 3D, n'hésitez pas à contacter [devnewton](https://linuxfr.org/users/devnewton/) pour aider à préparer des questions pertinentes*.

### Vous avez de futurs projets en cours ?

Alors oui,et il est toujours en cours, c’est de créer un processeur (rien que ça oui).
Celà fait 15 ans que je suis passionné de micro-architecture et je trouve l’architecture des PC ou des processeurs aberrantes, ce n’est clairement pas le choix que j’aurais fait.
Et on touchant a des machines tres diverses forcément l’idée m’est venu de compilé toute ces connaissances.
C’est un processeur VLIW, qui devient de plus en plus EPIC, en gros je ne fais ni du CISC, ni du RISC.
J’ai des raisons bien précises de ne pas en faire, je ne suis pas un fan inconditionnel du RISC, pour la simple raison est que pour moi l’ISA doit représenter en partie le fonctionnement interne du processeur.
Pour celà que je me tourne sur du VLIW ou EPIC pour rendre toute celà bien plus explicite.

Mon processeur est un processeur vraiment exotique même dans sa catégorie, vous pouvez voir le projet [ici](https://github.com/Kannagi/AltairX).


Le but étant de le faire tourner sur FPGA (voir de le graver via un kickstarter).
J’ai fait un assembleur, une VM et on travaille avec d’autre pour faire un compilateur avec LLVM (et si des personnes sont motivé par un tel projet, ils sont les bienvenues).
C’est un projet passionnant qui m’a apporté pas mal de connaissances, mais aussi la certitude que je dois le finir.
Je pense que ma solution ou proposition est une vrai alternative au micro-architecture actuel.

Surtout avec l'arrivée du RISC-V qui semble un peu pousser tout le monde sur cette architecture.
Je n’ai rien contre cette architecture, mais j’ai toujours une préférence au MIPS et je trouve qu’on y fonde peut être trop d’espoir pour "rien".
C’est un MIPS mis au goût du jour, et "open source", je mets entre guillemet parce que seul l’ISA est open source, pas l’implémentation et ça change tout ! :)
C’est un peu comme si on disait "ah oui mais l’API Linux est open source mais pas son noyau", c’est un peu pareil avec le RISC-V.
Alors on peut faire une implémentation open source, mais vous imaginez bien que pour le moment c’est pas trop le cas.

La raison pour que je ne fais pas du RISC-V est comme dit, je veux que l’ISA soit très explicite.
RISC-V n’est pas neutre dans sa conception (comme le mien d’ailleurs), et sa vision reste que pour des haute perf, l’ISA est pensé pour du Superscalaire Out of Order, ce que je ne veux pas faire absolument.

Pour revenir à mon processeur, l’avantage serait que si ça marche,il pourrait avoir de bonne performance et avec une consommation et un cout réduit.

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

Alors je tourne sur [MX-Linux](https://mxlinux.org/) (avec [XFCE](https://xfce.org/)), donc que des logiciels libres en général.
Firefox comme navigateur web favori, oui je n’aime pas google chrome ! :D
Je n’aime pas non plus [Libreoffice](https://fr.libreoffice.org/), du coup j’utilise OnlyOffice.
Pour le Dev, j’utilise VS Code, [Code::Block](https://www.codeblocks.org/), [Geany](https://www.geany.org/) et QT Créator (suivant  les langages et les projets).

J’utilise [GIMP](https://www.gimp.org/), je remarque que peu de personne de mon entourage savent l’utiliser alors que je le trouve plus facile qu’avant (il y’a 15 ans, je vous assure que c’était une horreur à utiliser).
J’utilise toujours Thunar comme exploreur de fichier et le Mousepad comem editeur de texte rapide, peu importe la distribution.
Niveau émulateur j’aime beaucoup FB Neo et [mednafen](https://mednafen.github.io/) qui me permet de ne pas trop changer d’émulateur peu importe la machine.

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

Au boulot, c’est Ubuntu, VSCode et tout les navigateur disponible, mais j’utilise beaucoup Firefox par habitude.

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

Alors moi j’ai fait Ubuntu, Fedora et maintenant MX-Linux et pour le moment MX-Linux j’adore, léger et robuste c’est ce qui me convient le mieux.

### Quelle question auriez-vous adoré qu’on vous pose ?

Je dirais les questions en rapport averc la micro-architecture ou de mon processeur !

### Quelle question auriez-vous détesté qu’on vous pose ?

Si je devrais etre honnête,  j’ai plein de question que je déteste qu’on me pose.
Et donc je ne les dirais pas !
Mais en général, je n’aime pas les gens qui me disent "pourquoi tu utilise pas ça au lieu de ça", en général mes choix sont assez réfléchis.
Mais souvent ce sont des question peut être naïves sur tel ou tel sujet / choix de ma part.