URL:     https://linuxfr.org/news/entretien-avec-alekmaul-a-propos-de-pvsneslib
Title:   Entretien avec Alekmaul à propos de PVSnesLib
Authors: devnewton
Date:    2023-05-04T22:29:01+02:00
License: CC By-SA
Tags:    retrogaming et jeu_vidéo
Score:   37


Alekmaul développe [un SDK](https://github.com/alekmaul/pvsneslib) pour créer des jeux pour la console [Super Nintendo](https://fr.wikipedia.org/wiki/Super_Nintendo), cet entretien revient sur son parcours et les raisons qui l’ont amené à s’intéresser à cette console.

![Super Nintendo](https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/Wikipedia_SNES_PAL.jpg/800px-Wikipedia_SNES_PAL.jpg)

----

[PVSnesLib](https://github.com/alekmaul/pvsneslib)

----

## Partie 1: présentation

### Qui êtes-vous, quel est votre parcours et est-il lié aux jeux vidéos ?

Hello, je m’appelle Jean-Michel Girard, connu sous le nom d’Alekmaul dans le monde du retro gaming.

J’ai fait des études d’informatique il y a maintenant bien longtemps où l’on apprenait encore comment développer en assembleur (**NDM: est-ce réellement le cas? **) ;)

J’ai toujours aimé développer des jeux vidéos. J’ai connu la grande époque [d’Hebdogiciel](https://fr.wikipedia.org/wiki/Hebdogiciel), le journal qui publiait des listings à taper sur le clavier de pas mal de machines, listings qu’envoyaient les lecteurs du journal. Moi même je n’ai jamais été publié dedans à l’époque ^^.
J’ai vu les débuts des micro-ordinateurs et consoles dans les années 80, j’ai commencé à développer sur [ZX81](https://fr.wikipedia.org/wiki/ZX81), [Casio PB-100](https://fr.wikipedia.org/wiki/Casio_PB-100) et [ZX Spectrum](https://fr.wikipedia.org/wiki/ZX_Spectrum), ma passion du développement de jeux vidéos date de là  ;)

J’ai aussi fait un passage éclair dans le jeu vidéo chez Virgin Espagne pour développer un jeu sur [DS](https://fr.wikipedia.org/wiki/Nintendo_DS), mais y’a eu la banqueroute en Espagne à ce moment là (vers 2010 si je me souviens bien), il n’est jamais sorti mais je suis fier d’avoir pu profiter du kit officiel de Nintendo sur DS ^^.

J’ai maintenant 56 ans, je travaille toujours dans l’informatique dans le monde du paiement sur Internet, je gère des personnes, des projets mais je ne développe plus.

Je m’éclate donc à développer sur mon temps perso sur de vieilles consoles !


## Partie 2: SNES

Comment en êtes-vous venu à vous intéresser à la SNES ?

C’est un ami, connaissant mes créations sur Nintendo DS et [GBA](https://fr.wikipedia.org/wiki/Game_Boy_Advance) qui m’a demandé de voir si j’étais intéressé pour réaliser un kit de développement sur SNES.

En effet, le développement en assembleur est pas simple et il se disait que si quelque chose était disponible en C comme pour la GBA ou la DS, ça aiderait à promouvoir cette machine.

Un début de kit existait sur googlecode, basé sur un fork du compilateur [tcc](https://bellard.org/tcc/). Tout est parti de là sur cette console.

Je l’ai sorti il y a maintenant plus de 10 ans sur SNES, pour les 20 ans de la console en France. On continue toujours à maintenir PVSneslib avec de plus en plus de contributeurs !

### Qu’est-ce que cette console a de particulier ?

Elle n’est pas facile à programmer avec son processeur entre le 8 bits et 16 bits. Elle possède un chip sonore bien sympathique et des capacités graphiques permettant pas mal d’effets surtout avec l’aide des transferts rapides de mémoire possibles. Ses différents modes graphiques peuvent être un atout mais aussi un frein à comprendre la console.

Sa résolution est moindre que la Megadrive mais dans la lignée des machines de l’époque. Son processeur est par contre bien lent, ça aurait été super d’avoir un 68000 dedans ^^.

### Comment fonctionne les processeurs graphiques (Super PPU) ?

Le processeur graphique est du même genre que les autres consoles de l’époque. Il possède certains modes graphiques avec un certain nombre de couleurs gérables par mode graphique.

Le mode le plus utilisé est le mode 1. Chaque mode possède un certain nombre de "fonds" ou plans sur lequel on peut réaliser des graphiques, les faire se déplacer.

Chaque plan dans ce mode possède 16 couleurs parmi "tout plein" de couleurs, comme les sprites. On fait ainsi de très jolis jeux (non je dirais pas des jeux plus jolis que sur Megadrive ;) :P ).

On possède aussi un certain nombre d’objets animés nommés sprites, ce qui aide beaucoup pour tout à dessiner sur un seul plan.

Le terme "Super PPU", en tant que tel, je connais pas désolé :( ( **NDM: ce terme est utilisé par [cet article](https://www.copetti.org/writings/consoles/super-nintendo/) ).

### Comment fonctionne les puces sonores (S-DSP, SPC700) ?

La puce sonore fonctionne par transfert de données depuis la puce de la SNES vers le processeur du SPC700.

Cela prend donc du temps, c’est pour cela que dans mes jeux, y’a un petit délai au début des niveaux pour transférer les musiques.

De plus, elle ne supporte pas plus de 64Ko de mémoire, sons et programmes sonore compris, c’est peu mais il faut faire avec !

### Beaucoup de cartouches embarques des coprocesseurs, quels sont les plus remarquables ?

J’en sais fichtrement rien :D ! je ne développe pas sur ces processeurs. Peut être que le DSP1 est sympa de part ses capacités de calculs matriciels et le Super-FX pour la 3D ;)

### La SNES a récemment eu une version "mini" ? Qu’en pensez-vous ?

C’est mignon, c’est mini, pas grand chose de plus. Je la possède à la maison, mais elle prend la poussière je dois dire ^^.

Faudrait que je la patch un de ces jours avec mes jeux pour les voir sur grand écran ^^.

### Quelle est votre manette préférée ?
Les manettes d’origines, elles me suffisent et fonctionnent bien :P

### Les émulateurs SNES sont-ils bons ?

Ils sont plus ou moins bon. Le meilleur est le dernier sorti : [Mesen-S](https://github.com/SourMesen/Mesen-S) ou MEsen maintenant depuis la reprise du projet par l’auteur après quelques rebondissements. B-Snes est très bon aussi. J’utilise pas mal aussi no$sns, car c’est facile de déboguer avec. Mais l’émulation n’est pas tout à fait fidèle pour ce dernier. En gros, Mesen-S pour tester la compatibilité avant de tester sur une vrai console (car rien ne remplace les vrais consoles) et no$sns pour debugger rapidement.

### Quels sont vos jeux commerciaux préférés sur cette console ?

Mario Kart et Metroid !

Mais aussi Chrono Trigger qui est une vrai claque sur SNES.

### Quels sont vos jeux "homebrew" préférés sur cette console ?

Héhé, je vais pas dire les miens, mais il y en a très peu sur SNES. Certainement les homebrews de [Shiru](http://shiru.untergrund.net/software.shtml), un développeur Russe bourré de talent.

Comme par exemple Classic Kong qui exploite bien la console.

### Pourquoi créer un SDK aujourd’hui pour si vieux système ?

J’ai créé le SDK à la demande d’un ami. Il voulait savoir si on pouvait programmer facilement sur cette console. Je suis parti d’un code basé sur un fork de tcc par Ulrich Hecht nommé SNES C SDK.

Les 20 ans de la console arrivaient, c’était le bon moment de travailler sur quelque chose.

Le SDK commence donc à dater mais n’a pas eu beaucoup de succès au départ, mais il commence à prendre forme maintenant, 10 ans plus tard ...

### Est-ce que vous participez vous même à la création de jeux ?

Ah bah oui, c’est même mon activité principale sur SNES. Parmi les plus connus, j’ai créé Sydney Hunter and the Caverns of Death, puis Eyra the Crow Maiden et je travaille maintenant sur The Illmoore Bay (plus pas mal d’autres jeux aussi ^^).

### Quels ont été les difficultés pour créer PVSnesLib ?

Tout d’abord, bien connaitre la console. C’est le minimum avant de se lancer la dedans. J’ai donc potassé tout ce que je trouvais, consulté les wikis, le fichier d’aide de no$sns qui est une mine d’information.

Ensuite, il faut réfléchir à trouver comment réaliser quelque chose de portable (et il reste du chemin avec PVSNeslib !) pour que tout le monde puisse l’utiliser sur son projet. C’est ce que j’ai tenté de faire. D’autres personnes ont utilisé le kit, m’ont fait part de remarque et on est maintenant plusieurs à le faire évoluer, ce qui est ma plus grande fierté !

### Vous aimeriez vivre du développement de ce logiciel libre?

Vivre du développement de jeu sur ancienne console ? Pas possible à mon avis, il faut faire quelque chose à côté.

Pour ma part, c’est maintenant plus un projet pour ma retraite ! J’ai d’ailleurs créé "Alekmaul Studio" pour essayer de régulariser tout cela et préparer cette seconde étape dans mes développements Homebrews.

### Est-ce que PVSnesLib gère les multitaps ?

Oui, il gère le multitap et un futur jeu va l’exploiter (Bizbille, pour mes amis Canadiens, un genre de roue de la fortune).

### Est-ce que PVSnesLib  gère les pistolets (Super Scope) ?

J’ai un début d’implémentation mais rien de concret ! J’attends de l’aide sur ce sujet !

### Est-ce que PVSnesLib  gère d’autres périphériques de la SNES (souris, modems, ...) ?

Non, rien d’autre pour l’instant, à part la SRAM.

### Quels conseils donneriez-vous à quelqu’un qui veut se lancer dans le développement SNES ?

Erf... Tout d’abord avoir la passion car ça va être long. Ensuite, connaitre le langage C, langage utilisé pour programmer avec PVSnesLib. Puis , connaitre le langage machine de la SNES (le 65C816), car ça aide à debugger. Et enfin et surtout, connaitre la SNES, comment elle fonctionne, quelles sont ses limites en matière de graphismes, de son, de mémoire.

### Quels sont les outils pour créer/préparer des graphismes utilisable par PVSnesLib  ?

J’utilise GraphicGale pour créer mes graphiques et c’est ensuite l’outil gfx2snes, livré avec PVSneslib qui fait le job pour rendre cela compatible avec ce que l’on souhaite.

### Quels sont les outils pour créer/préparer des musiques et des sons utilisable par PVSnesLib  ?

Il faut un tracker de musique qui supporte le format IT, comme openMPT. L’outil smconv livré avec PVSneslib fera ensuite le job pour rendre tout cela compatible avec ce que l’on souhaite.

### Est-il possible de créer ses propres cartouches ?
Bien sur ! j’en suis déjà à 4 jeux différents édités sur SNES :) !

### Comment s’accommoder des faibles ressources (peu de RAM, CPU 8 bits...) ?

Bah, faut réfléchir, bien poser les problèmes, savoir optimiser ce qu’il faut (on le dit jamais assez mais un octet fait 8 bits, on peut donc stocker 8 informations binaires dedans ^^ ).

## Partie 3: pour finir

### Au niveau personnel, quels logiciels libres utilisez-vous, sur quel OS ?

Qu’est-ce que l’on qualifie exactement de logiciel libre ? ( **NDM : en général sur linuxfr, on parle de logiciel libre pour ceux qui respectent les [4 libertés](https://www.gnu.org/philosophy/free-sw.html#four-freedoms)** )

Les compilateurs pour mes programmes, dont j’ai les sources, ça c’est certain.

Je crois que j’utilise peu de logiciels libres en tant que tel.

Car sinon, j’ai aussi du VSCode et du VirtualBox pour tester et éditer, mais je pense pas que cela soit considéré comme du libre.

J’utilise par contre pas mal de logiciels gratuits.

Ah si, j’utilise aussi VI, j’adore :D !

### Au niveau professionnel, quels logiciels libres utilisez-vous, sur quel OS ?

Je ne peux hélas pas en parler ;)

Tout ce que je peux dire c’est que l’on développe avec Java sous Linux (RedHat).

### Quelle est votre distribution GNU/Linux préférée et pourquoi, quels sont vos logiciels libres préférés ?

Ubuntu est sympa pour le peu de ce que je fais sous Linux. Au taf, on est sous RedHat.

Pas de logiciel libre sur Linux, je m’en sers uniquement pour tester PVSneslib et la compilation de cette dernière.

### Quelle question auriez-vous adoré qu’on vous pose ?

Je dois dire que je sais pas ... Peut être "t’es pas trop vieux pour faire ce genre de chose ? " :D !

### Quelle question auriez-vous détesté qu’on vous pose ?

C’est pas vraiment une question mais plus quelque chose qui est de plus en plus fréquent et qui me fatigue : pourquoi ne pas faire un SNESStudio ou un SNESMaker ? C’est trop dur de développer avec PVSneslIb ...

J’ai même le droit à "c’est plus simple de développer sur Megadrive avec SGDK :D" !