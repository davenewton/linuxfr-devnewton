URL:     https://linuxfr.org/news/ned-et-les-maki-0-2-lts-gusty-tsar
Title:   Ned et les maki 0.2 LTS Gusty Tsar
Authors: devnewton
         Pierre marijon, jymistriel, Davy Defaud, al.jes, BAud, Nils Ratusznik, rewind, Florent Zara et Xavier Claude
Date:    2014-03-22T23:37:49+01:00
License: CC By-SA
Tags:    libre, sokoban, jeu_vidéo et jdll
Score:   35


Pour fêter l’[adhésion de la Crimée à la Fédération de Russie](http://fr.wikipedia.org/wiki/R%C3%A9f%C3%A9rendum_sur_la_souverainet%C3%A9_%C3%A9tatique_de_la_Crim%C3%A9e), ainsi que la [victoire de [insérer ici le parti vainqueur] aux municipales](http://fr.wikipedia.org/wiki/%C3%89lections_municipales_fran%C3%A7aises_de_2014), [_devnewton_](http://linuxfr.org/users/devnewton), [Natir](http://linuxfr.org/users/natir) et Geeky Goblin Production ont décidé de sortir une nouvelle version du jeu libre _Ned et les maki_.

![ned1](ned1.png)

----

[Télécharger le jeu](https://play.devnewton.fr/prototypes.html)
[Geeky Goblin Production](http://geekygoblin.org/)
[Forge](http://dev.geekygoblin.org/public)
[Dépêche précédente](http://linuxfr.org/news/ned-et-les-maki-0-1)

----

# Le jeu
_Ned et les maki_ est un jeu de puzzle libre. Il revisite le genre _[[Sokoban]]_ en proposant une histoire, de nouvelles mécaniques et un univers visuel original.

# Les nouveautés
## Gameplay
* Ned, le personnage principal, peut désormais voyager dans le temps : si le joueur se retrouve bloqué dans un niveau, il peut rembobiner pour tenter une autre stratégie sans avoir à tout recommencer ;
* le jeu est utilisable à la souris ;
* des niveaux de tutoriels pour expliquer les différents mécanismes du jeu ont été ajoutés.

## Graphismes
* Le jeu comprend plusieurs nouvelles animations : 
    * les maki brûlent,
    * Ned a appris à :
      * voler,
      * monter des escaliers,
      * changer ses chaussettes ;
* Les décors ont été enrichis (meilleure lisibilité des niveaux et ajout d’espaces verts) ;
* l’écran titre et les menus sont beaucoup plus jolis et ergonomiques.

## Blog
Les GGP ouvrent leur [_blog_](http://blog.geekygoblin.org) ! On y parlera de _Ned et les Maki_, mais pas que. Par exemple, des illustrations et des blagues plus ou moins compréhensibles devraient y fleurir.

# Futurs développements
La prochaine version proposera l’ensemble des éléments du _gameplay_ : nouveaux personnages, nouveaux objets, un système pour raconter l’histoire… Un musicien, Valentin, ayant rejoint l’équipe, il y aura peut‐être du son.

# Journées du logiciel libre
Une partie de l’équipe sera aux JDLL de Lyon les 12 et 13 avril, dans le village associatif près du stand jeux vidéo, pour présenter _Ned et les maki_, ainsi que d’autres productions des GGP.

![ned2](ned2.jpg)
