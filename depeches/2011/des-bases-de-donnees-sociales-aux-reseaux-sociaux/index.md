URL:     https://linuxfr.org/news/des-bases-de-donnees-sociales-aux-reseaux-sociaux
Title:   Des bases de données sociales aux réseaux sociaux
Authors: devnewton
         
Date:    2011-07-02T20:10:15+02:00
License: CC By-SA
Tags:    réseau_social, retroshare, bdd, réseau, social, network et gestion_contacts
Score:   38


Quelques sites très populaires ont mis à la mode le terme « réseau social ». Pourtant, ce terme dont usent et abusent les médias cache le caractère extrêmement centralisé de ces sites et avec lui les problèmes que posent le fait de confier des données personnelles à une entreprise unique.



Des alternatives décentralisés existent, même si elles n'ont pas encore la faveur des médias. Grâce à elles, les utilisateurs soucieux de leur vie privée peuvent passer des bases de données sociales aux réseaux sociaux.

----

[Retroshare](http://retroshare.sourceforge.net)

----

# Définitions



## Qu'est-ce qu'un réseau ?



Un réseau est un ensemble de nœuds et de liens. Un lien relie deux nœuds.



Cette définition simple s'applique à de nombreux domaines : EDF, les chemins de fer ou encore les routes sont des réseaux « physiques » que nous utilisons tous les jours. Nos relations amicales ou professionnelles sont aussi des réseaux : les sociologues les baptisent réseaux sociaux.



Depuis un certain nombre d'années, nous utilisons un réseau, encore un, pour mettre en œuvre ces relations : Internet.



Internet lui même est un ensemble de réseaux. On peut en distinguer au moins trois types :



- des réseaux physiques constitués de câbles, d'équipements informatiques et d'ondes ;
- des réseaux logiques : chaque ordinateur fait partie d'un ou plusieurs réseaux : votre réseau domestique, celui de votre entreprise, celui de votre fournisseur d'accès...
- des réseaux applicatifs : chat ou mail par exemple.



## Qu'est-ce qu'une base de données ?



Une base de données est un ensemble d'informations stockées dans un système informatique. Il existe bien des manières d'accéder à une base de donnée, l'une d'elle est d'utiliser un site web : la plupart des sites sont en fait des interfaces pour accéder à une base de données. Le site de la Fnac permet d'accéder à plusieurs bases de données : la base des produits, la base des commandes et des paiements...



## Qu'est-ce qu'un réseau social ?



Un réseau social est un réseau dont les nœuds sont des individus ou des organisations et les liens sont leurs échanges. Une caractéristique de ces réseaux est que les liens n'ont pas d'existence physiques, pour ne pas disparaître, ils doivent en permanence être entretenus : un ami que vous n'appelez pas assez souvent, un client que vous ne démarchez plus, un oncle dont vous oubliez l'anniversaire...



Pour entretenir notre réseau social, nous utilisons des outils qui sont souvent eux mêmes basés sur réseaux : le plus ancien est sans doute le courrier que l'on s'échange via le réseau postal.



Outre le courrier ou le téléphone, de nos jours les outils les plus populaires passent par Internet : chat, mail et ce qu'on appelle les *réseaux sociaux*. Cette dernière appellation est problématique, car :



- elle entretient la confusion entre le réseau social (amis, collègues...) et l'outil de communication ;
- ce ne sont pas des réseaux : ce sont des sites et des bases de données dans lesquelles l'utilisateur stocke des informations sur son réseau social.



Il vaudrait donc mieux parler de sites sociaux ou plus clairement de bases de données sociales.



# Bases de données et réseaux : avantages et inconvénients des différents modèles



La plupart du temps, on ne choisit pas son support de communication. Ce sont les autres qui nous l'imposent. À quoi bon disposer d'un dispositif ultramoderne si on ne peut entrer en contact avec personne ? L'effet de groupe est prédominant. C'est parce que tout le monde dispose d'un téléphone et d'une boite postale qu'il est quasiment impossible de s'en passer.



Pourtant le choix de ces supports a des conséquences importantes sur :



- les coûts : cela va du paiement à la seconde aux financements par la publicité en passant par le forfait ;
- la confidentialité et la sécurité : certains systèmes sont anonymes, d'autres exposent vos communications à tous, d'autres au contraire vous garantissent que seuls vos correspondants seront concernés ;
- la maîtrise : qui peut décider de l'évolution voire l'arrêt du système ? qui autorise ou en interdit l'accès ?



## Les bases de données sociales : la centralisation extrême



Les bases de données sociales se caractérisent par une relation, souvent contractuelle, entre un fournisseur unique et ses utilisateurs/clients.



Le fournisseur met à disposition un système informatique composé d'une base de données et des moyens, site ou application, pour l'utilisateur d'y stocker son réseau social et toutes ses communications.



La plupart du temps l'utilisateur n'a pas conscience qu'il s'agit d'une base de données puisque la seule chose qu'il voit, c'est le site ou l'application proposé par le fournisseur. Il s'incrit sur le site, lit vaguement les conditions ou le contrat d'utilisation et commence à l'utiliser sans se poser de questions.



L'avantage de ce modèle centralisé est en effet sa simplicité et son confort pour l'utilisateur. Toute la technique, de l'installation à la maintenance, est assuré par le fournisseur. L'utilisateur est un simple consommateur d'un service qu'il paye soit directement soit (paiement à l'utilisation ou abonnement), soit en « temps de cerveau disponible » (publicité) ou encore avec ses données personnelles (reventes de coordonnées, études marketing...).



Tant qu'il reste dans le cadre de la loi, le fournisseur est tout puissant : il décide qui peut ou ne peut pas accéder à son système, il peut y pratiquer la censure, le faire évoluer à tout moment... Bien sûr il doit se comporter en dictateur éclairé : les utilisateurs sont certes relativement captifs, ils risquent de perdre leur réseau social, mais restent des clients dont le mécontentement peut entraîner des départs en masse vers la concurrence. L'Etat peut également le contraindre, par exemple à communiquer toutes les données à la Justice, aux services de renseignements ou à censurer des contenus illégaux.



## Les réseaux de bases de données sociales : la décentralisation possible



Ces réseaux sont proches des bases de données sociales. Il y a toujours au moins un fournisseur et des utilisateurs. La grande différence est que chaque utilisateur peut devenir son propre fournisseur, juste pour lui même ou pour d'autres utilisateurs (famille, association, entreprise...). Il doit alors gérer sa propre base de données et l'interconnecter avec celles des autres fournisseurs.



Être son propre fournisseur est assez contraignant. Il faut d'abord des compétences pointues en informatique et en réseau. Cela demande ensuite une maintenance non négligeable : les logiciels doivent être mis à jour, l'ordinateur doit tourner pratiquement 24h/24... En contrepartie, l'utilisateur/fournisseur maîtrise ses données et ne dépend pas du bon vouloir d'une entreprise ou d'une administration unique.



Même sans devenir son propre fournisseur, ce modèle donne une plus grande liberté de choix : si l'on ne se sent pas les compétences, on peut faire appel à un prestataire dont on pourra changer si jamais il est défaillant.



## Les réseaux sociaux : la décentralisation totale



À la différence du précédent modèle, ces réseaux ont été pensés dès le début comme décentralisés, pour des utilisateurs lambdas et non pour des entreprises ou des administrations disposant d'une direction des systèmes informatiques. Ils ne nécessitent pas de connaissances informatiques trop poussées et ne demandent pas de laisser tourner un ordinateur jour et nuit.



Presque aussi simples qu'une base de données sociales, non dépendant d'une seule entreprise tout en laissant à l'utilisateur la maîtrise de ses données, ces logiciels pourraient être idéaux s'il n'y avait pas quelques obstacles à leur développement.



Tout d'abord, ils sont plus difficiles à créer et ne représentent pas d'intérêts financiers pour les entreprises du milieu, ce qui explique qu'on commence à peine à voir émerger des logiciels de qualité dans ce domaine.



Ensuite, ils sont souvent inutilisables en entreprise. Ces dernières mettent en place des politiques de sécurité qui font que généralement seul l'accès aux pages webs est possible. Or le web fonctionne de manière très centralisée, il ne peut donc pas servir de socle technique à ces logiciels.



Enfin l'effet réseau joue ici : les gens ne vont sur un réseau que s'il y a du monde. Or ces projets sont souvent jeunes et donc avec peu d'utilisateurs.



# Rejoindre un réseau



Avec les bases de données sociales, rien de plus simple en apparence : tout le monde est capable de remplir un formulaire d'inscription. Lire les contrats et comprendre des conditions d'utilisation rédigés en argot juridique est une autre paire de manches, mais on peut retenir une chose : vous serez pratiquement toujours à la merci du fournisseur, notamment si le service est gratuit et financé par la publicité.



Examinons trois solutions plus décentralisées qui proposent les fonctionnalités minimales d'un réseau social moderne :



- messagerie instantanée (chat) ;
- messagerie asynchrone (mail) ;
- partage de fichiers (photos, vidéos, documents...) ;
- publication de contenu (blog).



*Attention, ce ne sont que des propositions, pas une liste exhaustive des solutions de communication sur Internet*



## Les bonnes vieilles technos



Il existe depuis longtemps des technologies pour communiquer sur Internet. Leur ancienneté a plusieurs avantages : tous les ordinateurs disposent en standard de logiciels pour les utiliser, elles sont robustes et éprouvées et la documentation est abondante.



Cette première solution consiste à combiner quatre protocoles que vous connaissez sans doute déjà : IRC pour le chat, SMTP pour les mails, FTP pour le partage de fichier et HTTP pour le blog.



En installant ou en louant un PC qui tourne 24h/24 avec tous ces logiciels serveurs qui implémentent ces protocoles, vous disposerez des fonctionnalités listées ci dessus en toute indépendance et vos amis pourront y accéder sans installer de logiciels supplémentaires.



Peu contraignante pour vos amis, cette solution l'est pour vous. Prévus pour des informaticiens, ces logiciels sont complexes à configurer et à maintenir.



## XMPP, le protocole qui devrait un jour tout faire



XMPP est un protocole de messagerie instantanée qui dispose d'extensions pour gérer les fonctions de mail, partage de fichier et blog. Il est utilisé pour les messageries de Facebook et Google, vous pouvez donc l'utiliser pour communiquer avec les gens inscrits chez ces fournisseurs.



Là aussi pour être indépendant, il faut un PC qui tourne en permanence, mais on n'a qu'un seul logiciel à configurer contrairement à la solution précédente.



XMPP serait parfait s'il ne souffrait pas d'un grave défaut : les logiciels que l'on utilise pour y accéder gèrent de façon très différentes et souvent mal tout ce qui est autre que le chat entre deux personnes.



Ainsi, si votre correspondant n'utilise pas le même logiciel client, certaines fonctionnalités ne seront pas utilisables. Cette situation est très pénible et dure depuis un moment, mais elle pourrait évoluer dans les années qui viennent. À condition que les utilisateurs, impatients, ne passent pas à autre chose.



Il faut quand même noter que de nouveaux clients, à l'interface calquée sur les bases de données sociales populaires, sont en cours de développement : Jappix, Lorea et Movim sont très prometteurs.



# Retroshare, le challenger pensé pour les utilisateurs



### Présentation



Retroshare est un logiciel de communication décentralisée. Il propose les fonctions de chat, mail, blog, le partage de fichier et de forum de discussion.



Contrairement aux précédentes solutions qui sont des bases de données sociales en réseau, il s'agit d'un vrai réseau social. Il a été créé pour être mis dans les mains des utilisateurs finaux et non d'administrateurs systèmes. Il n'est pas non plus nécessaire de le laisser tourner en permanence. Vous l'installez et commencez à l'utiliser en quelques minutes, vous le fermez quand vous n'en avez plus besoin.



### Utilisation quotidienne



Plutôt que de longuement décrire Retroshare, voici une description d'une session typique du logiciel.



Une fois lancé, vous commencerez presque toujours par regarder le « fil d'actualité ». Il s'agit d'un résumé des différents évènements sur votre réseau. Vous pouvez ainsi par exemple voir qu'un forum a été créé ou qu'un ami a posté une entrée sur son blog.



En allant voir ce blog, « canal » dans le vocabulaire de Retroshare, vous constatez que cet ami résume ses vacances et a mis à disposition ses photos, un clic les ajoute à votre liste de téléchargement. Comme vous étiez aussi présent pendant ces vacances, vous répondez sur son canal en ajoutant vos propres photos.



Après avoir lu ce blog, vous décidez d'aller voir ce nouveau forum. Vous constatez qu'il s'agit d'une discussion sur les qualités cinématographiques d'un film de science fiction réalisé par David Lynch. Outré par la présence d'un lien pour télécharger ledit film, vous répondrez pour déconseiller aux gens de cliquer pour l'ajouter à leur liste de téléchargement.



L'auteur du message contenant le lien n'ayant pas utilisé le mode anonyme pour envoyer son message sur le forum, vous utilisez la messagerie pour lui indiquer qu'il est un vil pirate.



Comme il se fait tard, vous écrivez dans le chat un « bonne nuit » que tous vos amis verront. L'un d'eux vous engage dans un chat privé pour vous signaler de ne pas oublier un rendez vous important. Après lui avoir assuré que vous ne l'oublierez pas, vous fermez enfin l'application. Les photos ne sont pas toutes téléchargées, tant pis, le téléchargement reprendra à votre prochaine connexion.



### Un avenir prometteur



Retroshare n'est pas parfait, mais c'est la solution la plus à même de redonner le contrôle aux utilisateurs de réseaux sociaux. Les seuls défauts viennent de la jeunesse du projet : il y a encore peu de monde dessus, la gestion des droits n'est pas encore assez fine (vous partagez tout ou rien avec tous vos contacts), la création d'un compte pourrait être plus conviviale... Mais ce projet, qui n'en est encore qu'à la version 0.5, évolue vite.



Il faut enfin noter que ce logiciel pourrait connaître un grand succès pour ses fonctionnalités « non sociales » : il permet de partager des fichiers ou de discuter dans des forums de façon totalement anonyme. Les lois et les attaques contre les sites de téléchargements vont pousser les utilisateurs vers ce type de logiciel.



# Le futur



S'il est difficile de faire des prédictions, on peut être certain d'une chose : les considérations économiques et politiques vont modeler le futur des bases/réseaux sociaux. Les entreprises investiront pour fournir des bases sociales et capter des utilisateurs tant que leurs données privées auront de la valeur. Les États chercheront à les contrôler et à canaliser les mouvements
de masse qu'ils permettent.



La plus grande inconnue est ce que vont faire les utilisateurs ? Aujourd'hui, ils ont encore le choix entre rester consommateurs de bases sociales ou devenir acteurs de réseaux sociaux.
