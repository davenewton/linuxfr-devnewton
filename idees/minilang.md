# Minilang core

This document is a draft of programming language designed :

- for educational purpose, it should be very easy to learn and show to students important programming concepts ;
- to be usable in different context with very limited core and extensions for specific domains ;
- to be powerfull enough to create real applications.

## Overview

Everything is a command call:

    command parameter1 parameter2 // comment

Command parameters can be:

    command 1 // constant
    command { 1 2 } // tuple
    command 1+2*5 42/2 // math expressions
    command true&&false false!=true // boolean expressions
    command command2 // other command calls

Some feature are only available with [extensions](#extensions).

Memory management is automatic excepts for [System extensions](#system-extensions).

## Variable

Variable are declared using a var command:

    var $name $type $value

Variable can be initialised or not:

    var x integer 0 // initialized value
    var x integer _ // uninitialized value
    var message string  "Hello world" // text constant

Variable value can be set using

    set x 1

## Math expressions

    set x x+1
    set x x-1
    set x x*2
    set x x/2

### boolean expression

    x>1
    x!=1
    x>0 && y<2 || z != 1

## Control structures

### if

    if {
        x > 10 {
            print "x is greater than 10"
        } 
        x < 10 {
            print "x is lower than 10"
        }
        {
            print "x is equal to 10"
        }
    }

### for

    for var i integer 1 i<10 i+1 {
        print "${i}"
    }

    for _ _ _ {
        print "Infinite loop"
    }

## function

    function add { x integer y integer } integer {
        return x+y
    }

## primitive types

    boolean
    integer
    character
    string

These types are passed by value (copied).

### boolean

    var b boolean true

### integer

    var x integer 10

### character

    var a character 'a'

Without [international extension](#international-extension), only ASCII character are valid.

### string

    var msg string "Hello world"

Without [international extension](#international-extension), only ASCII string are valid.

# Extensions

## object extension

    //definition
    object Cat {
        name string
        age integer
    }

    //instanciation
    var felix Cat {name "Felix" age 2}

Objects are always passed by reference.

### object function

    function Cat.changeName {newName string} _ {
        set self.name newName
    }

    var felix Cat {name "Felix" age 2}
    felix.changeName "Garfield"

### interfaces

An interface describes a list of functions that an object should implements.

    interface Animal {
        scream {} _
    }

    function Cat.scream {} _ {
        print "Meow"
    }

    function Dog.scream {} _ {
        print "Arf"
    }

    function makeAnimalScream { a Animal } _ {
        a.scream
    }

### generic objects

    //definition
    generic {K V} object Cell {
        K key
        V value
    }

    //instanciation
    var c Cell integer string {key 1 value "a"}

Objects are passed by references.

### collection objects

This extension add objects like list, map, table.

### list

A list is an ordered collection of values.

    var a list integer { 1 2 3 4 } //define a variable named "a" which is an list of 4 integer values 1, 2, 3 and 4
    a.get 0 // returns 1
    a.set 0 42 // change the first element value to 42

### map

A map is collection of key/value pairs.

    var m map string string
    menu.set "k" "v"
    menu.get "k" // returns "v"

### table

Table is a dynamic 2 dimensions data structure with rows and columns with different types for indexes and values.

    var menu table integer character string // define a menu table with 2 rows and 2 lines
    menu.set 1 'A' "First course"
    menu.set 1 'B' "Salad"
    menu.set 2 'A' "Main course"
    menu.set 2 'B' "Pizza"
    menu.get 1 'B' // returns "Salad"

## System extensions

A system extension add low levels and system programming features to the language.

### System types

#### System primitives

Each system defines its own datatype.

    // most system use integer of different sizes
    bit
    uint4
    int8
    int16
    uint32

    // system with FPU have floating point numbers
    float32
    float64

    // advanced cpu or gpu have vector and matrix
    vec4
    mat3

#### pointers

    var vram pointer uint8 0xA0000 // pointer to specific memory address
    p // read value at pointed address
    set p 255 // write value at pointed address
    pointer.set p 0xA0001 // change pointer address
    pointer.get p // returns pointer address

#### records

    //definition
    record SNES_Sprite {
        uint8 x
        uint8 y
        uint9 tile
        uint3 palette
        uint2 priority
        bit hFlip
        bit vFlip        
    }

    //instanciation
    var b SNES_Sprite {hFlip 1} //other fields have undefined values

    Records are passed by value (copied) to functions.

#### arrays

An array is collection of record or primitive types lay out continously in memory.

    var snes_oam_low_table array SNES_Sprite 128 //define an array with 128 entries
    var firstSprite pointer SNES_Sprite snes_oam_low_table.at 0 //access to array element is done through pointers
    set firstSprite.hFlip !firstSprite.hFlip

Arrays are passed by value (copied) to functions.

## International extension

This extension allows to write program in many languages by using Unicode for strings and characters.
