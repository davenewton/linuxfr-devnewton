URL:     https://linuxfr.org/users/devnewton/journaux/il-a-demande-a-free-il-a-rien-compris
Title:   Il a demandé à Free, il a rien compris
Authors: devnewton
Date:    2013-02-04T11:11:15+01:00
License: CC By-SA
Tags:    free, ssl, journal_du_mois_2013 et openoffice
Score:   42


Bonjour Nal,

Récemment un ami m'a demandé de l'aide, car son client mail affichait un message d'erreur inquiétant:

> Vérification du certificat SSL pour imap.free.fr : 
(...)
Signature : MAUVAIS

N'étant pas sur place, je lui ai demandé de faire appel au support de Free, leur réponse est assez étrange:

> Nous vous informons qu'il n'est pas utilisé d'authentification SSL par défaut avec un logiciel de messagerie pour un compte mail FREE.

> Si votre ordinateur dispose des outils de protection importants tels qu'antivirus et pare-feu, et que ceux-ci sont régulièrement mis à jour, alors cette authentification des mails n'est pas indispensable pour garantir la sécurité de vos correspondances.

Je n'ai pas bien compris ce que propose le support de Free pour protéger mes correspondances.

Dois-je installer OpenOffice?
