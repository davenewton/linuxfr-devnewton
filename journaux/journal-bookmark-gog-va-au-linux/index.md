URL:     https://linuxfr.org/users/devnewton/journaux/journal-bookmark-gog-va-au-linux
Title:   [journal bookmark] gog va au linux
Authors: devnewton
Date:    2014-03-18T17:43:39+01:00
License: CC By-SA
Tags:    linux, appeau_a_zenitroll et jeu
Score:   38


Bonjour,

Encore une excellente nouvelle pour les joueurs: après Steam et Desura, c'est maintenant gog, l'un des plus importants sites de vente de jeux vidéos qui a décidé de vendre des jeux linux.

Bien que pwivateurs, les jeux vendus par gog ont au moins l'avantage d'être sans DRM.

http://www.gog.com/news/gogcom_soon_on_more_platforms
