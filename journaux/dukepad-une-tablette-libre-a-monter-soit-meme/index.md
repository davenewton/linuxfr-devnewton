URL:     https://linuxfr.org/users/devnewton/journaux/dukepad-une-tablette-libre-a-monter-soit-meme
Title:   Dukepad: une tablette libre à monter soit même
Authors: devnewton
Date:    2013-09-27T10:01:05+02:00
License: CC By-SA
Tags:    java, tablette, tablette_libre_raspberypi et debian
Score:   19


Bonjour Nal,

Je viens de découvrir sur le wiki de l'openjdk un projet de tablette à monter soit même: la [DukePad](https://wiki.openjdk.java.net/display/OpenJFX/DukePad).

Sur une base de Raspberry PI et Debian avec une couche graphique en JavaFX, le site propose des plans et des instructions pour la construire et l'installer.

Je n'ai jamais trop compris à quoi sert une tablette par rapport à un portable, mais je trouve le projet intéressant.

![DukePad](DukePad-Front-Home-Screen-W.jpg)

![Plan](DukePad_2.7_3mm_rev_2.jpg)
