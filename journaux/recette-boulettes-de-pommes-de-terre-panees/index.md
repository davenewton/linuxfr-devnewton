URL:     https://linuxfr.org/users/devnewton/journaux/recette-boulettes-de-pommes-de-terre-panees
Title:   Recette: boulettes de pommes de terre panées
Authors: devnewton
Date:    2013-08-10T14:59:02+02:00
License: CC By-SA
Tags:    cuisinefrorg, cuisine et recette_de_cuisine
Score:   1


Ah Nal,

Hier soir je n'avais plus grand chose dans ma cuisine pour me faire à manger: un oeuf, du pain sec, quelques patates... Heureusement même avec peu de choses, il est possible de bien manger!

Ingrédients pour deux personnes _normales_
==========================================

- trois pommes de terre (ou potatines, comme on doit certainement les appeler à Toulouse).
- un oeuf.
- du pain sec ou de la chapelure toute prête.
- des herbes (Origan et Thym par exemple).

Préparation
===========

1. Mettre de l'eau à bouillir.
2. Couper les pommes de terre en 4.
3. Mettre les pommes de terre dans l'eau et laisser cuire jusqu'à ce qu'elles deviennent molles.
4. Egouter, ajouter les herbes et écraser mais pas jusqu'à en faire de la purée.
5. Former des boulettes grosses comme des boules de billards.
6. Battre un oeuf dans une assiette creuse ou un bol.
7. Mixer le pain sec et disposer la chapelure dans une assiette.
8. Tremper les boulettes dans l’œuf puis dans la chapelure.
9. Faire cuire à la vapeur ou à la poêle.

Ces boulettes se mangent seule ou avec une viande.

Bonus
=====

Il est possible d'ajouter du fromage aux boulettes pour plus de gras!

Bon appétit Nal!

[nimage](http://tof.canardpc.com/view/8adef8b5-29ad-4e54-8248-692c4dd7d3cb.jpg)
