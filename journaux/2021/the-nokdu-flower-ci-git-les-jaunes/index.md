URL:     https://linuxfr.org/users/devnewton/journaux/the-nokdu-flower-ci-git-les-jaunes
Title:   The Nokdu Flower : ci-git les jaunes
Authors: devnewton
Date:    2021-09-17T00:32:15+02:00
License: CC By-SA
Tags:    cinéma, série, coree, histoire et hors_sujet
Score:   23


Bonjour Nal,

Je t'écris pour te recommander une série dont je termine à l'instant le télévisionnage: the Nokdu Flower.

C'est coréen, historique et social, bref tout pour plaire aux moules de gauche. Il y aussi de l'humour familial non offensant pour les moules du centre et même quelques romances pour les moules partouzeuses de droite.

L'action se situe à la fin du XIX° siècle dans le Royaume de Joseon, aussi appelée dynastie Yi. Du coup tout le monde s'appelle Yi, ce qui n'est pas très pratique pour suivre les interactions entre les 42 personnages.

Une rébellion vient d'éclater : un mouvement religieux ressemblant pourtant au christianisme va tirer des conclusions différentes de l'ubiquisme. Puisque que Dieu est partout et en chacun, on est tous égaux alors au lieu de rendre à Yi ce qui est à Yi et de tendre la joue droite, prenons les armes pour construire une société égalitaire et humaniste à coup de lances de bamboo dans la gueule des nobles et du patronariat. Ils veulent moins d'impôts, du riz, le RIC, le Morty, 100 wons et un pepero.

Dans ce contexte, deux frères, Yi et Yi, vont se retrouver dans des camps différents.

![Yi et Yo](yi_et_yo.jpg)

(Pour illustrer ça, ils les ont mis à gauche et à droite d'un arbre, ça c'est surpuissant comme métaphore).

Pourtant au départ ils travaillent ensemble auprès de leur père Yi, notable de la région, qui veut faire du premier un ministre en lui faisant passer le concours de l'ENA du coin et du second un préfet pour tabasser ses opposants à coups de matraque avec les CRS de l'époque.

![Papa Yi](papa_yi.jpg)

(Le père Yi est une crevure de première, entre le macroniste et le ripoublicain).

La révolte traverse leur ville et Yi se retrouve face à son chef : Yi De Graine. On le surnomme comme ça, car il fait toujours des métaphores pourries sur les petites graines qui poussent et donnent d'autres graines. Toujours est-il que Yi va battre Yi mais lui donner une seconde chance s'il arrête de tabasser les pauvres. Pour le convaincre, il le tabasse.

![Yi De Graine](yi_de_graine.png)

(L'acteur qui joue Yi De Graine a l'air d'un vieux sage, mais le personnage historique a plus la tronche d'un cgtiste)

Yi de son côté est amoureux de Yi, frère d'un noble (Yi) qui lui dit _Oh comment tu parles à ma sœur ?_. Tout s'aggrave en un quart d'heure quand Yi fait en sorte que Yi soit incorporé de force dans l'armée qui doit réprimer la révolte que va rejoindre son frère Yi !

Ce dernier tombe amoureux de Yi, une marchande qui pendant toute la série va prendre l'air de souffrir d'atroces dilemmes moraux alors qu'elle choisit systématiquement de vendre des armes et de l'équipement aux plus forts du moment. Spoiler: elle finit plein aux as, mais avec toujours une petite larme, sans doute de ne pas avoir vendu assez cher.

![Yi et Yi](the-nokdu-flower-01.jpg)

(Les couples passent au moins 18 épisodes sans se faire un bisou sur le front. On est au XIX° siècle, on ne fait pas le donkey punch dès le premier soir).

Si tu es perdu Nal, ce schéma te montrera bien qui est qui parmi les personnages principaux:

![Yi est Yi](Nokdu-Flower-Relationship-Chart.jpg)

Il y a aussi toute une galerie de personnages secondaires que je te laisse découvrir avec plaisir comme Yi Fernandel et Yi Melenchon:

![Yi Fernandel](yi_fernandel.png)

![Yi Mélenchon](yi_melenchon.png)

Bon télévisionnage Nal !

