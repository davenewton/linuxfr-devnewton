URL:     https://linuxfr.org/users/devnewton/journaux/java-17-lts
Title:   Java 17 LTS
Authors: devnewton
Date:    2021-09-15T09:49:34+02:00
License: CC By-SA
Tags:    java
Score:   25


Bonjour Nal,

Je t'écris pour te signaler la sortie de la nouvelle version avec support à long terme de Java.

Celle-ci contient beaucoup de nouveautés voici les plus importes à mes yeux:

- la dépréciation de l'API Applet ;
- le filtrage par motif pour l'instruction switch ;
- les classes scellées ;
- une API vectorielle.

Si comme moi tu n'utilises que les versions LTS, il y aussi quelques changements intéressants depuis la version 11 :

- le portage sur Alpine Linux ;
- les records ;
- les blocs de texte.

https://openjdk.java.net/projects/jdk/17/
