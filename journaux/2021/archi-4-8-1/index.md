URL:     https://linuxfr.org/users/devnewton/journaux/archi-4-8-1
Title:   Archi 4.8.1
Authors: devnewton
Date:    2021-06-02T15:09:52+02:00
License: CC By-SA
Tags:    architecture et modélisation
Score:   23


Bonjour Nal,

Je t'écris pour te signaler la sortie d'[Archi](https://www.archimatetool.com/) en version 4.8.1.

Archi est un outil de modélisation pour le langage de modélisation [Archimate](https://en.wikipedia.org/wiki/ArchiMate) dédié aux études d'urbanisation des organisations* en général et des systèmes d'information en particulier.

Plutôt qu'une longue présentation, voici un petit exemple de schéma d'une partie de l'urbanisation d'une organisation fictive qui voudrait mettre en place un programme de transformation sociale, écologique et humaniste:

![vive jean luc](FI.png)

Ici on voit une analyse qui conduit à la fixation d'un objectif et de son résultat. Pour atteindre cette objectif, on mets en place un processus métier (que l'on pourra modéliser en BPMN par exemple) qui donne lieu à un livrable. Ce processus métier respecte un principe d'architecture et est réalisé par un processus applicatif lui même implémenté par un composant logicielle qui repose sur un socle technique (libre bien sûr).

Pour en savoir plus sur Archimate, je te conseille cette excellente page oueb: [Archimate: Présentation](https://emmanuelgeorjon.com/architecture/archimate-presentation/).

*: Les anglosaxois parlent d'Enterprise Architecture, car ce sont des capitalistes bourgeois à la solde du patronariat qui pensent que la seule forme valable d'organisation est l'entreprise privée, mais heureusement ils quittent bientôt l'Union Européenne et on ne devrait plus jamais en entendre parler.
