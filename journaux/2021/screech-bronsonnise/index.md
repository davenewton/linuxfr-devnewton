URL:     https://linuxfr.org/users/devnewton/journaux/screech-bronsonnise
Title:   Screech bronsonnisé
Authors: devnewton
Date:    2021-02-01T21:57:22+01:00
License: CC By-SA
Tags:    
Score:   15


Ah Nal,

J'apprends à l'instant la mort de Dustin Diamond, l'un des acteurs qui jouait dans la série _Sauvez par le gong_.

Comme les sales djeuns autour de moi ne saisissent pas l'importance de ce drame planétaire, je me permets de partager cette nouvelle avec toi.

![oh](screech.jpg)

PS: l'animateur de l'émission [CPU](https://cpu.dascritch.net/) lui semble toujours vivant.
