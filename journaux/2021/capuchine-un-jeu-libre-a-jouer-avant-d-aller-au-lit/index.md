URL:     https://linuxfr.org/users/devnewton/journaux/capuchine-un-jeu-libre-a-jouer-avant-d-aller-au-lit
Title:   Capuchine: un jeu libre à jouer avant d'aller au lit
Authors: devnewton
Date:    2021-11-09T22:22:29+01:00
License: CC By-SA
Tags:    jeu_libre, jeu_vidéo et capuchine
Score:   41


Bonjour Nal,

Je t'écris pour te parler de ma dernière création: [Capuchine](https://play.devnewton.fr/capuchine).

![capuchine](capuchine.jpg)

Il s'agit d'un jeu éducatif et... Non mais attends reviens ! C'est pas éducatif, genre maths & français. Voilà reste là, de toute façon la porte est verrouillée. Oui la fenêtre aussi.

Bref je disais donc qu'il s'agit d'un jeu éducatif qui a pour but d'apprendre aux enfants à jouer aux jeux vidéos.

Tu vas me dire qu'ils n'ont pas besoin de ça et que toi de ton temps tu as appris à jouer sur consoles 8 ou 16 bits à des jeux considérés aujourd'hui comme hardcore, mais j'ai testé sur ma fille: à trois ans, elle a encore du mal à sortir des furys sur Mortal Kombat.

Je lui ai donc développer un jeu plus adapté à son âge en m'inspirant d'un [conte](https://fr.wikipedia.org/wiki/Urashima_Tar%C5%8D) et en simplifiant au maximum le gameplay. Il est même impossible de perdre et la seule victoire est le plaisir de jouer.

Oui c'est aussi un jeu pour les générations de gros noobs fragiles.

Mais plutôt qu'un jeu classique, il faut le voir comme une histoire interactive à raconter à ton enfant avant d'aller au lit.

Si tu n'as pas d'enfant, tu trouveras plein de vidéos éducatives sur internet qui montrent comment en faire.

Je te postais il y a peu d'[Opensara](https://linuxfr.org/users/devnewton/journaux/opensara-un-nouveau-jeu-libre), le code source de [Capuchine](https://framagit.org/devnewton/capuchine) en est dérivé.

J'ai fait testé le jeu à ma fille, elle a bien aimé et m'a demandé de lui faire un autre jeu _avec un chien qui fait de l'escalade_.

Plus qu'à trouver un gameplay qui va avec cette "spec"...


