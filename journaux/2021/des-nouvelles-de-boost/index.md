URL:     https://linuxfr.org/users/devnewton/journaux/des-nouvelles-de-boost
Title:   Des nouvelles de boost
Authors: devnewton
Date:    2021-02-23T14:31:02+01:00
License: CC By-SA
Tags:    boost et cpp
Score:   21


Bonjour Nal,

Je ne fais plus de C++ depuis longtemps, mais je regarde parfois l'actualité de ce langage.

Sa meilleure bibliothèque est sortie en version 1.75 depuis et propose des nouveautés intéressantes:

Tout d'abord un [parseur JSON](https://www.boost.org/doc/libs/1_75_0/libs/json/doc/html/json/quick_look.html) vraiment simple:
    
    auto george = boost::json::parse( R"( { message: "Monde de merde" } )" );
    std::cout << george["message"] << std::endl;

Ensuite un gestionnaire d'erreur qui ressemble un peu à ce que fait [Go avec des valeurs de retours multiples](https://www.boost.org/doc/libs/1_75_0/libs/leaf/doc/html/index.html#introduction).

D'autres libs plus anciennes sont régulièrement mises à jour, par exemple :

- [beast](https://www.boost.org/doc/libs/1_75_0/libs/beast/doc/html/index.html) pour faire des clients et serveurs http mieux qu'en Node.js ;
- [smart_ptr](https://www.boost.org/doc/libs/1_75_0/libs/smart_ptr/doc/html/smart_ptr.html) pour être plus memory safe que Rust tout en gardant un langage plus simple et portable;
- [tribool](https://www.boost.org/doc/libs/1_75_0/doc/html/tribool.html) pour les non binaires, les centristes et les indécis ;
- et [bien plus encore](https://www.boost.org/doc/libs/1_75_0/?sort=boost-version).

