URL:     https://linuxfr.org/users/devnewton/journaux/tiled-1-6-attention-ca-va-tuiler-cherie
Title:   Tiled 1.6 : attention ça va tuiler chérie
Authors: devnewton
Date:    2021-05-10T18:18:21+02:00
License: CC By-SA
Tags:    tiled et jeu_vidéo
Score:   48


Bonjour Nal,

Je t'écris pour te tenir à l'énergie électrique de la sortie de [Tiled](https://www.mapeditor.org/), l'éditeur de niveau [libre](https://github.com/mapeditor/tiled) que j'utilise pour mes [jeux](https://play.devnewton.fr/).

Depuis la dernière fois que je t'en ai posté, il s'est enrichi de nouvelles fritures intéressantes.

Tout d'abord un outil pour créer des terrains facilement avec des tuiles de [Wang façon blob](http://www.cr31.co.uk/stagecast/wang/blob.html), un concept issu des mathématiques qui permets à partir d'un nombre limité de dessins de créer automagiquement des cartes avec des transitions douces :

![wang wang in the wutt](2021-03-edge-painting.gif)

Ensuite il est maintenant possible de prévisualiser les défilements paralaxatifs sans en chier:

![par la axe](parallax.webp)

Enfin Tiled est maintenant scriptable avec le langage de script de [Brendan Eich](https://www.mapeditor.org/docs/scripting/).

Il manque encore quelques fonctionnalités importantes à mes yeux (gérer des couches avec des tailles de tuiles différentes ou des spritesheets non alignés par exemple), mais aujourd'hui Tiled est sans doute le meilleur éditeur de niveau du marché et je compte l'utiliser pour mon futur jeu.

