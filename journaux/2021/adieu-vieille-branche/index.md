URL:     https://linuxfr.org/users/devnewton/journaux/adieu-vieille-branche
Title:   Adieu vieille branche
Authors: devnewton
Date:    2021-03-16T16:02:02+01:00
License: CC By-SA
Tags:    racisme
Score:   19


Ah Nal,

Je t'écris pour te prévenir de faire attention à des dépôts _con_, car Gitlab, à la suite de la PME Github, a à son tour décider renommer la branche par défaut de ses dépôts en _main_.

Je ne comprends pas trop pourquoi (une histoire de revendication syndicale de planteurs de coton je crois), j'aurais préféré le terme _tronc_ plus parlant pour une branche.

En tout cas cette main, c'est pas le pied.

[L'annonce de Gitlab](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/)
