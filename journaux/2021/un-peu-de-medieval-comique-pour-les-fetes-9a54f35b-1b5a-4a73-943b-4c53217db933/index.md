URL:     https://linuxfr.org/users/devnewton/journaux/un-peu-de-medieval-comique-pour-les-fetes-9a54f35b-1b5a-4a73-943b-4c53217db933
Title:   Un peu de médiéval comique pour les fêtes : 
Authors: devnewton
Date:    2021-12-23T18:17:50+01:00
License: CC By-SA
Tags:    médiéval-fantastique, comédie et série
Score:   10


Bonjour Nal,

Je t’écris pour te suggérer une série du genre médiéval comique (ou comic fantasy ou encore word & sorcery) à voir en famille pour les fêtes : [Wizards and warriors](http://www.wizardsandwarriors.org/).

![wizards and warriors](wizards-and-warriors-cancelled.jpg)

Tu te souviens sans doute de ces montages _Et si Game of Thrones était une vieille série?_: et bien c'est exactement ça. Les acteurs ont des coupes que l'on trouve aujourd'hui ridicule (mais souviens toi tu avais toi même une coupe mulet), les effets spéciaux sont des montages peu réussis mais on s'amuse beaucoup.

Wizards and warriors raconte des aventures dans le royaume d'Aperans en conflit avec ses voisins... Oh et puis l'histoire on s'en moque: il y a un gentil prince blond et un méchant prince brun, un magicien blanc et un sorcier noir, une gentille princesse nunuche et une vilaine sorcière en bikini.

![bethela](bethela.jpg)

Les acteurs prennent beaucoup de plaisirs à jouer ces personnages dans des situations plus clichés les unes que les autres:

- le marchandage qui se termine avec l'échange d'une bourse de cuir sorti de l'hyperespace avec la somme exacte ;
- les gentils attachés au dessus d'un gouffre dans lequel on les fait descendre doucement et qu'on ne surveille plus, car ils n'ont AUCUNE chance de s'en sortir ;
- la princesse qui se fait enlever et est tellement pénible que ses ravisseurs le regrettent vite.

Avec un budget de 42$ par épisode, les décors sont en carton, voire en papier (une partie des paysages sont des dessins qui ne collent pas du tout avec le reste), mais si les réalisateurs n'ont pas d'argent, ils ont des idées: une scène avec un dragon? Le dragon sera un dragon "invisible", on ne verra que les flammes qu'il crache et hop un simple allume barbecue suffira. Et ça marche: grâce à la bonne humeur générale, c'est plus réussi que le dragon de la série The Witcher.

Comme toujours, les personnages les plus réussis sont les méchants et notamment le sorcier Vector qui est pourtant loin d'être une flèche: dans le premier épisode, son plan génial consiste à cacher une bombe dans un château et de s'y rendre sans escorte pour en menacer les occupants. Ce scénario fantasmé par les partisans de la torture et les fans de Jack Bauer qui n'arrive jamais dans la vraie vie n'aura pas le dénouement attendu puisque les héros de Wizards and Warriors sont gentils voire bien gentils.

![vector](vector.jpg)

Bref une bonne série pour les amateurs de nanars fantastiques !

Et toi Nal, qu'as tu ouatché pour noël?
