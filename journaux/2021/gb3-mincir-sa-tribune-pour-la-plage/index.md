URL:     https://linuxfr.org/users/devnewton/journaux/gb3-mincir-sa-tribune-pour-la-plage
Title:   gb3: mincir sa tribune pour la plage
Authors: devnewton
Date:    2021-06-11T14:36:43+02:00
License: CC By-SA
Tags:    coincoin, tribune et golang
Score:   22


Bonjour Nal,

Si tu suis mes aventures, tu sais que je développe régulièrement des tribunes. Que ce soit pour tester un [concept](https://linuxfr.org/users/devnewton/journaux/une-tribune-decentralisee-est-elle-possible), enrichir un [produit existant](https://linuxfr.org/users/devnewton/journaux/une-tribune-pour-le-cms-grav) ou tester des technos.

C'était le cas avec [jb3](https://linuxfr.org/users/devnewton/journaux/jb3-la-tribune-des-beaux-gosses), la tribune la plus friturée du marché mais aussi la plus bloated: Java, Spring Boot, Mongodb et maintenant Node-Red parce que c'est rigolo.

J'avais parfois des envies de [minimalisme](https://linuxfr.org/users/devnewton/journaux/taab-une-tribune-sans-xml), mais quand c'est trop rikiki, ça manque de sensation.

Aujourd'hui je t'écris, car j'ai eu un nouveau prétexte (tester Golang) pour écrire une nouvelle suite logicielle de moulage: [gb3](https://gb3.devnewton.fr/).

- La [démo](https://gb3.devnewton.fr/).
- [Les sources](https://gitlab.com/davenewton/gb3) (licence MIT)

Architecture: TOGAF ready
-------------------------

gb3 se compose de trois composants:

- gc2, un coincoin en vanilla.js ;
- gb0, un bouchot sans IHM en Golang qui utilise le système de fichier comme base de données;
- gb2c, un agrégateur des meilleures tribunes du web en Golang. Il sert de dauphin à gc2.

![architecture](archi.png)

Cette architecture d'applications à douze facteurs respecte peut être la philosophie Unix, le modèle C4, le design en microservices, ainsi que toutes les bonnes pratiques connues ou inconnues de devoups à ce jour, notamment la couverture de 100% de tests unitaires sur la prod grâce la méthode Production Unit Testing Enhancement (PUTE).

Avec cette méthode, on teste les fonctionnalités une par une en production en attendant que les utilisateurs se plaignent pour corriger les bugs. Le logiciel en production est donc toujours au top sauf quand il ne marche pas.

Fritures: less is less
----------------------

Ma précédente solution moulesque avait beaucoup de fritures, au prix d'une certaine complexitude. Cette fois j'ai décidé de n'implémenter que des fonctionnalités que j'utilise au quotidien (moulage multi tribune, totoz, emoji, images jointes) et d'abandonner les rigolotes mais peu utiles (bots, trollomètre) et les problématiques pour la privacité des moules (présence en ligne, archivage de longue durée).

J'ai aussi réduit le nombre de paramétrage (on ne peut plus créer de tribune perso, la liste des tribunes est fixe). Bref un processus de gnomisation qui permets d'avoir un logiciel maintenable et sain pour les utilisateurs puisque le développeur sait toujours mieux qu'eux ce dont ils n'ont pas besoin.

Go goal?
--------

Le but de ce projet était d'évaluer Go. Pendant tout le développement je me suis répété _c'est nul à chier ce langage en plus les libs sont pas top_ et puis à la fin le résultat est simple à maintenir et fiable.

Et puis ça consomme un poil moins de RAM:

![rame](ram.png) (NdM: récupérée via webarchive)

Bref je suis mitigé<.

Et si je recommençais en Rust cette fois?
