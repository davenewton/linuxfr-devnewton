URL:     https://linuxfr.org/users/devnewton/journaux/moulebouffe-parisienne-ce-soir
Title:   Moulebouffe parisienne ce soir
Authors: devnewton
Date:    2016-07-30T09:53:49+02:00
License: CC By-SA
Tags:    bouffe
Score:   17


Bonjour Nal,

Je t'écris pour t'informer d'un évènement important: une moulebouffe aura lieu ce samedi 30 juillet à 20h dans le riant dixième arrondissement de la Capitale de La République Française.

Pour rendre hommage au glorieux passé colonial du pays et au multiculturalisme triomphant du présent, le restaurant choisi est libanais.

Pour cette occasion, de nombreuses guests stars seront présentes:

- Chrisix, historien de la moulosphère.
- LiNuCe, créateur du c² mondialement connu gcoincoin.
- Deeplop, bot pertinent.
- Domi, expert en orthographe.
- Dave Newton, survivant de l'attentat de Nice.

Il n'y a pas de thème imposé, mais il est certain que l'on évoquera des sujets comme l'avenir des bouchots et des c², le programme politique de Jean Luc Mélenchon ou l'incubation d'excellence en milieu urbain.

En bref:

- une soirée avec des moules de qualitai
- un bon restaurant: http://www.restaurant-libanais-assanabel.com/  
- un rendez vous à 20h au 6 rue Pierre Chausson 75010
