URL:     https://linuxfr.org/users/devnewton/journaux/une-recette-pour-auto-heberger-sa-boulangerie
Title:   Une recette pour auto-héberger sa boulangerie
Authors: devnewton
Date:    2013-06-05T12:23:36+02:00
License: CC By-SA
Tags:    boulangerie, pain, cuisine, journal_du_mois_2013, auto-hébergement et recette_de_cuisine
Score:   43


Peu de gens le savent, mais il est parfaitement possible de faire du bon pain à domicile sans machine.

Voici la recette que j'utilise depuis des années.

Les ingrédients et matériels nécessaires sont:

- 500g de farine.
- un sachet de levure boulangère.
- 1,5 cuillère à café de sel.
- 300g d'eau tiède.
- un grand récipient.
- une cuillère en bois.
- du papier cuisson.
- un four.

La fabrication se fait en sept étapes:

1. Commencez par mélanger la farine, la levure et le sel dans le récipient.
2. Ajoutez l'eau et remuez avec la cuillère en bois jusqu'à obtenir une pâte uniforme.
3. Mettre un torchon sur le récipient et laissez reposer pendant 30 minutes dans l'endroit le plus chaud de votre demeure.
4. Allonger le papier cuisson sur la plaque du four et saupoudrer de farine.
5. Mettre la pâte dessus et lui donner une forme allongée ou ronde selon votre envie.
6. Poser un torchon dessus et attendre 45 minutes.
7. Enlever le torchon, arroser d'un peu d'eau et mettre au four à 200° pendant 30 minutes.

Vous obtiendrez ainsi un pain meilleur que dans la plupart des boulangeries parisiennes qui peut être consommé pendant 4 à 5 jours.

Il est possible de varier la recette en mixant la farine de blé avec d'autres. La farine de châtaigne par exemple donne un pain excellent pour accompagner le fromage et la confiture.

Attention: certaines personnes considèrent que l'auto-hébergement de boulangerie est un truc de bobo/hipster qui ruine les commerces. Si vous les recevez à dîner, n'oublier pas de leur prendre une baguette industrielle pour respecter leurs convictions.
