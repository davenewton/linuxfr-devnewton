URL:     https://linuxfr.org/users/devnewton/journaux/une-tribune-pour-le-cms-grav
Title:   Une tribune pour le CMS grav
Authors: devnewton
Date:    2018-09-17T12:21:20+02:00
License: CC By-SA
Tags:    grav, cms, php, plugin, tribune, bouchot et chat
Score:   6


Bonjour Nal,

Je t'écris pour te présenter un plugin que j'ai eu à écrire pour le [CMS Grav](https://getgrav.org/): il s'agit d'une tribune semblable à celle de [linuxfr](https://linuxfr.org/board).

![gpt](gpt.png)

Conforme au [standard coutumier](https://rfm.devnewton.fr) des tribunes de la moulosphère, il est utilisable via la plupart des coincoins, tel que l'excellent [QuteQoin](https://github.com/dguihal/quteqoin):

![QuteQoin](http://uploads.euromussels.eu/f.php?h=0-Jf8IW-&d=1)

J'espère que [grav-plugin-tribune alias gpt](https://framagit.org/very-old-devnewton-projects/grav-plugin-tribune) prendra sa place parmi les bouchots facile à installer à côté d'un site comme l'était le [plugin pour Drupal](https://www.drupal.org/project/tribune).
