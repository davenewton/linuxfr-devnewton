URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-au-prochain-pitch-my-game-2013-04-11t19-15-2-rue-de-la-roquette-75011-paris
Title:   Newton Adventure au prochain Pitch My Game ( 2013-04-11T19:15  2 rue de la Roquette 75011 Paris )
Authors: devnewton
Date:    2013-04-02T18:25:43+02:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure, événement et jeu
Score:   11


Bonjour Nal,

Je t'écris pour te signaler que [Newton Adventure](https://play.devnewton.fr) sera présenté par mon avatar IRL lors du prochain [Pitch My Game](http://www.pitchmygame.com/), une conférence parisienne qui a pour but de faire découvrir des projets de jeux vidéos indépendants à un public de passionnés, développeurs, graphistes, musiciens et autres gamedesigners.

Note bien cet évènement dans ton agenda, car tu pourras y découvrir les mods ~~pleins de DRMs et pubs ingame~~ que je prépare ~~en secret~~ depuis des mois.

En attendant tu peux essayer la dernière version, 1.10 de [Newton Adventure](https://play.devnewton.fr) qui apporte quelques nouveautés et corrections:

- des traductions françaises et allemandes.
- l'affichage optionnel des FPS.
- des corrections de bugs avec les textures et la physique.

![Pièce 5 du concours Plee the Bear](http://www.stuff-o-matic.com/ptb/linuxfr/puzzle/puzzle-5.png)
