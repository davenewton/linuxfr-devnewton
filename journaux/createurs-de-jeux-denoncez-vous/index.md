URL:     https://linuxfr.org/users/devnewton/journaux/createurs-de-jeux-denoncez-vous
Title:   Créateurs de jeux, dénoncez vous!
Authors: devnewton
Date:    2012-10-06T15:05:32+02:00
License: CC By-SA
Tags:    jeux_linux et jeu_vidéo
Score:   20


Bonjour Nal,

Je t'écris, car j'ai changé un sprite dans Newton Adventure et... Non pas cette fois!

Je cherche à compléter la page [jeux_libres_linuxfr](/wiki/jeux_libres_linuxfr "Lien du wiki interne LinuxFr.org") qui recense les jeux créés par les linuxfriens. J'en ai trouvé une quinzaine, mais il doit certainement en manquer.

Si vous avez créé un jeu, libre bien sûr, n'hésitez pas à vous faire connaître !

Je profite de ce journal pour partager une url vers le blog d'un gamedesigner qui s'est lancé un défi consistant à documenter 300 idées de gameplay : http://www.squidi.net/three/index.php
