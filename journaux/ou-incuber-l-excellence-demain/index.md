URL:     https://linuxfr.org/users/devnewton/journaux/ou-incuber-l-excellence-demain
Title:   Où incuber l'excellence demain?
Authors: devnewton
Date:    2013-04-16T12:24:45+02:00
License: CC By-SA
Tags:    tribune, pacliquai et chauve
Score:   5


Bonjour Nal,

Je t'écris, car je viens de voir l'annonce suivante:

> Intervention prévue sur le serveur hébergeant LinuxFr.org mercredi 17 avril dans l'après-midi. Cela va entraîner une coupure de service d'une durée indéterminée.

Puisque de nombreuses personnes ce sont retrouvées fort dépourvues, lorsque la dernière coupure est survenue, voici une liste des sites alternatifs pour l'enrichissement personnel dans la bonne humeur:

- http://euromussels.eu/tribune
- http://hadoken.free.fr/board/
- http://dax.sveetch.net/tribune/
- https://gb3.devnewton.fr
