URL:     https://linuxfr.org/users/devnewton/journaux/un-peu-de-medieval-comique-pour-les-fetes
Title:   Un peu de médiéval comique pour les fêtes
Authors: devnewton
Date:    2019-12-26T18:24:52+01:00
License: CC By-SA
Tags:    série, comédie et médiéval-fantastique
Score:   14


Bonjour Nal,

Je t’écris pour te suggérer trois séries du genre médiéval comique (ou _comic fantasy_ ou encore _word & sorcery_) à voir ou à revoir en famille pour les fêtes.

Kröd Mändoon and the Flaming Sword of Fire
==========================================
Cette série britannique nous fait suivre les aventures de Kröd Mändonn, sorte de Robin des bois, prenant part au mouvement de résistance face au lâche et cruel Chancelier Dongalor.
    
Proche de l’esprit du donjon de Naheulbeuk, on y croise un magicien qui trouve toujours une excuse pour ne pas lancer de sort, un cyclope amateur de prosciutto et une guerrière à la cuisse légère.
    
![Kröd Mändoon](krod-mandoon-wide.jpg)

Galavant
========
>    _♪ Way back in days of olde
>    There was a legend told
>    About a hero known as Galavant ♪_
    
Cette série nous fait suivre les aventures de Galavant, héro beau, fort et rusé, mais éconduit par sa dulcinée dès le pilote, car elle préfère l’argent et le pouvoir.
    
À moitié comédie musicale, chaque épisode comporte des chansons héroïques ou pathétiques. On y croise de grands nains qui combattent de petits géants, un roi puceau suivi partout par une licorne et Kylie Minogue en tenancière d’une auberge gay. 
    
![Galavant](galavant_season_2_poster.jpg)

The Witcher
===========
Petite production indépendante du collectif d’artistes Toileflims, Le Ouicheur est une parodie des romans d’Andrzej Sapkowski : Gérard de La Riviera, pilier d’auberge, finance ses beuveries en chassant des monstres.
    
Avec un casting particulièrement savoureux, on y croise un elfe noir façon Jackson five, une Triss qui ressemble à une poissonnière et un djinn qui est juste la fumée d’un pneu brûlé.

Le détail qui tue : quand Gérard négocie le prix de sa prestation, celui qui l’embauche finit toujours par sortir une bourse de cuir avec la somme exacte à l’Oren prêt, même s’il n’avait aucune idée de la somme qui serait convenue.
    
Une série si drôle qu’ils devraient en faire un jeu !
    
![Le Ouicheur](witcher.jpg)
