URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-6-6-bonus
Title:   Newton Adventure la soluce 6/6: Bonus
Authors: devnewton
Date:    2012-09-22T13:03:45+02:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure et jeu_vidéo
Score:   4


Bonjour Nal,

Ce nourjal est le dernier d'une série de six épisodes proposant la soluce de Newton Adventure.

![Bonus](bonus.jpg)

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Les niveaux bonus
=================

Les niveaux bonus sont accessibles en utilisant un téléporteur spécial:

![teleporteur](teleporteur.jpg)

Pour pouvoir l'emprunter, il faut d'abord récolter toutes les pommes du niveau en cours. Le téléporteur change alors de couleur et permet d'accéder à un niveau bonus.

Ces niveaux permettent de récolter un grand nombre de pièces qui ajoute beaucoup de points au score final de la quête en cours. Ce score final peut être envoyé sur mon serveur de score, une application web écrite avec python/django.

Trouver les pommes
==================

Partir à la cueillette des pommes n'est pas facile, j'ai créé les cartes des niveaux du jeu qui permettent leur récolte afin de servir de soluce.

todo://retrouverlavideocartes/

![Cartes](cartes.jpg)

The end
=======

Je travaille sur quatre nouvelles quêtes et l'ajout de mécanismes pour renouveler le jeu. En attendant, voici l'obligatoire [nimage](nimage.jpg) de fin.
