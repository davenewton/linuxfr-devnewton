URL:     https://linuxfr.org/users/devnewton/journaux/java-xii-est-dehors
Title:   Java XII est dehors
Authors: devnewton
Date:    2019-03-26T10:10:23+01:00
License: CC By-SA
Tags:    java et entreprise
Score:   32


Ah Nal,

Je sais que les projets de ta SSII sont encore sous Java 1.6, mais il est temps de migrer: la douzième version du langage libre le plus populaire en entreprise vient de sortir.

Les nouveautés sont peu nombreuses, mais sympathiques.

Expression d'aiguillage
-----------------------

    boolean joursOuvrees = switch (jour) {
        case LUNDI, MARDI, MERCREDI, JEUDI, VENDREDI -> true;
        case SAMEDI, DIMANCHE                -> false;
    };



Unicode 11
----------

Avec 66 nouveaux emojis 💩 et les nombres mayas pour recalculer la fin du monde.

Attention ça va déprécier chérie
--------------------------------

Des classes et méthodes sont retirées pour alléger le JDK. N'oublie pas de rappeler à tes collègues roumains et indiens qu'il serait temps d'arrêter utiliser des classes de _com.sun.*_.

[Openjdk 12](https://openjdk.java.net/projects/jdk/12/)
