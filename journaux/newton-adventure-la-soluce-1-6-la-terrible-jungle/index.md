URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-1-6-la-terrible-jungle
Title:   Newton Adventure la soluce 1/6: la terrible jungle
Authors: devnewton
Date:    2012-08-21T08:53:26+02:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure et jeu_vidéo
Score:   10


Bonjour Nal,

Ce nourjal est le premier d'une série de six épisodes proposant la soluce en vidéo de [Newton Adventure](https://play.devnewton.fr).

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Making off
==========

Pour produire ces vidéos, j'ai utilisé [apitrace](https://github.com/apitrace/apitrace), un utilitaire qui permet d'enregistrer les appels à opengl dans un fichier puis les rejouer, en générant une image ppm à chaque frame.

    LD_PRELOAD=/opt/apitrace/wrappers/glxtrace.so java -jar newton_adventure-1.6-SNAPSHOT.jar

Mon PC de jeu sous Windows étant plus puissant que mon serveur/poste de développement, j'ai d'abord essayé sous cet OS, mais ça n'a pas fonctionné, sans doute à cause du manque de préparation de ce système pour le bureau.

En pipant ces images à ffmpeg, j'ai obtenu une vidéo au format mpeg.

    glretrace -s - jungle_level0.trace | ffmpeg -r 25 -f image2pipe -vcodec ppm -i pipe: -vcodec mpeg2video -qscale 1 -y jungle_level0.mkv

Puis conversion en theora :

    ffmpeg2theora jungle_level0.mkv -o jungle_level0.ogv

Les vidéos
==========

Les vidéos de la première quête du jeu, jungle, se trouvent [ici](todo://retrouverlavideo) (attention petit serveur autohébergé).

Jungle est une quête assez simple... au début, mais les choses se corsent rapidement et demandent de bien préparer ses sauts. Ces niveaux demandent du joueur de la dextérité là où d'autres quêtes privilégient la réflexion.

![jungle](jungle.jpg)

En attendant la suite, j'espère que vous trouverez l'obligatoire [nimage](todo:retrouverlanimage) de fin esthétique et non sexiste.
