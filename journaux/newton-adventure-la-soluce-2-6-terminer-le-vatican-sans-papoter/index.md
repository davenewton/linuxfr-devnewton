URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-2-6-terminer-le-vatican-sans-papoter
Title:   Newton Adventure la soluce 2/6: terminer le Vatican sans papoter
Authors: devnewton
Date:    2012-08-25T15:11:28+02:00
License: CC By-SA
Tags:    newton_adventure, tiled, jeux_linux et jeu_vidéo
Score:   4


Bonjour Nal,

Ce nourjal est le deuxième d'une série de six épisodes proposant la soluce en vidéo de Newton Adventure.

![Vatican](http://tof.canardpc.com/view/0d5045b2-0d70-4fc0-b178-86aafbd4275a.jpg)

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Les vidéos
==========

Les vidéos de la deuxième quête du jeu, vatican, se trouvent [ici](todo://retrouverlavideo) (attention petit serveur autohébergé, je cherche un site libre friendly où mettre mes vidéos).

Plus difficile que la première quête, jungle, elle est l'une des plus équilibrées du jeu, avec des niveaux qui demandent à la fois de l'habileté pour déplacer Newton à travers des passages remplis de pièges et de bumpers, mais aussi de la réflexion pour trouver comment déclencher les mécanismes qui parsèment les niveaux.

Retours
=======

Certains linuxfriens m'ont fait des retours très pertinents sur la façon de jouer à Newton Adventure dans le précédent [journal](https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-1-6-la-terrible-jungle), je réfléchis à comment en tenir compte, sachant qu'il n'est pas facile de changer un gameplay et level design qui a deux ans.

To fork or not to fork
======================

Une mauvaise nouvelle pour la future refonte graphique: l'auteur de [Tiled](http://www.mapeditor.org/), l'éditeur de niveau que j'utilise, a rejeté un des patchs que je considérais comme essentiel pour continuer.

En effet, comme son nom l'indique Tiled est fait pour les jeux basés sur des tilemaps, cad une grille d'images carrés de taille fixe. Avec la refonte graphique, je voulais pouvoir utiliser des éléments de n'importe quelle forme et taille. Tiled permets d'avoir une couche avec des formes destinés aux collisions. Mon patch permettait de leur affecter une texture.

La question d'un fork se pose donc, sachant que je maintiens déjà une vieille version en Java de Tiled (avec laquelle j'ai dessiné les niveaux actuels), car son auteur a procédé à une réécriture en C++ qui n'est pas capable de relire les anciens fichiers...

A plus dans le bus
==================

En attendant le prochain nourjal, voici la [nimage](http://tof.canardpc.com/view/e02e18ce-59a4-4dd5-b47a-695ce8632cdb.jpg) de fin toujours esthétique et non sexiste.
