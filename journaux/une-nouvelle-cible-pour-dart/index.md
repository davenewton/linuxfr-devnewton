URL:     https://linuxfr.org/users/devnewton/journaux/une-nouvelle-cible-pour-dart
Title:   Une nouvelle cible pour Dart
Authors: devnewton
Date:    2013-11-14T15:26:00+01:00
License: CC By-SA
Tags:    dart et langage
Score:   15


La version 1.0 de Dart SDK vient d'être publiée. Ce langage de programmation a pour but de remplacer Javascript à long terme: plus structuré, plus sûr, plus performant... Les promesses habituelles des nouveaux produits sont là.

Dart propose une approche plus radicale que les solutions concurrentes (Haxe, Typescript, asm.js...): remplacer l'interpréteur javascript. Sur le site il est ainsi possible de télécharger une version patché de Chromium qui embarque un interpréteur Dart. Pour être compatible avec les autres navigateurs, il est possible de compiler le code Dart en javascript. Il est aussi possible de l'utiliser hors d'un navigateur pour écrire programmes en ligne de commande ou des serveurs.

Proche de Java, D ou C#, le langage semble assez simple à apprendre et puissant (closures, mixins, generics...) , mais ne propose pas de features orientées performances (struct, raii, gestion manuelle de la mémoire...). Toutefois l'interpréteur semble bien [se débrouiller](http://www.infoq.com/news/2013/05/Dart-Java-DeltaBlue).

Contrairement à beaucoup de nouveaux langages, l'outillage semble complet: IDE basé sur Eclipse avec débugger, gestionnaire de dépendances, bibliothèques, documentation bien fournie.

A suivre...

https://www.dartlang.org/
http://googledevelopers.blogspot.fr/2013/11/dart-10-stable-sdk-for-structured-web.html
