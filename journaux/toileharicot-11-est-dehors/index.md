URL:     https://linuxfr.org/users/devnewton/journaux/toileharicot-11-est-dehors
Title:   Toileharicot 11 est dehors
Authors: devnewton
Date:    2019-04-09T09:59:34+02:00
License: CC By-SA
Tags:    netbeans, java, ide, gradle, oracle et apache
Score:   10


Bonjour Nal,

Je t'écris pour te signaler la sortie de la nouvelle version du meilleur IDE java/php/node.js du marché : [Netbeans 11](https://netbeans.apache.org/download/nb110/index.html) !

Repris par la fondation Apache (aka la poubelle d'Oracle), le haricot est toujours vivant avec quelques nouveautés:

- la gestion de Java 12.
- la prise en compte de Java EE.
- l'intégration de Gradle.

![new project](https://netbeans.apache.org/download/nb110/nb11-new-project.png)
