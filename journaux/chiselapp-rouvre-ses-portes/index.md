URL:     https://linuxfr.org/users/devnewton/journaux/chiselapp-rouvre-ses-portes
Title:   Chiselapp rouvre ses portes
Authors: devnewton
Date:    2013-06-27T22:30:41+02:00
License: CC By-SA
Tags:    fossil, chiselapp, forge, gestionnaire_de_version et auto-hébergement
Score:   13


[chiselapp](http://chiselapp.com/), le site d'hébergement de dépôts [fossil](http://fossil-scm.org) dont j'avais annoncé l'[abandon par son créateur](https://linuxfr.org/users/devnewton/journaux/chiselapp-ferme-ses-portes), a finalement trouvé un [repreneur](
http://www.mail-archive.com/fossil-users@lists.fossil-scm.org/msg11672.html).

Les amateurs de ce gestionnaire de version qui intègre des fonctionnalités de bug tracker et wiki, en faisant ainsi une vraie petite forge logicielle dans un exécutable de moins de 2mo facile à autohéberger, ne manqueront pas de verser de viriles larmes de bonheur.

[nimage](http://i2.kym-cdn.com/photos/images/newsfeed/000/561/206/a46.png)
