URL:     https://linuxfr.org/users/devnewton/journaux/recette-du-pot-au-feu
Title:   Recette du pot au feu
Authors: devnewton
Date:    2018-05-05T13:14:17+02:00
License: CC By-SA
Tags:    cuisine, tribune, excellence et recette_de_cuisine
Score:   -5


Bonjour,

Je t'écris pour te rappeler qu'aujourd'hui à lieu une marche dans la petite ville de Paris en l'honneur d'un des meilleurs plats de la cuisine française: le pot au feu.

En voici les ingrédients :

- de la viande de bœuf: joue, plat de côte, queue, paleron...
- un os à moelle
- des pommes de terre
- des poireaux
- des carottes
- des navets
- du céleri
- des oignons
- des clous de girofle
- du poivre
- du thym
- de l'ail

Pour la préparation:

- faire revenir la viande dans une marmite avec de l'huile d'olive.
- éplucher et couper les légumes en morceaux.
- verser les ingrédients (sauf les pommes de terre) dans la casserole et remplir d'eau.
- faire cuire à petit feu quelques heures, cuire les pommes de terre dans une casserole 15 minutes.
- avant la fin de cuisson, ajoutez l'os à moelle et les pommes de terre.

Comme la cuisson est un peu longue, je te propose de patienter avec [cette vidéo consacrée à cette marche](https://www.youtube.com/watch?v=Xlem0wUWW6M) et de venir discuter gastronomie sur la [tribune](https://linuxfr.org/board).
