URL:     https://linuxfr.org/users/devnewton/journaux/plusjamaisca-manifestation-en-ligne
Title:   #PlusJamaisCa Manifestation en ligne
Authors: devnewton
Date:    2020-04-25T14:24:10+02:00
License: CC By-SA
Tags:    
Score:   -10


Ah Nal,

Le confinement est bien triste, mais aujourd'hui a lieu une manifestation en ligne qui va l'égayer un peu.

Le concept est original puisque chacun peut générer son affiche revendicative.

[Manif en ligne](https://plusjamaisca.site/)

La mienne:

![teletaf permanent](teletravail.png)

Et toi Nal, quels sont tes revendications?
