URL:     https://linuxfr.org/users/devnewton/journaux/firefox-75-installe-un-mouchard-sous-windows
Title:   Firefox 75 installe un mouchard sous Windows
Authors: devnewton
Date:    2020-04-12T19:11:51+02:00
License: CC By-SA
Tags:    orwell, spyware, fakenews, tabloid, mozilla et firefox
Score:   -34


Ils n'ont pas (encore) osé sous Linux, mais la mofo, qui prétends défendre la privacité de ses utilisateurs, a décidé d'installer un mouchard avec la dernière mise à jour de Firefox.

https://blog.mozilla.org/data/2020/03/16/understanding-default-browser-trends/

Voici deux procédures pour le désactiver:

- https://support.mozilla.org/en-US/kb/telemetry-clientid
- https://www.ghacks.net/2020/04/09/mozilla-installs-scheduled-telemetry-task-on-windows-with-firefox-75/

C'est tellement contradictoire avec le discours marketing de la mofo que d'aucun ose parler de "suicide" pour le brouteur au panda roux.
