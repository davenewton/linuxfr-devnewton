URL:     https://linuxfr.org/users/devnewton/journaux/mozilla-abandonne-le-web-des-objets
Title:   Mozilla abandonne le web des objets
Authors: devnewton
Date:    2020-09-25T14:12:41+02:00
License: CC By-SA
Tags:    mozilla et iot
Score:   19


En lisant le titre du jour Nal, je suppose que tu infères Nal que je vais te poster d'un nieme projet abandonné par la Mofo.

Et bien oui, cette fois il s'agit de [Webthings](https://iot.mozilla.org/about/), le projet qui devait permettre à nos objets connectés d'utiliser HTTPS comme tout bon internaute honnête.

Laissant leurs chaussettes brutes et protocoles maisons, nos objets allaient enfin être accessibles via des urls et communiquer par GET, POST, PUT, DELETE...

Grâce à la [porte d'entrée](https://iot.mozilla.org/gateway/), j'allais enfin pouvoir faire mon café avec `POST https://devnewton.home/cafetiere`, demander si mon pain est cuit `GET https://devnewton.home/four/status` ou faire plaisir à Madame avec `PUT https://mme-devnewton.io/toy`.

Malheureusement cela ne sera peut être pas possible avec l'annonce de [l'abandon par la mofo de ce projet](https://discourse.mozilla.org/t/an-important-update-on-mozilla-webthings/67764).

Espérons que ce projet prometteur et disruptif trouvera d'autres soutiens pour [continuer](https://webthings.io/).

![web of machins](things_ui_screenshot.png)

