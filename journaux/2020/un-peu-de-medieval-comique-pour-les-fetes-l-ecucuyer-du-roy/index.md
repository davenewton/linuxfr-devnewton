URL:     https://linuxfr.org/users/devnewton/journaux/un-peu-de-medieval-comique-pour-les-fetes-l-ecucuyer-du-roy
Title:   Un peu de médiéval comique pour les fêtes : l'écucuyer du Roy
Authors: devnewton
Date:    2020-12-28T17:32:28+01:00
License: CC By-SA
Tags:    comédie et médiéval-fantastique
Score:   5


Bonjour Nal,

Je t’écris pour te suggérer une série du genre médiéval comique (ou comic fantasy ou encore word & sorcery) à voir en famille pour les fêtes : _The Letter for the King_.

Traduit on ne sait pas trop pourquoi par _L'écuyer du Roi_ est l'histoire d'un ado mal peigné à qui l'on confie une quête chronoposte: porter une lettre au roi.

Notre héro aux boucles pas belles et rebelles est un futur chevalier: il renonce pourtant à une vie de gloire et d'honneur pour devenir postier. Son conseiller d'orientation lui a sans doute fait comprendre que quand on a le physique d'un hobbit, il vaut mieux ne pas aller jouer sa vie à l'épée à deux mains sur un champ de bataille.

Cette lettre étant d'un importance capitale pour le destin du royaume, on envoie à sa poursuite de redoutables mercenaires (bon choix) et une bande d'ados chétifs et tout aussi mal peigné (mauvais choix, ils deviendront ses amis alors qu'on leur avait demandé de l'assassiner sauvagement, ah les jeunes aujourd'hui on ne peut plus rien leur confier de sérieux).

![les goonies](goonies.webp)

Comme dans toute bonne aventure, on croisera une galerie de personnage pittoresque: un cheval plus intelligent que le groupe, une fille qui sert de femelle au héro, un sidekick surprise qui fait des blagues, un méchant avec une mèche...

![le mech](mech.jpg)

Pour faire Game of Throne inclusif, Netflix a aussi ajouté une asiatique (elle ne fait pas du kung fu, mais on y était presque), une romance gay subtilement amené (on va mourir demain, on s'encule?) et de la magie vaudou pour scénariste en panne (une situation impossible ? magie magie et vos héros s'en sont sortis !).

Bref une bonne série pour les amateurs de nanars fantastiques !

Et toi Nal, qu'as tu ouatché pour noël?

