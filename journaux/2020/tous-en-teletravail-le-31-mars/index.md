URL:     https://linuxfr.org/users/devnewton/journaux/tous-en-teletravail-le-31-mars
Title:   Tous en télétravail le 31 mars
Authors: devnewton
Date:    2020-03-12T17:11:39+01:00
License: CC By-SA
Tags:    ouin-ouin
Score:   -6


Les syndicats appellent à une journée nationale de grève le 31 mars : https://www.force-ouvriere.fr/IMG/pdf/communique_intersyndical_11_mars_2020.pdf

Comme en informatique, la grève n'est pas très à la mode, je vous propose un mode d'action alternatif: se mettre tous en télétravail ce 31 mars !

Outre le caractère pacifique et apaisé, cela permettra aussi de limiter la progression du coronavirus, de diminuer la pollution liée aux déplacements et de ne pas voir Robert, votre collègue qui fait des blagues du style _tu as pris ton après midi?_ quand quelqu'un part à 18h.
