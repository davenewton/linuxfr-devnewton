URL:     https://linuxfr.org/users/devnewton/journaux/c-vin-va-vous-faire-tourner-en-barrique
Title:   C++ vin va vous faire tourner en barrique !
Authors: devnewton
Date:    2020-09-17T12:17:58+02:00
License: CC By-SA
Tags:    c++
Score:   19


Ah Nal,

Je viens de découvrir avec grand plaisir que C++, mon premier amour de langage, va enfin entrer dans le vingtième siècle en adoptant un système de modules similaire à ce que l'on trouve dans les autres langages.

Ces modules seront introduit dans [C++20](https://www.infoq.com/news/2020/09/cpp-20-final/):

Exemple d'un module dans un fichier duck.cpp:


    export module duck;
    import <iostream>;
     
    export void quack() {
        std::cout << "Coin coin !" << std::endl;
    }


Son utilisation:


    import duck;
     
    int main() {
        quack();
    }


Il y a aussi d'autres nouveautés intéressantes (coroutines, concepts...), mais je ne les attendais pas autant que ces modules qui mettent enfin C++ au niveau de Pascal !
