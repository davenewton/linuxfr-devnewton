URL:     https://linuxfr.org/users/devnewton/journaux/toileharicot-12-est-dehors
Title:   Toileharicot 12 est dehors
Authors: devnewton
Date:    2020-08-19T15:15:59+02:00
License: CC By-SA
Tags:    netbeans, java, php et nodejs
Score:   14


Ah Nal,

Je t'écris pour t'informer que la version 12 de [Netbeans](https://netbeans.apache.org/), le meilleur IDE Java/PHP/Node.js, est en sorti.

Cette version LTS apporte les nouveautés suivantes:

- la gestion des dernières nouveautés de Java (le meilleur langage pour les projets d'entreprise): records, pattern matching, bloc de textes) ;
- la même chose pour PHP (le pire langage pour les projets d'entreprise): typage, nouveaux opérateurs ;
- de nouveaux thèmes.

Oracle, une généreuse PME qui éditait Netbeans avant, a aussi fait don de code concernant C++. Ce langage devrait revenir dans les prochaines versions, ce qui comblera de joie les pauvres développeurs C++ qui doivent en attendant se contenter d'Eclipse ou pire de VSCode.

![netbeans](dark-metal-nimbus-11.3.png)


