URL:     https://linuxfr.org/users/devnewton/journaux/nanimstudio-un-editeur-d-animations-2d
Title:   Nanimstudio, un éditeur d'animations 2d
Authors: devnewton
Date:    2013-02-06T00:20:50+01:00
License: CC By-SA
Tags:    nanim, newton_adventure, animation, jeux_linux et jeu
Score:   26


Bonjour Nal,

Je t'écris pour te faire part de la naissance d'un nouvel outil pour créer des animations 2d: nanimstudio.

Il fait partie de mon projet [nanim](https://framagit.org/very-old-devnewton-projects/nanim), consistant à spécifier un format d'animation pour les jeux vidéos [dont je t'ai déjà parlé](https://linuxfr.org/users/devnewton/journaux/nanim-continue-son-bonhomme-de-chemin) ainsi que des programmes associés.

Créé pour [Newton Adventure](https://play.devnewton.fr), ce format basé sur [protobuf](https://code.google.com/p/protobuf/) permets un chargement rapide de données [optimisées](https://linuxfr.org/users/devnewton/journaux/nanimopt-un-optimiseur-de-nanim) pour les cartes graphiques.

Jusqu'ici je n'avais réalisé que des utilitaires en ligne de commande, comme nanimenc pour encoder les fichiers à partir de png, nanimview pour les visualiser, sheet2nanim pour convertir des tables de sprite...

Comme j'ai eu envie d'avoir un outil plus intuitif et de pouvoir prévisualiser mes changements en temps réel, j'ai pris un peu de temps pour réaliser Nanimstudio.

Assez simple d'utilisation, il présente 3 grands panneaux: un pour importer les images, un pour éditer la liste des animations et un dernier pour éditer les frames d'une animation.

Outre le format nanim, il gère aussi l'import/export de fichiers gif. J'ajouterais la gestion d'autres formats si quelqu'un a en besoin et en faire ainsi un éditeur générique.

Pour essayer ce logiciel libre (licence BSD), tu peux le télécharger sur la page de [nanim](https://framagit.org/very-old-devnewton-projects/nanim).
