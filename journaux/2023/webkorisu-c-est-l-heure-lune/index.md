URL:     https://linuxfr.org/users/devnewton/journaux/webkorisu-c-est-l-heure-lune
Title:   webkorisu : c'est l'heure lune
Authors: devnewton
Date:    2022-11-09T16:37:17+01:00
License: CC By-SA
Tags:    jeu_libre et jeu_vidéo
Score:   23


Bonjour Nal,

Je t'écris pour te présenter la nouvelle version de [mon jeu webkorisu](https://gitlab.com/davenewton/webkorisu).

[Jouable via un brouteur web](https://play.devnewton.fr/webkorisu/), c'est un jeu dont l'objectif est d'aider des écureuils à ramasser des noisettes avec un à quatre joueurs (en branchant des manettes après s'être plaint à Mozilla pour leur demander de gérer correctement le [mapping des boutons](https://bugzilla.mozilla.org/show_bug.cgi?id=1643358) en prenant sur le salaire mirobolant de leur PDG).

Plutôt facile (on ne peut pas perdre), je l'ai conçu pour aider les enfants ou les joueurs _peu bons_ à aborder les jeux de plateforme en douceur.

Cette nouvelle version ajoute des niveaux qui se passent dans une base lunaire avec des noisettes volantes, des téléporteurs et des lasers.

![moon](moon.png)

Et toi Nal, à quoi joues-tu en ce moment ?
