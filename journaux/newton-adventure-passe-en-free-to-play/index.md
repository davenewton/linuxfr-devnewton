URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-passe-en-free-to-play
Title:   Newton Adventure passe en free to play!
Authors: devnewton
Date:    2014-11-10T23:24:24+01:00
License: CC By-SA
Tags:    newton_adventure, java, jeu_libre, crise, moche et jeu
Score:   34


Ah Nal, les temps comme les œufs sont durs!

Suite à la fermeture de mon éditeur, d'ennuis avec Paypal et de soucis dans mon secteur d'activité, j'ai du me résoudre, comme beaucoup de développeurs de jeux vidéos en ce moment, à passer Newton Adventure en free to play.

Les joueurs pauvres ou radins seront contents de pouvoir enfin profiter de contenus jusqu'ici payants:

- 18 nouveaux niveaux dans le jeu principal.
- un mod dit "retro".

J'ai également profité de cette gratuitification* pour améliorer d'autres points:

- j'ai mis à jour la version html5 pour ceux qui ont la flemme d'installer le jeu: attention, un octocore à 5Ghz est nécessaire! Pour des performances au top, je conseille la version java.
- la version android devrait être à peu près jouable (testé sur un Samsung Galaxy Machin).
- un nouveau site plus html5 responsive design w3c compliant.

*: notes qu'il s'agit d'un free to play vraiment free, sans pub, ni achat ingame. Si tu y tiens vraiment, tu peux faire des pauses en criant _Avec linuxfr, mes trolls ont le poil plus brillant!_ ou en brûlant des billets de 50€.

Screenshoutes
===========

Oh de nouveaux niveaux !
-----------

![bridge](049_mod_deluxe_bridge_level4.png)

![lab](055_mod_deluxe_lab_level3.png)

![prison](061_mod_deluxe_prison_level1.png)

Un mod à tomber sur l'écu
-----------

![retro](086_mod_retro_retro3_level3.png)

Newton Adventure warez cdkey
============================

[Site officiel](https://play.devnewton.fr)

Portes toi bien Nal!
