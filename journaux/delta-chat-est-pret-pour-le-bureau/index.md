URL:     https://linuxfr.org/users/devnewton/journaux/delta-chat-est-pret-pour-le-bureau
Title:   Delta Chat est prêt pour le bureau
Authors: devnewton
Date:    2019-02-07T14:13:09+01:00
License: CC By-SA
Tags:    chat, smtp, imap, messagerie, bureau et interopérabilité
Score:   29


[Delta Chat](https://delta.chat/fr/) est un logiciel de messagerie instantanée comme il en existe des milliers: on ajoute des contacts, on crée des groupes et s'envoie des mots doux ou des insultes.

Il a toutefois un avantage majeur sur ses concurrents: tous vos contacts ont déjà un compte sur le réseau qu'il utilise.

Plutôt que de réinventer un nième protocole, les auteurs de Delta Chat ont fait le choix de se baser sur IMAP, SMTP et les diverses RFC qui définissent les courriers électroniques.

Il était déjà disponible pour les OS privateurs mobiles (Android, iOS) et la dernière version est disponible pour des bureaux privateurs (MacOS) et libres (Linux).

![android](2019-01-chat.png)
![ios](ios_screenshot_chat_view.png)
![bureau](desktop-screenshot.png)

Une version Windows est aussi en développement cours.

Je trouve l'initiative très intéressante, les messageries instantanées interopérables étant rare (à part XMPP, je n'en connais pas d'autre).


