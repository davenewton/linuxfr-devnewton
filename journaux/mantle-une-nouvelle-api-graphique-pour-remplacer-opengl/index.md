URL:     https://linuxfr.org/users/devnewton/journaux/mantle-une-nouvelle-api-graphique-pour-remplacer-opengl
Title:   Mantle: une nouvelle API graphique pour remplacer OpenGL
Authors: devnewton
Date:    2013-09-26T14:30:48+02:00
License: CC By-SA
Tags:    gpu, opengl, amd et pilote
Score:   27


AMD vient de présenter une nouvelle API graphique, Mantle, dont le but est de remplacer OpenGL (ainsi que DirectX et diverses API sur le marché de niche des OS Microsoft et sur console). Plus bas niveau, elle permettrait d'obtenir de meilleures performances au prix d'un plus grand effort de développement.

Dans un premier temps spécifique aux GPU de la marque, le développement serait ouvert et permettrait aux constructeurs concurrents d'implémenter leurs propres versions.

Cette annonce arrive un jour trop tôt, mais on s'ennuyait un peu avec Wayland vs Mir.

[source](http://www.techspot.com/news/54134-amd-launches-mantle-api-to-optimize-pc-gpu-performance.html)
