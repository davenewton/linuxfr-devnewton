URL:     https://linuxfr.org/users/devnewton/journaux/panpantempo
Title:   panpantempo
Authors: devnewton
Date:    2018-03-09T21:40:39+01:00
License: CC By-SA
Tags:    taptempo, javascript, panpan, panpantempo et adulte
Score:   43


Ah Nal,

En voyant l'enthousiasme autour de TapTapTempo et notamment la [version js](https://linuxfr.org/users/wawet76/journaux/portage-de-taptempo-en-javascript), j'ai eu l'idée d'en faire une version pour adultes: [panpantempo](https://play.devnewton.fr/panpantempo/).

Au lieu de mesurer le tempo de l'appui sur le clavier ou du clic de souris, panpantempo utilise le micro de la webcam pour détecter le tempo d'une fessée!

Pour le tester, tu peux ouvrir la page https://play.devnewton.fr/panpantempo/ avec ton brouteur préféré, autoriser l'accès au microphone et demander un coup de main à ton·a conjoint·e !

![panpan](https://nsfw.totoz.eu/img/kokia%3A4)
