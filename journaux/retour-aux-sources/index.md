URL:     https://linuxfr.org/users/devnewton/journaux/retour-aux-sources
Title:   Retour aux sources
Authors: devnewton
Date:    2014-09-26T09:28:41+02:00
License: CC By-SA
Tags:    c++, java, cmake, maven, cetaitpasmieuxavant et python
Score:   26


# Amour, gloire et cpp

Bonjour Nal,

Si tu suis régulièrement mes aventures, tu sais que depuis quelques années je n'utilise plus qu'une seule plateforme de développement pour tous mes [projets](https://www.devnewton.fr): Java.

Portable, performant et surtout productif, ce mariage de raison n'a pas empêché quelques aventures avec Python ou Javascript, mais est resté assez solide.

Toutefois je n'ai jamais complètement pu oublier mon premier amour pour le C++, celui-ce ne cessant de se rappeler à mon bon souvenir, me promettant [qu'il n'était plus le même](http://cpprocks.com/c11-a-visual-summary-of-changes/), [qu'il avait changé](https://isocpp.org/blog/2014/08/we-have-cpp14).

J'ai fini par craquer et convenu d'un rendez-vous autour d'un prototype de jeu de sport dont je te reparlerais bientôt j'espère, bien que je ne sache pas si j'irais jusqu'au bout. En effet, la mise en place du projet n'a pas été un retour aux sources chaudes, mais plutôt une douche glacée façon ice bucket challenge avec un seau oublié pendant 3 mois dans un congélateur industriel.

# Le choix des larmes

Le panel de choix de l'outillage a été très décevant.

J'ai essayé quelques IDE, avant de me rendre compte que Eclipse ou Netbeans sont les seuls à proposer une bonne complétion, du refactoring qui marche et à ne pas planter lamentablement (kdevelop, c'est de toi que je parle). Les meilleurs IDE C++ sont donc en Java...

Pour le gestionnaire de projet, c'est bien simple, ça n'existe pas. Je n'ai trouvé aucun outil qui semble seulement s'approcher du centième de ce que fait un maven hors de la boite. Il faut donc utiliser une sorte script de compilation et gérer ses dépendances à la main comme à la préhistoire de l'informatique. J'ai pris cmake, car tout le monde semble l'utiliser de nos jours, mais mes yeux saignent en voyant le genre d'horreurs qu'il faut écrire:

```c++
    if (${CMAKE_CXX_COMPILER_ID} STREQUAL "GNU" OR ${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
       set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -pedantic -std=c++11")
       set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS} -g")
       set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS} -O2")
    endif()
```

J'ai aussi regardé comment faire de la cross compilation, créer des packages ou des binaires portables: là aussi il n'y a rien de bien au point, juste des tutoriaux ou des recommandations au détour d'un blog ou d'un forum.

# Le vent du changement c'est maintenant

Une fois passée la douloureuse mise en place du projet, j'ai quand même été content de voir que C++ a bien changé. Fini les functors, vive les lambdas! Enfin vous voilà, smart pointers dans la STL! Oh et boost qui s'est bonifié avec le temps!

Je peux enfin écrire sans honte de belles lignes de code comme celles ci-dessous sans recourir à 150000 lignes de templates, de macros et de libs externes:

```c++
    boost::property_tree::read_json(path, pt_nanim);
    boost::filesystem::path parent_path = boost::filesystem::path(path).parent_path();
    auto collec = std::make_shared<nanim::collection>();
    for (auto& pt_animation : pt_nanim.get_child("animations")) {
        auto anim = std::make_shared<nanim::animation>();
    for (auto& pt_frame_child : pt_animation.second.get_child("frames")) {
        auto pt_frame = pt_frame_child.second;
        ...
```

Si le langage a bien évolué sur des points qui me faisaient mordre mon clavier tous les jours à l'époque, je constate qu'il n'a pas bougé d'un poil sur des sujets fondamentaux:

- l'absence de système de modules: des entêtes avec #ifndef ... #endf en 2014, c'est très moche.
- j'ai une [dizaine de classes](todo://retrouverlecode) et la compilation se fait déjà longuette.
- toujours pas de stacktrace lors d'une exception.

# Si tu reviens, je compile tout

Ah Nal! Tu l'auras compris, retrouver une ancienne amour, c'est souvent se rendre compte que tout a changé pour que rien ne change.

A ton avis, dois-je continuer cette aventure risquée ou revenir à la Raison?
