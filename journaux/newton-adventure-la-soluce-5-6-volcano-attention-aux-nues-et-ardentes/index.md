URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-5-6-volcano-attention-aux-nues-et-ardentes
Title:   Newton Adventure la soluce 5/6: Volcano, attention aux nues et ardentes.
Authors: devnewton
Date:    2012-09-11T23:01:29+02:00
License: CC By-SA
Tags:    newton_adventure, jeux_linux et jeu_vidéo
Score:   8


Bonjour Nal,

Ce nourjal est le cinquième d'une série de six épisodes proposant la soluce en vidéo de [Newton Adventure](https://play.devnewton.fr).

![Volcano](http://tof.canardpc.com/view/0cefc96a-bb96-4b39-a261-6c6ca8ae7a50.jpg)

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Les vidéos
==========

Les vidéos de la quête Volcano, sont téléchargeables:

* en http sur [fiat tux](http://www.fiat-tux.fr/files/newton/).
* via bittorent sur [nitrotoxine](http://nitrotoxine.free.fr/www/libre/Newton_Adventure/2012-09-11-Newton_Adventure-Soluce_5_6.torrent)
* en http sur mon petit [serveur](todo://retrouverlavideo) autohébergé.

(Un grand merci à Nonas et Luc pour leur précieuse aide et précieuse leur bande passante!)

Dernière quête créé pour le jeu, Volcano est plus orienté réflexion. Un système de téléporteurs permet de découper les niveaux en plusieurs salles avec chacune des mécanismes à activer, des clefs à déplacer, des plateformes à débloquer...

Par exemple, certains blocs affichent des numéros pendant quelques secondes lorsque Newton les touche. Si on affiche tous les blocs de même numéro en même temps, des plates-formes qui bloquent la progression disparaissent.

La résolution de ces petites énigmes demandent un peu d'observation, mais aussi d'habileté, car il faut aller assez vite et user du changement de gravité pour atteindre les blocs à temps.

gimp my newton
==============

Feust travaille toujours sur la refonte des graphismes, voici un exemple de ce  à quoi pourrait bientôt ressembler le jeu&nbsp;:

![New background](http://tof.canardpc.com/view/f3c86449-e739-4c5e-a0dc-4488d7ad4182.jpg)

Notre Coin< préféré ayant décidé de se consacrer à d'autres projets, je me suis remis au pixel art&nbsp;:

![City hunter](http://opengameart.org/sites/default/files/styles/medium/public/ville.png)
![C'est de la bombe](http://opengameart.org/sites/default/files/styles/medium/public/bomb_about_to_explode0.png)
![Ainsi pont pont pont](http://opengameart.org/sites/default/files/styles/medium/public/bridge_sample_0.png)

Afin de participer à _la Cause_, j'ai décidé de mettre dès que possible mes créations sur [opengameart](http://opengameart.org/users/devnewton).

Amis du soir, bonsoir
=====================

En attendant le dernier nourjal, jouez bien à Newton Adventure ou regardez l'obligatoire [nimage](http://livvylove.com/files/images/jasmine-aladdin.preview.jpg) de fin toujours esthétique et non sexiste.
