URL:     https://linuxfr.org/users/devnewton/journaux/journal-social-agenda
Title:   Journal social agenda
Authors: devnewton
Date:    2017-09-10T15:37:49+02:00
License: CC By-SA
Tags:    social et travail
Score:   5


Bonjour Nal,

Je t'écris pour te rappeler les prochaines dates importantes concernant le monde du travail qui risque de bouger avec les ordonnances prévues par le gouvernement.

Côté contre:

- le [12 septembre](http://www.cgt.fr/Actions-et-mobilisations-le-12-septembre.html): les syndicats pro travailleurs ont appelé à une journée d'actions.
- le [23 septembre](https://marche23septembre.fr/): les partis de gauche proposent une marche.

Côté pour:

- le [10 septembre](https://en-marche.fr/evenements/afd03349-23bd-46c0-94cf-193976d2e436/2017-09-10-1er-running-den-marche-de-la-rentree): la République En Marche propose un running (une course à pied je suppose).
