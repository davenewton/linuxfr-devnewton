URL:     https://linuxfr.org/users/devnewton/journaux/la-fuite-de-conan-le-cimmerien-un-autre-point-de-vue
Title:   La fuite de Conan le Cimmérien: un autre point de vue
Authors: devnewton
Date:    2019-09-18T20:01:30+02:00
License: CC By-SA
Tags:    conan, barbare et humour_pas_drôle
Score:   12


Ah Nal,

Si tu suis les rumeurs sociales de ces derniers jours, tu n'es pas sans savoir que le célèbre barbare est victime d'une campagne de diffamation inédite dans l'âge hyborien.

Pourtant les derniers témoignages semblent le disculper dans l'affaire de la Vallée des femmes perdues: bien qu'ayant initialement accepté de libérer une jeune esclave blonde en échange de ses faveurs, il aurait finalement réalisé que, bien ce genre d'échange soit légal en Zingara, cela pouvait être considéré comme un abus de sa part.

Certains mettent aussi en doute la moralité de la jeune femme qui aurait déclaré _mieux vaut se donner à un barbare blanc plutôt qu’à un puissant homme noir_.

Le scénario le plus plausible est donc que le cimmérien n'a pas commis d'assaut sexuel.

Il semble par contre que l'assaut à la hache sur la tribu esclavagiste fut par contre bien réel et bien sanglant.
