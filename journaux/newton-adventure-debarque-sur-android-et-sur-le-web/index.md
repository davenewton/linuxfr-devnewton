URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-debarque-sur-android-et-sur-le-web
Title:   Newton Adventure débarque sur Android et sur le web 
Authors: devnewton
Date:    2014-01-16T11:20:21+01:00
License: CC By-SA
Tags:    newton_adventure, jnuit, configuration, padpourri, android, jeu et ned_et_les_maki
Score:   30


Bonjour Nal,

J'ai profité des vacances de noël pour ~~manger trois fois mon poids par jour~~ faire une implémentation du moteur de [Newton Adventure](https://play.devnewton.fr) avec l'API [PlayN](https://code.google.com/p/playn/). Celle-ci permets à partir d'une seule base de code Java de générer des applications desktop, android, ios et html5.

[Newton Adventure](https://play.devnewton.fr) est donc maintenant jouable via un [navigateur web](https://play.devnewton.fr/) et sur Android.

Tout ceci reste assez expérimental:

- je n'ai pas pu compiler de version pour iPhone, car il faut posséder un Mac pour cela...
- je n'ai aussi pu faire qu'un seul essai sur Android, mon S4 de test ayant été assassiné quelques heures plus tard (Est-ce un hasard? Je ne crois pas).
- la version web utilise `<canvas>`. PlayN propose aussi un backend webgl, mais je n'ai pas réussi à le faire fonctionner.
- pour aller vite, je n'ai pas porter le menu des options.

Pour ce dernier, je développe un projet nommé [jnuit](https://gitlab.com/davenewton/jnuit). Il s'agit d'une bibliothèque pour faire des GUI pour les jeux: elle propose des composants très simples (bouton, case à cocher, select) et d'autres spécialisés (réglages de la résolution, du son, des contrôles) manipulables en utilisant une souris, un clavier ou une manette de jeu. Le but est de proposer une API simple pour ajouter à un jeu tout ce qu'un joueur PC est en droit d'attendre d'un jeu moderne en terme de configuration.

Jnuit incube actuellement au sein du jeu [Ned et les maki](https://play.devnewton.fr/prototypes.html) et j'espère pouvoir sortir une version autonome dans l'année.

Si j'ai le temps, je ferais aussi une version C++...

![Newton Adventure sur Android](http://tof.canardpc.com/view/5b645108-b03a-4fe3-b519-10b4155eda11.jpg)

A bientôt, Nal!

