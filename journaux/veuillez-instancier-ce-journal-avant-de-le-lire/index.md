URL:     https://linuxfr.org/users/devnewton/journaux/veuillez-instancier-ce-journal-avant-de-le-lire
Title:   Veuillez instancier ce journal avant de le lire
Authors: devnewton
Date:    2014-10-09T17:47:55+02:00
License: CC By-SA
Tags:    c++, performance, java, template, benchmark, kiss et jeu
Score:   15


    /* attention ce journal est très légèrement technique, il ne suit pas la ligne éditoriale de linuxfr, vous n'y trouverez donc ni recette de cuisine, ni histoire de motards */
    
class journal < typename... Users > {

Bonjour Nal!

Si tu as lu mon précédent journal, tu sais que je me remets à jour en C++ en écrivant un petit prototype de jeu afin d'explorer ou de redécouvrir certaines parties de l'univers de cette plateforme de développement en kit.

Cette semaine, je me suis intéressé aux variadic templates (_patrons en nombre variable_ en français?) et au policy based design (_dessein basé sur la police_?) pour implémenter un petit entity system (_système à entités_ ça c'est facile!): bourrines.

Si ce _bourrin entity system_ n'a pas pour but de faire de la concurrence à des [bibliothèques plus sérieuses](https://github.com/jube/libes), il s'inspire fortement de la référence java dans le domaine, [artemis](https://github.com/junkdog/artemis-odb) tout en usant de la possibilité qu'offre C++ de bien gérer la mémoire.

Pour ceux qui ont raté les journaux de rewind<, un entity system est une architecture très à la mode dans les jeux vidéos: il s'agit d'une variante du role pattern combiné avec des principes du data oriented programming. Pour simplifier et éviter de massacrer la langue de Jacques Toubon, un système à entité dans un jeu peu se résumer à:

- presque tout élément du jeu (décors, avatars, pnjs, objets, bonus, tirs, plateformes...) est une entité.
- une entité peut se voir attribuer ou retirer des composants.
- un composant est un ensemble de données simples (PDO ie plain data object).
- le moteur du jeu est constitués de systèmes.
- un système est un ensemble d'algorithmes qui peuvent créer et détruire des entités ainsi que lire, écrire et transformer leurs composants.

Dans ma version de cette architecture, j'ai fait en sorte:

- de pouvoir changer facilement de layout mémoire pour les entités et les composants via des [stores](todo://retrouverlecode/devnewton/superpaflaballe/tree/master/src/bourrines/stores): c'est l'application du policy based design.
- de générer ces layouts à la compilation sans avoir à écrire de code intrusif dans les composants: c'est là qu'intervient l'usage des variadic templates, par exemple au lieu de créer une struct à la main avec un champ par composant, j'ai fait une classe component_struct qui hérite récursivement d'elle même en ajoutant un champ correspondant à un template de sa liste.

Si tu es un peu perdu, le mieux est regarder le [benchmark](todo://todoretrouverlecode/blob/master/src/bourrines_benchmark.cpp) que j'ai écrit pour comparer les performances des deux layouts en mémoire:

- je crée, fait rebondir et détruis un grand nombre de sprites sur l'écran.
- chaque sprite est une entité.
- chaque entité a des composants pour la position, l'image à afficher, sa durée de vie...
- un _hera_system_ donne la vie aux sprites.
- un _hades_system_ les tue au bout d'un moment.
- un _hades_system_ les tue au bout d'un moment.
- un _move_system_ les déplacent.
- un _render_system_ les affiche.

Chez moi, le layout _array of struct_ est environ 2% plus rapide, mais je n'ai pas encore creusé et profilé le sujet pour savoir où sont vraiment les goulots d'étranglements.


[Sources](todo://retrouverlecodedesuperpaflaballe/)

};

class journal< U, Users... > : public journal< Users... > {

Bonjour à toi aussi U! Que penses-tu de mon code? Comment ferais-tu pour l'améliorer?

};

![benchmark](benchmark.jpg)
