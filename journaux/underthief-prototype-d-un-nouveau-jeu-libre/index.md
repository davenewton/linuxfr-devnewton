URL:     https://linuxfr.org/users/devnewton/journaux/underthief-prototype-d-un-nouveau-jeu-libre
Title:   underthief: prototype d'un nouveau jeu libre
Authors: devnewton
Date:    2018-03-04T12:41:35+01:00
License: CC By-SA
Tags:    typescript, brunchio, phaserio, happosai, jeu et jeu_libre
Score:   23


Bonjour Nal,

Après Newton Adventure et Ned et les maki, je me lance dans le développement de nouveaux jeux libres.

Ayant plusieurs projets en tête, j'ai décidé de faire des prototypes pour tester mes idées de gameplay. Le premier était [shmuprpg](https://linuxfr.org/users/devnewton/journaux/shmuprpg-prototype-d-un-nouveau-jeu-libre), voici le deuxième: [underthief
](https://play.devnewton.fr/prototypes.html)

Il s'agit de l'adaptation d'un jeu populaire dans les lycées, connu en France sous le nom de _course au slip_: deux équipes placent leurs sous vêtements au centre d'un terrain avec un camp de chaque côté, puis chaque équipe doit capturer les sous vêtements de l'équipe adverse.

Dans ma version, on joue à deux filles contre deux garçons, au clavier ou à la manette, de préférence à 4 joueurs ou contre une IA basique. J'ai aussi ajouté la possibilité de donner des coups de maillets à ses adversaires ainsi qu'une course folle (dash).

![jeu](screenshot.png)

Sous licences libres ( MIT pour le code et CC-BY-SA pour les médias, le jeu est une application web écrite avec Typescript, brunch and Phaser (https://phaser.io).

Underthief et mes autres jeux sont tous jouables en ligne sur [play.devnewton.fr](https://play.devnewton.fr).
