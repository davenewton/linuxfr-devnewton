URL:     https://linuxfr.org/users/devnewton/journaux/replopbot-un-threeway-en-chameau-ca-vous-botte
Title:   Replopbot: un threeway en chameau, ça vous botte?
Authors: devnewton
Date:    2014-08-29T20:25:42+02:00
License: CC By-SA
Tags:    irc, xmpp, tribune, java, camel et bot
Score:   17


Ah Nal,

Il est bien difficile de communiquer de nos jours. Si nous vivons dans un monde connecté, tout le monde n'est pas connecté de la même façon. Les vieux geeks restent accrochés à IRC, les amoureux des standards que personne n'utilisent ne jurent que par XMPP et les moules restent... dans la moulosphère, un univers de messagerie étrange où il existe plus de logiciels que d'utilisateurs.

Devant me former à [Camel](https://camel.apache.org/), un cadriciel destiné à _empowers you to define routing and mediation rules in a variety of domain-specific languages_ ( ce qui ne veut rien dire à part que l'on va brancher des trucs avec des machins), j'ai saisi l'occasion pour m'attaquer à ce problème et faire un prototype d'un bot destiné à synchroniser:

- une tribune
- un salon XMPP
- un chan IRC

Tu peux te connecter à l'un des trois et poster un message, Replopbot, c'est son petit nom, va répliquer les messages sur les deux autres.

Ça ne casse pas trois plumes à un canard, mais il est intéressant de voir ce qu'on peut faire avec un [petit script java utilisant Camel](todo://retrouverlessourcesdereplopbot).
