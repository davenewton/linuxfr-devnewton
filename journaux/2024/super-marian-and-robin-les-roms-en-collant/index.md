URL:     https://linuxfr.org/users/devnewton/journaux/super-marian-and-robin-les-roms-en-collant
Title:   Super Marian and Robin: les roms en collant
Authors: devnewton 🍺
Date:    2024-04-03T15:18:26+02:00
License: CC By-SA
Tags:    retrogaming, jeu_vidéo, jeu_libre et snes
Score:   0


Bonjour Nal,

Je t'écris pour te proposer de tester mon nouveau jeu: [Super Marian and Robin](https://play.devnewton.fr/).

![Ecran titre](smr01.png)

Il s'agit d'un jeu de plateformes pour un ou deux joueurs sur le thème du célèbre [yeoman](https://www.radiofrance.fr/franceculture/robin-des-bois-heros-de-la-lutte-des-classes-6341322) en collant.

![Ecran de sélection](smr02.png)

Il est composé de trois scènes:

- un tournoi où il faut toucher le plus cibles possibles en un temps limité ;
- un battle royale dans la forêt de Sherwood ;
- un vol de riches pour donner aux pauvres (sauf les frais) dans le château du Roi Jean (pour l'instant vu qu'il n'a pas de terres et moi pas de temps, cette scène n'est pas implémentée).

![Tournoi](smr03.png)

Je parle de scènes et non de niveaux, car mes inspirations pour ce jeu viennent du cinéma :

- un peu du célèbre [Les Aventures de Robin des bois](https://fr.wikipedia.org/wiki/Les_Aventures_de_Robin_des_Bois_(film,_1938)) pour la palette de 15 couleurs ;
- pas du tout de [Robin and Marian](https://en.wikipedia.org/wiki/Robin_and_Marian) malgré le titre ;
- énormément des chefs d'oeuvre de Mel Brooks, [When Things Were Rotten](https://fr.wikipedia.org/wiki/When_Things_Were_Rotten) et [Robin Hood: Men in Tights](https://en.wikipedia.org/wiki/Robin_Hood:_Men_in_Tights). **Petit jeu pour les moules observatrices : essayez de trouver toutes les références dans ce journal, dans le jeu et dans son code source.**

![Forêt](smr04.png)

Pour y jouer, il vous faudra une [Super Nintendo](https://fr.wikipedia.org/wiki/Super_Nintendo) ou un émulateur de cette console, car après mes dépêches sur le retrodev/retrogaming, j'ai eu envie de m'y mettre avec le fabuleux SDK [PVSnesLib](https://linuxfr.org/news/entretien-avec-alekmaul-a-propos-de-pvsneslib).

La ROM est téléchargeable [ici](https://gitlab.com/davenewton/supermarianandrobin/-/releases/0.1/downloads/binaries/supermarianandrobin.sfc).

Le code source sous WTFPL est [là](https://gitlab.com/davenewton/supermarianandrobin).

Les tuiles et les sprites sous CC-BY-SA sont sur [opengameart](https://opengameart.org/content/marian-and-robin).

++Nal.

PS : si vous avez quelques talents retromusicaux, je cherche de l'aide pour ajouter sons et musiques à ce jeu.

