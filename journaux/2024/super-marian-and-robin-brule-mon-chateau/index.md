URL:     https://linuxfr.org/users/devnewton/journaux/super-marian-and-robin-brule-mon-chateau-deverse-ton-huile-bouillante
Title:   Super Marian and Robin: brûle mon château, déverse ton huile bouillante
Authors: devnewton 🍺
Date:    2024-05-11T23:00:36+02:00
License: CC By-SA
Tags:    retrogaming, jeu_vidéo, jeu_libre et snes
Score:   0


Bonjour Nal,

Je continue mon [travail](https://linuxfr.org/users/devnewton/journaux/super-marian-and-robin-les-roms-en-collant) sur mon nouveau jeu pour la console Super Nintendo: [Super Marian and Robin](https://play.devnewton.fr/).

![chateau](smr00.png)

## ♪ Ah son beau château, prends sa tire-reli-relire ♪

J'ai ajouté une scène dramatique : Marian et Robin, deux malfaiteurs cathogauchistes, vont s'associer pour commettre le plus terrible des crimes au moyen âge à savoir dérober la fortune d'un noble qui en a pourtant hérité à la sueur de sa couronne.

Nos deux criminels devront éviter des pièges et combattre le Roi Jean pour y parvenir.

Pour l'instant il y a peu de salles et cette aventure est assez courte, mais pour un vol c'est la taille de la bourse qui compte.

![chateau](smr01.png)

La ROM est téléchargeable [ici](https://gitlab.com/davenewton/supermarianandrobin/-/releases/0.4/downloads/binaries/supermarianandrobin.sfc).

Le code source sous WTFPL est [là](https://gitlab.com/davenewton/supermarianandrobin).

Les tuiles et les sprites sous CC-BY-SA sont sur [opengameart](https://opengameart.org/content/marian-and-robin).

![chateau](smr02.png)

## La track et l'épée

J'ai ajouté quelques musiques et sons sans grand intérêt pour l'instant, car je cherche toujours de l'aide pour sonoriser ce jeu correctement.

Cela demande de savoir composer de la musique avec un tracker, j'ai réussi à le faire modestement avec OpenMPT, mais ce n'est pas du tout mon royal domaine :-(

![chateau](smr03.png)
