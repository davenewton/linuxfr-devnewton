URL:     https://linuxfr.org/users/devnewton/journaux/delta-chat-1-42-le-chiffrement-de-bout-en-bout-plus-simple-et-plus-securise
Title:   Delta Chat 1.42 : le chiffrement de bout en bout plus simple et plus sécurisé
Authors: devnewton 🍺
Date:    2024-02-07T16:36:44+01:00
License: CC By-SA
Tags:    deltachat et messagerie_instantanée
Score:   16


Bonjour Nal,

Je t'écris pour te signaler la sortie de [Delta Chat](https://delta.chat/en/2023-11-23-jumbo-42), une application de messagerie instantanée libre qui se base sur les protocoles et l'infrastructure mail existante.

Jusqu'ici le chiffrement de bout en bout n'était possible que via [autocrypt](https://en.wikipedia.org/wiki/Autocrypt) qui est très pratique (l'utilisateur n'a rien à faire), mais vulnérable aux attaques de l'homme au milieu (MITM) : c'était un échange de clefs opportunistes au premier message envoyé.

Pour éviter le MITM, Delta Chat gère maintenant [SecureJoin](https://securejoin.readthedocs.io/en/latest/) qui consiste à permettre aux utilisateurs de s'inviter avec à un qrcode à scanner par exemple lors d'une rencontre physique autour d'une bière ou d'un joint (d'où le nom?) ou en visioconférence (à condition que celle-ci soit aussi sécurisée) par exemple.

Et toi, Nal? Comment communiques-tu de façon sécurisé ?
