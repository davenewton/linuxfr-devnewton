URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-1-18-le-chien-java-et-la-caravane-patche
Title:   Newton Adventure 1.18 : le chien java et la caravane patche
Authors: devnewton 🍺
Date:    2024-03-15T21:50:32+01:00
License: CC By-SA
Tags:    java, newton_adventure et lwjgl
Score:   16


Bonjour Nal,

Je t'écris pour signaler la sortie d'une release mineure de mon jeu [Newton Adventure](https://play.devnewton.fr/).

Mineure non pas parce que le jeu a [~14 ans](https://linuxfr.org/news/un-nouveau-jeu-libre-newton-adventure), mais parce qu'il s'agit d'une version de maintenance : j'ai mis à jour le [JDK](https://openjdk.org/) et quelques bibliothèques, notamment [LWJGL](https://www.lwjgl.org/) ce qui devrait améliorer la compatibilité du jeu avec les systèmes d'exploitation à jour.

Si votre système d'exploitation n'est pas à jour, faites comme moi: [un patch management tous les matins garde le RSSI au loin.](https://en.wikipedia.org/wiki/An_apple_a_day_keeps_the_doctor_away).

Et toi Nal, tes applis sont-elles à jour?
