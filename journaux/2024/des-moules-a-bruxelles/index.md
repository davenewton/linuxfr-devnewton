URL:     https://linuxfr.org/users/devnewton/journaux/des-moules-a-bruxelles
Title:   Des moules à Bruxelles
Authors: devnewton 🍺
Date:    2024-01-29T15:05:51+01:00
License: CC By-SA
Tags:    fosdem
Score:   16


Bonjour Nal,

Je serais au [FOSDEM](https://fosdem.org/2024/) cette fin de semaine et je me demandais si d'autres moules< s'y rendent aussi pour échanger sur Linux, les logiciels libres, le retrogaming, les recettes de cuisine, les flims Dune, l'avenir de Charles Bronson et autres sujets majeurs de notre temps autour d'une bière et/ou d'un repas les samedi et dimanche soir.

Vous pouvez me contacter par [mail](https://play.devnewton.fr/about.html) en laissant un message après le bip.
