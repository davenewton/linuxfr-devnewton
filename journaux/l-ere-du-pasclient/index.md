URL:     https://linuxfr.org/users/devnewton/journaux/l-ere-du-pasclient
Title:   L'ère du pasclient?
Authors: devnewton
Date:    2013-07-19T10:20:07+02:00
License: CC By-SA
Tags:    
Score:   13


Ah Nal,

Alors que certains se lancent dans des ~~trolls~~ débats sans fin pour savoir si [l'avenir est aux clients légers, LOURDS, riches ou pauvres](https://linuxfr.org/users/bluebird/journaux/et-moi-qui-croyais-que-le-client-lourd-serait-gagnant), je me suis rendu compte que j'utilise souvent des logiciels d'une catégorie encore mal définie et peu connue du grand public: faute de mieux je les nomme "pasclient".

Qu'est-ce qu'un pasclient?
==========================

C'est un logiciel qui sert à produire, travailler ou transformer du contenu à partir de données brutes, textes, binaires, images...

Pourquoi utiliser un pasclient?
===============================

Ces logiciels permettent d'être plus productifs, car:

- les données brutes, surtout au format texte, se versionnent, se manipulent et s'échangent facilement.
- les lignes de commande des pasclient permettent l'automatisation des tâches répétitives.
- les imports/exports permettent souvent de travailler avec tout le monde: on n'impose pas son outil aux autres.

Quels pasclients utiliser après ce journal?
===========================================

Voici une petite liste des pasclients que j'utilise régulièrement:

- [Taskjuggler](http://www.taskjuggler.org/): un gestionnaire de projet pour performer plus qu'un consultant MS Project sous acide.
- [Graphviz](http://www.graphviz.org/): un visualiseur de graphes pour ne plus faire des schémas à la main comme au moyen âge.
- [pandoc](http://johnmacfarlane.net/pandoc/) et https://code.google.com/p/wkhtmltopdf/ pour créer de la documentation et l'importer/exporter vers différents wiki, traitements de texte et formats papiers.
- [ImageMagick](http://www.imagemagick.org/) pour transformer des images.

Il y en a beaucoup d'autres, dont je n'ai pas encore eu besoin, mais qui me semble intéressant:

- [latex](http://www.latex-project.org/): le célèbre compositeur de document.
- [povray](http://www.povray.org/): un lanceur de rayons.
- [TextUML](http://sourceforge.net/apps/mediawiki/textuml/index.php?title=TextUML_Toolkit): un logiciel de modélisation.
- [gmic](http://sourceforge.net/projects/gmic/): un manipulateur d'images.

