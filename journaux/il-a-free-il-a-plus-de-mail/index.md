URL:     https://linuxfr.org/users/devnewton/journaux/il-a-free-il-a-plus-de-mail
Title:   Il a Free, il a plus de mail
Authors: devnewton
Date:    2013-12-28T11:11:44+01:00
License: CC By-SA
Tags:    noël, free et auto-hébergement
Score:   -2


Bonjour Nal,

Je t'écris via linuxfr, car je sais que pour ne pas subir les aléas de l'autohébergement, tu utilises une boite mail chez Free, la société de Xavier Niel, inventeur de l'internet français, défenseur de la liberté et du pouvoir d'achat.

Malheureusement je n'arrive plus à te joindre depuis une semaine, il semble que ce soit du à une grande panne sur les serveurs de la société, notamment pour les malchanceux qui disposent d'un accès via le webmail Zimbra:

http://www.universfreebox.com/article/23487/Free-Des-problemes-d-acces-avec-Zimbra

J'espère que tu as quand même passé de bonnes fêtes et que tu pourras souhaiter joyeux noël à ta famille éloignée avant 2014!
