URL:     https://linuxfr.org/users/devnewton/journaux/je-dis-coi-quoi-dans-ton-chat
Title:   Je dis COI! Quoi? Dans ton chat!
Authors: devnewton
Date:    2019-04-02T14:27:38+02:00
License: CC By-SA
Tags:    delta, chat, messagerie, xkcd927, courriel et fosdem
Score:   13


Bonjour Nal,

Je t'avais parlé de [Delta Chat](https://linuxfr.org/users/devnewton/journaux/delta-chat-est-pret-pour-le-bureau) il y a quelques temps et j'ai découvert que d'autres personnes s'y mettent:

- une tentative de standardiser le protocole Chat Over IMAP (COI quoi)  https://www.coi-dev.org/ avec de nouveaux clients [ox-talk](https://github.com/open-xchange/ox-talk) et https://www.spikenow.com/
- Thunderbird semble intéressé: https://blog.mozilla.org/thunderbird/2019/03/fosdem-2019-and-deltachat/
- un plugin pour libpurple: https://gitlab.com/lupine/purple-plugin-delta
- l'application de chat du futur Librem: https://source.puri.sm/Librem5/chatty/issues/96#

Petit à petit, le chat par dessus courriel fait son chemin...
