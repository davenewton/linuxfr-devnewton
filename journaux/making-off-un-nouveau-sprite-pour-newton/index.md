URL:     https://linuxfr.org/users/devnewton/journaux/making-off-un-nouveau-sprite-pour-newton
Title:   Making off: un nouveau sprite pour Newton
Authors: devnewton
Date:    2012-12-27T01:29:50+01:00
License: CC By-SA
Tags:    newton_adventure, graphisme, jeux_linux, sprite, animation et jeu_vidéo
Score:   42


Bonjour Nal,

Si tu suis régulièrement mes aventures sur [linuxfr](http://linuxfr.org/users/devnewton), tu sais que le jeu que je développe, [Newton Adventure](https://play.devnewton.fr), utilise un _sprite_ sans rapport avec le thème du jeu.

![Le sprite actuel](sprite_actuel.png)

J'y suis attaché, car il ressemble à Rick Dangerous, un jeu amiga qui est l'une de mes sources d'inspiration.

Toutefois, j'ai décidé de m'attaquer à la réalisation d'un personnage ressemblant à Isaac Newton à l'aide des logiciels libres suivants :

- [gimp](http://www.gimp.org/) pour le dessin et la retouche d'image.
- [nanim](https://framagit.org/very-old-devnewton-projects/nanim) pour la création de l'animation.
- [imagemagick](http://www.imagemagick.org/) pour la création du gif animé, afin de pouvoir te montrer le résultat à la fin de ce journal.

Voici la procédure que j'ai suivie.

Dessin
======

Première étape, faire un dessin grossier au crayon :

![crayon](crayon.jpg)

Deuxième étape, scanner et utiliser des filtres (Dessin au crayon, nettoyer...) pour avoir un contour net :

![contour net](contour.jpg)

C'est aussi le moment de se maudire pour ne pas être allé acheter une feuille sans petit carreau au supermarché du coin.

Troisième étape, réduire la résolution et faire un coloriage rapide :

![couleurs](couleurs.png)

Pixel art
=========

Quatrième étape, réduire encore la résolution et essayer de faire juste ressortir les détails importants :

![details](details.png)

Cette étape est assez difficile, la faible résolution (une contrainte de cohérence avec les autres _sprites_ du jeu), oblige à vraiment trouver des formes et des couleurs particulières. Pour cela, il est indispensable de bien configurer gimp. J'ouvre deux vues sur la même image, l'une avec un zoom important, l'autre en taille réelle et qui n'affiche pas les sélections ou bords de cadre. Je mets aussi un calque d'une couleur que je n'utilise pas pour faire bien ressortir les contours.

![gimp conf](gimp.png)

Animation
=========

Enfin, il faut recommencer pour toutes les positions clefs de l'animation, n'ayant pas la possibilité d'engager une armée d'intervallistes, je me suis contenté de 5 _frames_ :

![nanimation](anim.png)

Avec le sdk de nanim, la commande suivante permet de créer l'animation :

    nanimenc -author devewton -license "CC-BY-SA 3.0" -d 100 -a stay -f newton_03.png -a walk -f newton_03.png -f newton_04.png -f newton_05.png -f newton_04.png -f newton_03.png -f newton_02.png -f newton_01.png -o hero.nanim

Pour obtenir le gif, il faut utiliser la commande convert d'imagemagick :

    convert -delay 10 -dispose Background newton_03.png newton_03.png newton_04.png newton_05.png newton_04.png newton_03.png newton_02.png newton_01.png hero.gif

Et voilà le résultat :

![gif](anim.gif)

Le mot de la fin
================

Avec ce nouveau _sprite_ principal, Newton Adventure s'approche de plus en plus de sa version finale. Depuis quelques mois, je fais un travail de finition afin de pouvoir en faire un jeu vendable.

Mais ça, c'est une autre histoire...

En attendant, il me reste à décider ce que je vais faire de l'ancien personnage.
