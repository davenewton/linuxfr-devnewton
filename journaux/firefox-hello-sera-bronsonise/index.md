URL:     https://linuxfr.org/users/devnewton/journaux/firefox-hello-sera-bronsonise
Title:   Firefox hello sera bronsonisé
Authors: devnewton
Date:    2016-09-17T10:36:34+02:00
License: CC By-SA
Tags:    mozillafirefoxhellobronson et firefox
Score:   13


Ah Nal, c'est terrible!

Je t'écris pour te dire que je ne pourrais plus t'appeler, ni voir ton doux visage sur mon écran, car Mozilla a décidé d'abandonner son projet Firefox Hello, qui permettait un échange simple & efficace via webcam entre plusieurs utilisateurs:

https://support.mozilla.org/en-US/kb/hello-status
