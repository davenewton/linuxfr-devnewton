URL:     https://linuxfr.org/users/devnewton/journaux/j-ai-lance-une-commande-dans-mon-terminal-decouvrez-l-incroyable-resultat
Title:   J'ai lancé une commande dans mon terminal, découvrez l'incroyable résultat
Authors: devnewton
Date:    2018-10-26T11:54:47+02:00
License: CC By-SA
Tags:    
Score:   -9


		$ cat /dev/urandom | base64 | head
		GiYS3pTw5k2iX18aExng4Hl7fTq3lwkOQ9OchbwfEko84rTbrueGtAOFAk/6U/CPdesDIAJdIq+D
		evpkhDh2HV08lDg0IHsIyNIdWRWa+grcKd4DedMZrX/PNOBygO4Cs8wT+3m6ylcDe2x4P+VTUmF/
		bjCGb+DITbIAOK7CmTYjSBQjp+8gYkvmjPQFyuvmGCgXhgn6e+M/6eqPMi45wRXrzM3mCiKxidn2
		RpJcFBecHrNn0n8ucFlRht3qw+/KK8bdShv5kHYbbfWH76jnvD8Wjon62RvyUumSlmm4hOuhU8h+
		0SD4nSuB5YatG3CY4Kcssk3FrVO9ev34ANjhA0y87y4RDq7Vo2UckKbnudbeoabAXCrRpOG4QDAl
		3bRGOkxVIf9xbaa3SrtF44Vy5L7FxrKAIwe6byJnvJD6CIxw/YMp09GcU/JWLaUMetEKm7rmsNaR
		utxEHb6pwlsFteA+2A49/eo8yCnt6TUyHuE4jsUtBmubv3iG0OkzSSBlySFPh5ygxnHt6DRVnPEh
		W+bT0iNorli2NKzgWyYmWq1m2wZnxwLG6U7IfFEYhauKwO8DW1ypSo6K31cEm5X4NjyEEjWTkf3g
		xadb1AZ+bgxRVS+mvmYC/Z308LVOnf9iSxxO3fhX1cZ4rG+y/StNP+IcS7e9LaMYlXotkkthdcrc
		eH0MR8xh6vacsQxE6tfiYhKIlFlliKwpHGFhohXGPeaUKM5hZ2RmbCyO+YI4fQfOTZ93sYyGkWQg


Les journaux c'était mieux à vent.
