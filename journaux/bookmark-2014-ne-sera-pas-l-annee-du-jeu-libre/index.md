URL:     https://linuxfr.org/users/devnewton/journaux/bookmark-2014-ne-sera-pas-l-annee-du-jeu-libre
Title:   [bookmark] 2014 ne sera pas l'année du jeu libre
Authors: devnewton
Date:    2014-01-02T12:51:13+01:00
License: CC By-SA
Tags:    librefrorg, prosélytisme et jeu
Score:   15


Bonjour Nal,

Je te recommande la lecture d'un article sur gamedev.net, un site très populaire chez les développeurs de jeux vidéo: [You Don't Need to Hide Your Source Code](http://www.gamedev.net/page/resources/_/business/business-and-law/you-dont-need-to-hide-your-source-code-r3503)

Il reprends les arguments classiques en faveur de l'ouverture du code et compare la situation du jeu du vidéo à celles de la musique ou du cinéma: des domaines dans lesquels l'opinion dominante est qu'il faut employer des moyens techniques et légaux pour priver les clients de la possibilité de partager et modifier les œuvres, avec diverses justifications économiques (manque à gagner) et morales (supériorité des artistes).

Étant donné que l'auteur se fait largement démonter dans les commentaires, je pense que malheureusement 2014 ne sera pas l'année du jeu libre :-(
