URL:     https://linuxfr.org/users/devnewton/journaux/elisabeth-ii-bronsonnisee-bien-fait
Title:   Élisabeth II bronsonnisée : bien fait ?
Authors: devnewton
Date:    2022-09-08T20:54:33+02:00
License: CC By-SA
Tags:    bronsonisation et anglais
Score:   -1


Bonjour Nal,

Je t'écris, car la reine de la perfide Albion vient d'être bronsonnisée.

Est-ce une raison de se réjouir?

On pourrait le penser vu ses [relations](https://www.lelibrepenseur.org/la-reine-elizabeth-ii-recoit-bill-gates-au-chateau-de-windsor/) et son rapport à notre [OS favori](https://www.zdnet.com/article/queen-elizabeth-banishes-linux/), mais il ne faut pas oublier qu'elle ne fut pas seulement reine, mais aussi une formidable actrice, notamment dans son meilleur flim :

[Y a-t-il un flic pour sauver la reine ?](https://fr.wikipedia.org/wiki/Y_a-t-il_un_flic_pour_sauver_la_reine_%3F)

Elle y joue son propre rôle au côté du grand [Leslie Nielsen](https://fr.wikipedia.org/wiki/Leslie_Nielsen) qu'on regrette sincèrement, lui.

![L'agent fait la farce](y_a_t_il_un_flic_pour_sauver_la_reine.jpg)
