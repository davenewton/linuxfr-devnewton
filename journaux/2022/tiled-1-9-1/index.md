URL:     https://linuxfr.org/users/devnewton/journaux/tiled-1-9-1
Title:   Tiled 1.9.1
Authors: devnewton
Date:    2022-08-18T20:41:36+02:00
License: CC By-SA
Tags:    tiled et jeu_vidéo
Score:   47


Ah Nal,

Je profite d'une pause durant mes vacances au pays des grenouilles pour te signaler la sortie de la nouvelle version de [Tiled](https://www.mapeditor.org/2022/08/11/tiled-1-9-1-released.html), l'éditeur de niveau que j'utilise pour mes [jeux](https://play.devnewton.fr/).

Bien que mineure, elle est bonne, car elle contient une contribution d'un certain Newton Dave qui permets d'utiliser des tiles de plusieurs tailles dans une même carte.

Et les tailles, ça compte, car il est souvent pratique d'avoir de grosses derrières et de petites devant comme le montre cet [exemple](https://github.com/mapeditor/tiled/tree/master/examples/forest): 

![tiled](tiled.png)
