URL:     https://linuxfr.org/users/devnewton/journaux/memento-git-avec-capuchine
Title:   Memento Git avec Capuchine
Authors: devnewton
Date:    2022-12-21T11:17:26+01:00
License: CC By-SA
Tags:    git, cheatsheet et capuchine
Score:   0

Bonjour Nal,

Je t'écris pour te proposer quelques dessins sous licence libre ( CC By-SA 4.0 ) qui servent à se rappeler des commandes les plus utiles du célèbre gestionnaire de version [Git](https://git-scm.com/) avec l'héroïne d'un de [mes jeux](https://play.devnewton.fr/).

# git init

Cette commande permets de créer un dépôt dans le dossier courant. Elle va ajouter un dossier _.git_ avec les fichiers du gestionnaire de version.

![git init](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_init.png)

# git clone

On peut aussi faire une copie d'un dépôt existant avec cette commande.

![git clone](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_clone.png)


# git add

Maintenant qu'on a un dépôt, on peut ajouter des fichiers avec cette commande. Attention l'ajout n'est qu'une "mise en place" (staging), les fichiers ne seront versionnés qu'une fois "consigné" (commit).

![git add](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_add.png)

# git diff

Lors que l'on fait des changements sur un fichier du dépôt, on peut voir les différences avec la version précédente avec cette commande.

Pour une vue plus globale, on préfèrera _git status_.

![git diff](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_diff.png)

# git commit

Cette commande enregistre les fichiers précédemment ajoutés dans le dépôt. Chaque _commit_ est associé à son auteur : pour cela il faudra configurer son nom avec _git config user.name_ et son mail _git config user.email_.

Les _commits_ sont visibles dans l'historique (log) via la commande _git log_.

![git commit](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_commit.png)

# git branch

Si on souhaite travailler sur une version parallèle, on peut utiliser cette commande pour créer une branche avant de faire des changements et de les _commiter_.

On pourra plus tard fusionner des branches avec _git merge_.

![git branch](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_branch.png)

# git push/pull

Pour récupérer les commits d'un autre dépôt, on utilise _git pull_.

Pour partager ses commits, on lance _git push_.

![git pull et git push](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_pull_push.png)

# git blame

Cette commande permets de savoir qui a fait un changement sur un fichier et à quelle ligne.

![git blame](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_blame.png)

# git help

Il existe beaucoup d'autres commandes git, pour les découvrir un bon point d'entrée est la commande _git help_.

![git help](https://gitlab.com/davenewton/linuxfr-devnewton/-/raw/main/journaux/memento-git-avec-capuchine/capuchine_git_help.png)

Et toi Nal, quelle est ta commande git préférée ?