URL:     https://linuxfr.org/users/devnewton/journaux/ces-langages-avec-lesquels-il-faut-tout-reecrire
Title:   Ces langages avec lesquels il faut tout réécrire
Authors: devnewton
Date:    2022-09-26T07:29:37+02:00
License: CC By-SA
Tags:    langage_de_programmation, zig, crystal et nim
Score:   27


Depuis quelques temps, on voit apparaître de nouveaux langages à tout faire : Go, Rust et Swift sont les plus connus, mais il en existe d'autres.

The V Programming Language
==========================

Sans pointeur nul, sans comportement non défini et pouvant s'apprendre en un week end, [V](https://vlang.io/) devrait devenir rapidement populaire chez les reptiliens.

Zig
===

Proche de Go, mais avec une gestion manuelle de la mémoire et une concurrence via async/await, ce drôle de [Zig](https://ziglang.org) peut aussi [compiler... du C](https://ziglang.org/learn/overview/#zig-is-also-a-c-compiler) !

Crystal
=======

Vous avez toujours rêvé d'un Ruby avec du typage statique? Non ? Heu et bien les auteurs de [Crystal](https://crystal-lang.org/) l'ont fait quand même.

Nim
===

Plus secret, [Nim](https://nim-lang.org/) ressemble lui à un Python avec du typage statique. Vous n'en aviez pas rêvé non plus?

Mais quel langage voyez-vous donc en songe la nuit?
