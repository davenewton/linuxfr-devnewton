URL:     https://linuxfr.org/users/devnewton/journaux/un-peu-de-medieval-comique-pour-les-fetes-de-2022
Title:   Un peu de médiéval comique pour les fêtes de 2022
Authors: devnewton
Date:    2022-12-28T12:15:36+01:00
License: CC By-SA
Tags:    médiéval-fantastique, comédie et série
Score:   0


Bonjour Nal,

Comme chaque année, je t’écris pour te suggérer une série du genre médiéval comique (ou comic fantasy ou encore word & sorcery) à voir en famille ou Han Solo pour les fêtes : [La Légende de Dick et Dom](https://fr.wikipedia.org/wiki/La_L%C3%A9gende_de_Dick_et_Dom).

## La légende de Dick et Dom

![The legend of Dick and Dom](legend_of_dick_and_dom_title.jpg)

Dis moi Nal, avec le budget d'une [production AB](https://fr.wikipedia.org/wiki/Liste_des_s%C3%A9ries_t%C3%A9l%C3%A9vis%C3%A9es_d%27AB_Productions), quel genre choisirais-tu de produire ? Une sitcom ou une telenovela ? Et bien, les producteurs de cette série ont choisi de faire de la high fantasy, genre toujours risqué même avec des moyens pharaoniques.

Heureusement l'argent ne fait pas tout et ils ont pris d'excellents acteurs. Ah non, en fait ce sont des comiques de seconde zone et des premiers rôles à l'écran.

Situé dans un royaume frappé par une pandémie, _La légende de Dick et Dom_ raconte la quête de deux princes pour refaire un antidote (après avoir laissé tomber la première version). Aidés par un magicien raté et une voleuse peu fiable, chaque épisode est une aventure pour trouver un nouvel ingrédient.

Les histoires ne tiennent pas debout, les blagues tombent à l'eau et c'est même souvent gênant.

Alors pourquoi regarder cette série? Et bien parce que si le ridicule ne tue pas (ou rarement), il est plaisant de voir le même acteur jouer le personnage secondaire dans chaque épisode, les comiques en faire des caisses pour sembler surpris par un gag téléphoné, les costumes improbables...

Bref c'est le même plaisir coupable que de rire du gros roux à qui l'on a demandé de jouer Superman à la kermesse de l'école et qui se prends les pieds dans sa cape.

Si toi aussi tu es pétris de schadenfreude, cette série est pour toi.

## A voir aussi en ce moment

Quelques autres séries peuvent être qualifiées de médiéval comique en ce moment:

- [Hélène et les dragons](https://fr.wikipedia.org/wiki/House_of_the_Dragon): je me suis endormi devant, il faut sans doute être américain et donc fan d'incestes avec belle famille pour en profiter pleinement ;
- [Les anneaux de pouvoir du seigneur des anneaux avec des anneaux dedans](https://fr.wikipedia.org/wiki/Le_Seigneur_des_Anneaux_:_Les_Anneaux_de_Pouvoir): ça partait bien avec des elfes métrosexuels et des hobbits gitans, mais là aussi on s'ennuie beaucoup ;
- Qui envoyer pour une quête dangereuse ? Des troupes d'élites,  des aventuriers aguerris et des éclaireurs sacrifiables? Un vieux qui tremble, une cuisinière et une princesse jamais sorti de son pays ? Vous découvrirez le choix fait dans la prometteuse série [Willow](https://fr.wikipedia.org/wiki/Willow_(s%C3%A9rie_t%C3%A9l%C3%A9vis%C3%A9e)), suite du célèbre flim dont personne ne se rappelle.


Et toi Nal, qu'as tu ouatché pour les fêtes ?
