URL:     https://linuxfr.org/users/devnewton/journaux/webkorisu-un-nouveau-jeu-libre-pour-une-pause-cafe-noisette
Title:   webkorisu : un nouveau jeu libre pour une pause café noisette
Authors: devnewton
Date:    2022-11-09T16:37:17+01:00
License: CC By-SA
Tags:    jeu_libre et jeu_vidéo
Score:   23


Ah Nal,

Je t'écris pour te présenter mon [nouveau jeu libre](https://gitlab.com/davenewton/webkorisu).

![webkorisu](webkorisu.jpg)

Pré en bullet
-------------

Mais comme nous sommes enfin en automne avec ses belles couleurs, commençons par un peu de poésie:

> Il ne se faut jamais moquer des casus,
Car qui peut s'assurer d'être toujours skillé?
Le sage Ésope dans ses bonus
Nous en donne un exemple de gameplay ; 

> Le Renard se moquait un jour de l'Écureuil
Qu'il voyait noober un hardcore platformer: 
Te voilà, disait-il, manette en deuil ;
Mordant ton pad en vain près du game over.

> Un chasseur ayant confondu,
Le train du renard avec un VTT ;
Déchargea dans son cul ;
Son mortel mousquet.

> Moralité: on ne joue pas en forêt pendant la saison de la chasse.

Le jeu
------

[Webkorisu](https://play.devnewton.fr/webkorisu/) est un jeu dont l'objectif est d'aider des écureuils à ramasser des noisettes afin de rester dodus quand la bise sera venue.

Il s'agit d'un jeu de plateforme éducatif et collaboratif.

Éducatif, car il est fait pour initier de jeunes enfants ou de vieux noobs au genre : on ne peut pas mourir, on ne peut pas perdre, juste prendre son temps pour apprendre à réussir les classiques du genre (plateformes fixes, mouvantes, traversables, intermittentes).

Collaboratif, car jusqu'à quatre joueurs peuvent ensemble prendre le contrôle d’écureuils. Pour cela il faudra brancher des manettes ( et se plaindre à Mozilla pour gérer correctement le [mapping des boutons](https://bugzilla.mozilla.org/show_bug.cgi?id=1643358) ), car les claviers ne gérant pas [beaucoup de touches simultanées](https://www.mechanical-keyboard.org/key-rollover-test/), j'ai préféré ne pas m'en remettre à ce seul périphérique.

![gameplay](gameplay.webp)

Et toi Nal, que t'inspires l'automne ?
