URL:     https://linuxfr.org/users/devnewton/journaux/opensara-la-bataille-du-rail
Title:   opensara: la bataille du rail
Authors: devnewton
Date:    2022-03-30T18:48:06+02:00
License: CC By-SA
Tags:    train, jeu_vidéo et jeu_libre
Score:   30


Bonjour Nal,

C'est avec entrain que je t'annonce un nouveau niveau pour mon jeu [opensara](https://play.devnewton.fr/opensara/).

Comme tu dérailles un peu, je te rappelle qu'il s'agit de petites aventures, à jouer pendant une pause café, un peu comme on lit une bande dessinée en une page à la fin d'un journal. Petites, mais pas de tout repos: Sara va être empoisonnée, faite prisonnière d'un vampire et forcée à combattre des monstres pour la République Populaire de Grande Asie.

Et maintenant Sara se retrouve sur le toit d'une locomotive pour affronter une redoutable hors la Loi dont l'arrière train sifflera plus que trois fois pour pouvoir s'envoler avant de se transformer en tornade et de tirer au fusil.

![train](opensara.png)

Les sources du projet sont sur [gitlab](https://gitlab.com/davenewton/opensara) toujours sous des licences libres.

Amuse toi bien et à plus dans le ouibus, Nal.
