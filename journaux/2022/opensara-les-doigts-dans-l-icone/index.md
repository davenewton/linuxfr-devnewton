URL:     https://linuxfr.org/users/devnewton/journaux/opensara-les-doigts-dans-l-icone
Title:   opensara : les doigts dans l'icône
Authors: devnewton
Date:    2022-01-15T00:51:54+01:00
License: CC By-SA
Tags:    opensara, jeu_vidéo, jeu_libre, écran_tactile, ordiphone, tablette et pwa
Score:   18


Bonjour Nal,

Je t'écris pour te faire part d'une histoire qui me donne envie de faire un gros doigt aux développeurs d'un certain brouteur. N'étant pas mapolinus, je vais plutôt expliquer le problème que j'ai rencontré.

Je travaille sur l'accessibilité de mon dernier jeu [opensara](https://play.devnewton.fr/opensara/): les jeunes générations n'étant vraiment pas doué avec l'informatique, mais plus à l'aise avec le digital, ils veulent tout faire avec leurs doigts boudinés et sales sur les écrans minuscules des smartphones tournant avec des OS privateurs. Bref moi de mon temps, c'était mieux avant, on avait une souris et un clavier. Le monde était pur, on avait l'avenir devant nous avec des voitures volantes et des répliquants...

Mais je dis verge: je travaille donc sur l'adaptation aux écrans tactiles de ce jeu en implémentant une manette virtuelle:

![virtual pad](virtualpad.png)

Et puis je me suis dit que j'allais ajouter un petit manifest.json pour faire d'opensara [une PWA (Progressive Web App) capable A2HS (Add to Home Screen)](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Add_to_home_screen#why_a2hs) pour que le joueur puisse ajouter une icône pour lancer le jeu avec un brouteur en plein écran sur son écran d'accueil.

En plein écran, grâce à la propriété `"display" : "fullscreen"`.

Et là ça coince: le jeu ne se lance pas en plein écran.

3h de debug plus tard, j'ai compris: Firefox pour Android n'applique le mode fullscreen que si l'application déclare une icône en 512x512.

Pourquoi? Sans doute un bout de code fait avec le cul, je vais éviter d'y mettre les doigts.

Ah Nal, quel est le dernier bug rigolo que tu as recontré?
