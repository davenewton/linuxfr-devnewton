URL:     https://linuxfr.org/users/devnewton/journaux/mono-3-2-est-sorti
Title:   Mono 3.2 est sorti
Authors: devnewton
Date:    2013-08-23T08:56:30+02:00
License: CC By-SA
Tags:    mono et tiling
Score:   19


Bonjour Nal,

[Mono](http://mono-project.com), l'implémentation présumé libre de la technologie Microsoft .NET, est sorti le mois dernier. Comme je n'ai pas vu de dépêche sur l'évènement, je me suis dit que tu serais intéressé par la lecture du journal des changements: http://www.mono-project.com/Release_Notes_Mono_3.2

Apparemment cette nouvelle version contient surtout des optimisations du ramasse miettes, le support sur iOS et le portage du cadriciel XNA abandonné par Microsoft il y a peu.

J'espère que ce mono nouveau me permettra d'utiliser [Instant Tiled](https://bitbucket.org/nemiz/instanttileed/), un logiciel de dessin pour les jeux à base de [tuiles](http://en.wikipedia.org/wiki/Tile_engine) dont il n'existe, à ma connaissance, aucun équivalent dans de bons langages.

![Instant Tiled](ite_import_bug.png)
