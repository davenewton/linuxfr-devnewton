URL:     https://linuxfr.org/users/devnewton/journaux/jb3-la-tribune-des-beaux-gosses
Title:   jb3, la tribune des beaux gosses
Authors: devnewton
Date:    2014-12-17T23:24:12+01:00
License: CC By-SA
Tags:    c2, coincoin, b3, tribune, chauve, chrisix et jb3
Score:   5


Ah Nal, je t'écris pour te parler d'un nouveau drame!

Gros mythe
==========

La malédiction des tribunes à encore frappée.

Tu ne le sais peut être pas, mais à l'aube des temps tribunaux, une moule dont le nom est aujourd'hui oublié a offensé les Dieux en créant le premier coincoin. Pour se venger, ils jetèrent sur leurs adorateurs et leur descendance une terrible malédiction:

> Vous errerez désormais dans un monde caché. Chaque moule devra un jour ou l'autre développer un c2 ou une b3 et jusqu'au royaume d'Hadès vos specs resteront ineptes.

Cela fait quelques années que j'y échappais, grâce à une ruse faisant passer un [client xmpp pour une tribune](https://framagit.org/very-old-devnewton-projects/muckl_tribune).

Malheureusement, les Dieux m'ont envoyé une obligation de veille techno et j'ai du accomplir mon Destin.

6 if
====

Le résultat est une monstruosité du nom de [jb3](https://framagit.org/very-old-devnewton-projects/jb3), une tribune possédant les caractéristiques suivantes:

- un code applicatif basé sur des technos web solides, modernes et performantes: Java et Spring Boot.
- une persistence nosql via Mongodb.
- une architecture n tiers, MVC, avec de l'injection de dépendances et d'autres patterns rigolos dedans.
- une API REST, plus ass2ass que hateoas, mais fonctionnelle.
- un frontend simple inspiré des meilleurs pratiques ergonomiques en matière de coincoin.
- une couche pour manager le legacy en mode lean, cad assurer la compatibilité avec les meilleurs c2 (testé et approuvé sur olcc) sans se forcer non plus à être compatible avec les nombreux clients pas très vivants.
- les moyens pour une future gestion avancée des norloges: celles-ci sont en fait des ids et le moteur se charge des conversions en norloge horaire afin de pouvoir un jour gérer les conversations en moules vivant sous des latitudes lointaines (cad pas UTC+1).
- l'intégration des meilleurs bots tels que alice<, fortune<, deeplop< et moinsbete<
- un code source simple à comprendre et à maintenir.
- facile à héberger grâce à un tomcat embarqué: il suffit de le lancer sur machine avec un mongodb et un serveur web pour faire reverse proxy, si possible dans une infrastructure Docker pour plus de sécurité.
- un slip basé sur [jsoup](http://jsoup.org/) et sur la découverte continue de failles.

Highly scalable, KISS et full friturée
======================================

jb3 est disponible sous licence ~~MYTHE~~MIT et hébergé sur [framagit le temps que je construise un vrai site](https://framagit.org/very-old-devnewton-projects/jb3).

Que faire après ce journal?
===========================

Si vous n'avez rien compris à ces histoires de b3, de c2 et de norloge, vous pouvez poser vos questions au [groupe d'experts qui se fera une joie de vous répondre](https://linuxfr.org/board).
