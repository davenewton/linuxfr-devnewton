URL:     https://linuxfr.org/users/devnewton/journaux/aseprite-1-0
Title:   Aseprite 1.0
Authors: devnewton
Date:    2014-06-25T14:00:04+02:00
License: CC By-SA
Tags:    sprite, animation, modèle_économique, appeau_a_zenitroll, éditeur et pixel_art
Score:   22


Le logiciel de dessin et animation [Aseprite](http://www.aseprite.org) [libre](https://github.com/aseprite/aseprite/blob/master/LICENSE.txt) est sorti en version [1.0](http://blog.aseprite.org/post/87978099760/aseprite-1-0-released).

Il est disponible:

- en [précompilée](http://www.aseprite.org/download/) pour Windows et Mac OS X en version complète pour 10$ ou gratuitement sans possibilité de sauvegarder.
- gratuitement et sans limite sous forme de [code source](https://github.com/aseprite/aseprite).

![Aseprite](tag-frames.gif)

Les noobs pauvres pourront se rabattre sur un éditeur dans les nuages, [piskel](http://juliandescottes.github.io/piskel/), écrit entièrement en html/javascript.
