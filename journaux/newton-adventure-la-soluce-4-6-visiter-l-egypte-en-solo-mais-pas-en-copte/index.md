URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-4-6-visiter-l-egypte-en-solo-mais-pas-en-copte
Title:   Newton Adventure la soluce 4/6: visiter l'egypte en solo, mais pas en copte.
Authors: devnewton
Date:    2012-09-04T18:11:52+02:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure et jeu_vidéo
Score:   6


Bonjour Nal,

Ce nourjal est le quatrième d'une série de six épisodes proposant la soluce en vidéo de Newton Adventure.

![Egypt](http://media.indiedb.com/images/games/1/17/16482/screen_3.jpg)

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Les vidéos
==========

Les vidéos de la troisième quête du jeu, arctic, sont téléchargeables:

* en http sur [fiat tux](http://www.fiat-tux.fr/files/newton/).
* en http sur mon petit [serveur](todo://retrouverlavideo) autohébergé.

(Un grand merci Luc pour sa précieuse aide et précieuse bande passante!)

Egypt est la première quête que j'ai créée pour Newton Adventure, les premières versions du jeu ne proposaient d'ailleurs pas de système de quête, juste ces niveaux au décor de désert et de briques. Elle démarre par un challenge très simple, mais devient vite assez difficile avec des niveaux très grands et est la seule à se conclure par l'affrontement avec un boss aux airs de Robotnic.

Les joueurs sont souvent déroutés par egypt_level2 qui est une grande cage dont tous les murs sont recouverts de piques mortelles. Le seul moyen de survivre est de se poser sur les nombreux nuages, mais ceux ci disparaissent en quelques secondes, il faut donc trouver comment s'échapper avant de ne plus avoir d'endroit où se poser.

Bientôt du nouveau
==================

Je travaille sur de nouvelle quête, dite "quêtes prologues", qui n'utiliseront pas forcément le changement de gravité à volonté. J'ai envie d'essayer de varier le style du jeu et voir ce que je peux créer à partir du moteur existant.

Et Webcrise dans tout ça?
=========================

J'avais annoncé un nouveau projet il y un mois, webcrise: https://linuxfr.org/users/devnewton/journaux/webcrise-appel-a-contribution

Je continue à réfléchir au gameplay, mais plus j'avance, plus je crains que ça ne soit un projet plus intéressant à réaliser qu'à utiliser. Je me demande comment font les game designers professionnels pour savoir avant d'avoir réaliser un jeu s'il sera intéressant...

La suite au prochain numéro
===========================

En attendant le prochain nourjal, jouez bien à Newton Adventure ou en regardez l'obligatoire [nimage](http://images.allocine.fr/medias/nmedia/18/62/90/40/19087847.jpg) de fin toujours esthétique et non sexiste.

