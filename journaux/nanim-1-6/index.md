URL:     https://linuxfr.org/users/devnewton/journaux/nanim-1-6
Title:   nanim 1.6
Authors: devnewton
Date:    2013-12-20T10:34:28+01:00
License: CC By-SA
Tags:    nanim et animation
Score:   20


Bonjour Nal,

Je t'écris pour te donner des nouvelles de [nanim](https://framagit.org/very-old-devnewton-projects/nanim). Au départ simple format d'animation 2D basé sur [protobuf](https://developers.google.com/protocol-buffers/) optimisé pour les jeux vidéos, le projet a évolué pour proposer un outil d'édition d'animation générique.

Cette version propose les nouveautés suivantes:

Compression
-----------

Je suis passé de [fossil](http://fossil-scm.org) à git et ce dernier n'apprécie pas les gros fichiers. Nanim étant un format non compressé, on arrive facilement à des fichiers de plusieurs dizaines de mo...

Les fichiers d'animation peuvent être compressés au format gzip (extension .nanim.gz ou .nanimz).

J'envisage de gérer d'autres types de compression (bzip?) si je trouve des bibliothèques simples.

Packaging
---------

Je fournis un paquet pour [debian](http://france.debian.net/) et un installeur pour [Windows](windowsfr.org). J'essayerais de faire des rpms ou d'autres paquets si j'ai des demandes.

J'aimerais bien faire des paquets pour Mac OS X, mais je ne sais pas si c'est possible sans acheter une machine à [Apple](http://applefr.org/).

JSON
----

J'envisage de faire une version web de [Newton Adventure](https://play.devnewton.fr). Malheureusement, il est assez difficile de gérer des fichiers binaires avec cette ~~daube~~ technologie du futur qu'est [HTML5](http://dailysyntax.com/blinking-text-in-html5-tag/).

J'ai fait un essai, mais ça donne un code est moche et des performances pas terribles... De plus la quasi totalité des cadriciels de jeu ne prévoient pas qu'une nimage puisse être autre chose qu'une image au format png/jpg/gif.

Comme j'ai peu de chances de faire accepter nanim par ~~le W3C~~ ~~le whatg~~ la fondation Mozilla, j'ai développé une version web de nanim en json:

    {
      "animations": [
        {
          "name": "walk_up",
          "frames": [
            {
              "duration": 100,
              "image": "ned_image_0.png",
              "u1": 0.0,
              "v1": 0.0,
              "u2": 1.0,
              "v2": 1.0
            },
            {
              "duration": 100,
              "image": "ned_image_1.png",
              "u1": 0.0,
              "v1": 0.0,
              "u2": 1.0,
              "v2": 1.0
            },
            {
              "duration": 100,
              "image": "ned_image_2.png",
              "u1": 0.0,
              "v1": 0.0,
              "u2": 1.0,
              "v2": 1.0
            },
            {
              "duration": 100,
              "image": "ned_image_3.png",
              "u1": 0.0,
              "v1": 0.0,
              "u2": 1.0,
              "v2": 1.0
            }
          ]
        },

Code clean
----------

J'ai également fait beaucoup de [ménage](http://www.lefigaro.fr/medias/2011/07/24/43996e10-b6cf-11e0-bc2f-eb507614a439.jpg): les outils peu utiles ont été abandonnés, la génération des paquets est plus simple et réduits la taille des livrables, le code a été nettoyé...


Aujourd'hui linuxfr et demain le Monde!
---------------------------------------

nanim commence à être utilisé pour d'autres projets que [Newton Adventure](https://play.devnewton.fr). Il y a bien sûr [Ned et les maki](https://play.devnewton.fr/prototypes.html), mais aussi [Akagoria](http://www.akagoria.org/) et un étonnant moteur de jeu qui reproduit le fonctionnement d'une [Super Nintendo](http://www.superplay.info/).

The end?
--------

A plus dans le bus, Nal!

[Site officiel](https://framagit.org/very-old-devnewton-projects/nanim)
[nimage de fin officielle](http://www.ztel.org/fichiers/euromussels/DSC00928_s.jpg)
