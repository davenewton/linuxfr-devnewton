URL:     https://linuxfr.org/users/devnewton/journaux/nanimstudio-1-0-est-dehors
Title:   nanimstudio 1.0 est dehors
Authors: devnewton
Date:    2013-08-14T19:04:31+02:00
License: CC By-SA
Tags:    nanim, animation et firefox
Score:   20


Bonjour Nal,

Je t'écris pour te faire part de la sortie de la version 1.0 de mon projet libre [nanimstudio](https://framagit.org/very-old-devnewton-projects/nanim/)
, un éditeur d'animation 2d, dont je t'avais parlé en [février](http://linuxfr.org/users/devnewton/journaux/nanimstudio-un-editeur-d-animations-2d).

La principale nouveauté est l'ajout d'un export au format [Animated Portable Network Graphics](https://wiki.mozilla.org/APNG_Specification).

~~Ce gros hack~~ Cette extension astucieuse de la norme PNG se veut le successeur des GIF animés ~~comme standard du web~~ pour le navigateur Firefox. Poussé par la fondation Mozilla, il est le concurrent du [MNG](http://fr.wikipedia.org/wiki/Multiple-image_Network_Graphics).

Démarré comme un outil pour éditer les nanims, un format d'animation basé sur [protobuf](https://code.google.com/p/protobuf/) et optimisé pour les [jeux](https://play.devnewton.fr), [nanimstudio](https://framagit.org/very-old-devnewton-projects/nanim/) est devenu un éditeur générique qui gère aussi les GIF et les spritesheets.
