URL:     https://linuxfr.org/users/devnewton/journaux/nanim-continue-son-bonhomme-de-chemin
Title:   nanim continue son bonhomme de chemin
Authors: devnewton
Date:    2012-10-11T21:20:54+02:00
License: CC By-SA
Tags:    nanim, newton_adventure, jeux_linux et jeu_vidéo
Score:   27


Bonjour Nal,

Je t’écris pour te faire part des dernières évolutions de mon projet [_nanim_](https://framagit.org/very-old-devnewton-projects/nanim/).

Au cas où tu aurais oublié, _nanim_ est un format d’animations que j’ai créé lors du développement de [_Newton Adventure_](https://play.devnewton.fr).

Comme tout bon développeur moderne, j’ai réinventé la roue en voyant que j’avais le choix entre une roue en bois (GIF) et des roues carrées (APNG, MNG), mais surtout pour pouvoir utiliser une technique connue sous le nom barbare de [_texture packing_](http://linuxfr.org/users/devnewton/journaux/nanimopt-un-optimiseur-de-nanim).

![_nanim_](nanim.jpg)

Outre de nombreuses corrections, j’ai ajouté au fur et à mesure de mes besoins de petits outils en ligne de commande pour manipuler les fichiers _nanim_. Dans l’esprit UNIX, chaque outil est destiné à une tâche précise :
 
* **_nanimenc_**, pour créer un _nanim_ à partir de PNG ;
* **_nanimdec_**, pour extraire des PNG ;
* **_nanimls_**, pour afficher des informations sur un _nanim ;_
* **_nanimopt_**, pour le _texture packing ;_
* **_nanimview_**, pour jouer les animations ;
* **_gif2nanim_**, pour convertir des ~~totoz~~ animations existantes ;
* **_nanimmerge_**, pour fusionner des _nanims ;_
* **_nanimrename_**, pour renommer des animations ou les images au sein d’un _nanim_.

J’aimerais disposer d’un outil graphique (_nanimstudio ?_ _nanimator ?_) pour faire ces opérations, mais j’étudie actuellement les logiciels existants, car je n’ai pas le temps de développer une interface utilisateur complète, et je préférerais donc écrire un simple greffon d’import‐export.

En attendant la suite, je te laisse convertir, à titre d’exercice, cette animation de [chauve tournant](characterrotation.gif).
