URL:     https://linuxfr.org/users/devnewton/journaux/recette-pain-perdu-au-chorizo
Title:   Recette: pain perdu au chorizo
Authors: devnewton
Date:    2013-07-25T13:58:29+02:00
License: CC By-SA
Tags:    recette_de_cuisine
Score:   14


Bonjour Nal,

Aujourd'hui je te propose une recette detox pour être bien dans son corps sous la chaleur.

Ingrédients
===========

Pour 4 personnes:

- 4 grandes tranches de pain.
- 4 œufs.
- un chorizo.
- 100g d'olives vertes.
- de l'origan
- du parmesan.
- un poivron

Préparation
===========

1. Couper le chorizo en fines tranches, le faire revenir à la poêle quelques minutes puis égoutter.
2. Couper les olives en deux.
3. Couper le poivron en lamelles.
4. Mélanger le chorizo, le poivron et olives.
5. Battre les oeufs et tremper le pain dedans.
6. Faire cuire à feu moyen à la poêle les tranches de pain quelques minutes.
7. Mettre le mélange sur les tranches de pain.
8. Râper du parmesan par dessus et saupoudrer d'origan.
9. Déguster tout de suite ou faire réchauffer au four.
