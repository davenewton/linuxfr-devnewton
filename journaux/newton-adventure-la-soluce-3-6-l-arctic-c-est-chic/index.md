URL:     https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-3-6-l-arctic-c-est-chic
Title:   Newton Adventure la soluce 3/6: l'arctic c'est chic
Authors: devnewton
Date:    2012-08-29T23:50:48+02:00
License: CC By-SA
Tags:    jeux_linux, newton_adventure, spam et jeu_vidéo
Score:   8


Bonjour Nal,

Ce nourjal est le troisième d'une série de six épisodes proposant la soluce en vidéo de [Newton Adventure](https://play.devnewton.fr).

![Arctic](http://tof.canardpc.com/view/8a66ff35-cf32-4835-94f5-828de439b00e.jpg)

Le jeu
======

Newton Adventure est un jeu vidéo libre de plates-formes qui met en scène Newton, un personnage qui a le pouvoir de modifier le sens de la gravité. Cela permet de parcourir les niveaux comme le joueur le désire, d'atteindre des plates-formes inaccessibles dans un jeu classique, de déplacer des objets ou ennemis grâce à la gravité.

Dans presque tous les niveaux, le but est de chercher une clef et de l'apporter jusqu'à la porte de sortie.

Il existe également des niveaux bonus qui demandent de récolter toutes les pommes dans un niveau normal et de trouver un passage spécial.

Dans les grands niveaux, Newton peut trouver une boussole et une carte pour se repérer.

Les vidéos
==========

Les vidéos de la troisième quête du jeu, arctic, sont téléchargeables:

* en http sur [fiat tux](http://www.fiat-tux.fr/files/newton/).
* via bitorrent sur [nitrotoxine](http://nitrotoxine.free.fr/www/libre/Newton_Adventure/2012-08-29-Newton_Adventure-Soluce_3_6.torrent)
* en http sur mon petit [serveur](todo://retrouverlavideo) autohébergé.

(Un grand merci à Nonas et Luc pour leur précieuse aide et précieuse leur bande passante!)

Par rapport aux précédentes quêtes, Arctic est plus orienté action. La plupart des plateformes sont givrées et font glisser Newton à toute vitesse vers de nombreux bumpers qui donnent au joueur la sensation d'être au milieu d'un flipper. Si les premiers niveaux peuvent être parcourus en fonçant comme un hérisson bleu roulé en boule, il faudra ensuite veiller à bien récupérer les items carte et boussole afin de ne pas perdre le grand nord.

Quelques nouvelles sur le développement
=======================================

Je suis en train d'essayer de négocier un compromis avec l'auteur de [Tiled](http://www.mapeditor.org/) afin de ne pas avoir à forker cet éditeur de niveau (cf explications dans le [journal](https://linuxfr.org/users/devnewton/journaux/newton-adventure-la-soluce-2-6-terminer-le-vatican-sans-papoter) précédent).

J'ai pu faire un nouvel essai de la version Android sur des téléphones récents, aujourd'hui je n'ai pas beaucoup de temps et de moyen pour m'occuper de ce portage, j'espère pouvoir le reprendre plus tard ou recruter un développeur qui a envie de travailler sur cet OS.

A demain si on le veut bien
===========================

Amusez vous bien en attendant le prochain nourjal en jouant à Newton Adventure ou en contemplant l'indispensable [nimage](http://tof.canardpc.com/view/e28a9c21-16c9-459c-8c21-a58d36a5c08d.jpg) de fin toujours esthétique et non sexiste.

